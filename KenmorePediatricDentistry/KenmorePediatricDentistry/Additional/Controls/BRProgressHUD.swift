//
//  BRProgressHUD.swift
//  MDRegistration
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

class BRProgressHUD: UIView {
    
    static var hudView: BRProgressHUD!

    class func sharedInstance() -> BRProgressHUD! {
        if hudView == nil {
            hudView = BRProgressHUD(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height))
            hudView.backgroundColor = UIColor.black.withAlphaComponent(0.2)
            
            hudView.activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
            hudView.activityView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
            hudView.activityView.center = hudView.center
            hudView.addSubview(hudView.activityView)
        }
        return hudView
    }
    
    class func show() {
        let hudView = sharedInstance()
        hudView?.activityView.startAnimating()
        UIApplication.shared.delegate?.window!!.addSubview(hudView!)
        UIApplication.shared.delegate?.window!!.bringSubview(toFront: hudView!)
        hudView?.isHidden = false
    }
    class func hide() {
        let hudView = sharedInstance()
        hudView?.removeFromSuperview()
        hudView?.isHidden = true
        hudView?.activityView.stopAnimating()
    }
    
    var activityView: UIActivityIndicatorView!
    
}
