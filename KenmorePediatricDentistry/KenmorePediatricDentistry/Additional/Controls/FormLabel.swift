//
//  FormLabel.swift
//  AmanaDental
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class FormLabel: UILabel {

    var bottomBorder : CALayer!
    
    override func layoutSubviews() {
        bottomBorder = bottomBorder == nil ? CALayer() : bottomBorder
        bottomBorder.frame = CGRect(x: 0.0, y: self.frame.size.height - 1, width: self.frame.size.width, height: 1.0)
        bottomBorder.backgroundColor = underlineColor.cgColor
        self.layer.addSublayer(bottomBorder)
    }
    @IBInspectable var underlineColor: UIColor = UIColor.black
}
class VerticalSeperator: UILabel {
    
    var verticalBorder : CALayer!
    
    override func layoutSubviews() {
        verticalBorder = verticalBorder == nil ? CALayer() : verticalBorder
        verticalBorder.frame = CGRect(x: self.frame.size.width/2, y: 0, width: 1.0, height: self.frame.size.height)
        verticalBorder.backgroundColor = UIColor.black.cgColor
        self.layer.addSublayer(verticalBorder)
    }
}
