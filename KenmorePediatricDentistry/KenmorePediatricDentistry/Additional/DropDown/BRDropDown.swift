//
//  BRDropDown.swift
//  PeopleCenter
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit
protocol BRDropDownDelegate {
    func dropDown(_ dropDown: BRDropDown, selectedAtIndex index: Int, selectedOption option: String?)
}
class BRDropDown: MCView {
    
    @IBInspectable var dropDownRowHeight: CGFloat!
    @IBInspectable var dropDownBorderColor: UIColor = UIColor.black {
        willSet {
            
        } didSet {
            tableView?.layer.borderColor = dropDownBorderColor.cgColor
        }
    }
    var placeholder: String = "-- SELECT --" {
        willSet {
            if enablePlaceholder {
                self.labelTitle.text = newValue
                self.dropDownButton.tag = 0
            } else if items.count > 0 {
                self.dropDownButton.tag = 1
                self.labelTitle.text = items[0].uppercased()
            }
        }
    }
    var delegate: BRDropDownDelegate?
    var items: [String]! {
        willSet {
            if enablePlaceholder {
                self.labelTitle.text = placeholder
                self.dropDownButton.tag = 0
            } else {
                self.dropDownButton.tag = 1
                self.labelTitle.text = newValue[0].uppercased()
            }
        } didSet {
            self.tableView.reloadData()
        }
    }
    var selected: Bool = false {
        willSet {
            dropDownButton.isSelected = newValue
            if newValue == true {
                showDropDown()
            } else {
                hideDropDown()
            }
        } didSet {
            
        }
    }
    var dropDownOptionSelected: Bool {
        get {
            return dropDownButton.tag > 0
        }
    }
    var selectedOption: String? {
        get {
            return dropDownButton.tag == 0 ? nil : items[dropDownButton.tag - 1]
        } set {
            if newValue == nil {
                self.selectedIndex = 0
            } else {
                self.selectedIndex = (items.index(of: newValue!) == nil) ? 0 : items.index(of: newValue!)! + 1
            }
        }
    }
    func reset() {
        self.selectedIndex = 0
    }
    var selectedIndex: Int {
        get {
            return dropDownButton.tag
        } set {
            if newValue == 0 {
                self.labelTitle.text = enablePlaceholder ? placeholder : items[0].uppercased()
                self.dropDownButton.tag = enablePlaceholder ? 0 : 1
            } else if newValue <= items.count {
                self.labelTitle.text = items[newValue - 1].uppercased()
                self.dropDownButton.tag = newValue
            }
        }
    }
    
    @IBInspectable var enablePlaceholder: Bool = true
    @IBInspectable var dropDownImage: UIImage? {
        willSet {
            
        } didSet {
            dropDownButton?.setBackgroundImage(dropDownImage, for: UIControlState())
        }
    }
    @IBInspectable var dropDownSelectedImage: UIImage? {
        willSet {
            
        } didSet {
            dropDownButton?.setBackgroundImage(dropDownSelectedImage, for: UIControlState.selected)
        }
    }
    
    var dropDownButton: UIButton!
    var labelTitle: UILabel!
    var tableView: UITableView!
    var superViewFrame: CGRect!
    
    var tableRowHeight: CGFloat {
        get {
            if dropDownRowHeight == nil {
                return dropDownButton.frame.height
            }
            return dropDownRowHeight
        }
    }
    @IBInspectable var selectedFont: UIFont = UIFont(name: "WorkSans-Regular", size: 17)! {
        willSet {
            
        } didSet {
            self.labelTitle?.font = selectedFont
        }
    }
    @IBInspectable var listFont: UIFont = UIFont(name: "WorkSans-Regular", size: 17)! {
        willSet {
            
        } didSet {
            self.tableView?.reloadData()
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initView()
    }
    func initView() {
        self.backgroundColor = UIColor.clear
        labelTitle = UILabel()
        labelTitle.textColor = UIColor.white
        labelTitle.textAlignment = NSTextAlignment.center
        labelTitle.font = UIFont(name: "WorkSans-Regular", size: 17)
        labelTitle.adjustsFontSizeToFitWidth = true
        self.addSubview(labelTitle)
        
        dropDownButton = UIButton()
        dropDownButton.setBackgroundImage(dropDownImage, for: UIControlState())
        dropDownButton.setBackgroundImage(dropDownSelectedImage, for: UIControlState.selected)
        dropDownButton.layer.borderColor = UIColor.white.cgColor
        dropDownButton.layer.borderWidth = 1.0
        dropDownButton.layer.cornerRadius = 3.0
        clipsToBounds = true
        dropDownButton.addTarget(self, action: #selector(BRDropDown.dropDownButtonAction(_:)), for: UIControlEvents.touchUpInside)
        self.addSubview(dropDownButton)
        
        tableView = UITableView()
        tableView.backgroundColor = UIColor.clear
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        tableView.separatorColor = UIColor.clear
        tableView.layer.cornerRadius = 3.0
        tableView.layer.masksToBounds = true
        
        self.addSubview(tableView)
    }
    func showDropDown() {
        (self.superview!.subviews as NSArray).enumerateObjects({ (obj, idx, stop) in
            if let otherdropDown = obj as? BRDropDown {
                if otherdropDown != self {
                    otherdropDown.selected = false
                }
            }
        })
        self.superview?.bringSubview(toFront: self)
        UIView.animate(withDuration: 0.3, animations: {
            var frame = self.frame
            frame.size.height = min(screenSize.height * (2/3), self.dropDownButton.frame.height + self.tableRowHeight * CGFloat(self.items.count))
            self.frame = frame
            self.tableView.frame = CGRect(x: 0, y: self.dropDownButton.frame.height, width: self.frame.width, height: self.frame.height - self.dropDownButton.frame.height)
            }, completion: { (finished) in
                let diff = self.frame.maxY - self.superViewFrame.height
                if diff > 0 {
                    UIView.animate(withDuration: 0.1, animations: {
                        self.superview?.frame.size.height = self.superViewFrame.height + diff + 20
                        self.superview?.frame.origin.y = self.superViewFrame.minY - diff - 20
                    }, completion: { (finished) in
                        if Int(self.tableView.frame.height/self.tableRowHeight) < self.items.count {
                            UIView.animate(withDuration: 0.2, animations: {
                                self.tableView.scrollToRow(at: IndexPath(row: self.items.count - 1, section: 0), at: UITableViewScrollPosition.bottom, animated: true)
                            })
                        }
                    }) 
                } else if Int(self.tableView.frame.height/self.tableRowHeight) < self.items.count {
                    UIView.animate(withDuration: 0.2, animations: {
                        self.tableView.scrollToRow(at: IndexPath(row: self.items.count - 1, section: 0), at: UITableViewScrollPosition.bottom, animated: true)
                    })
                }
        }) 
    }
    func hideDropDown() {
        UIView.animate(withDuration: 0.3, animations: {
            var frame = self.frame
            frame.size.height = self.dropDownButton.frame.height
            self.frame = frame
            self.tableView.frame = CGRect(x: 0, y: self.dropDownButton.frame.height, width: self.frame.width, height: 0)
        }, completion: { (finished) in
            UIView.animate(withDuration: 0.2, animations: {
                self.superview?.frame = self.superViewFrame
            })
        }) 
    }
    
    func dropDownButtonAction (_ sender: UIButton) {
        self.selected = !self.selected
    }
    
    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        dropDownButton.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        labelTitle.frame = CGRect(x: 0, y: 0, width: self.frame.width - self.dropDownButton.frame.height, height: self.frame.height)
        self.tableView.frame = CGRect(x: 0, y: self.dropDownButton.frame.height, width: self.frame.width, height: 0)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        if superViewFrame == nil && self.superview != nil {
            self.superViewFrame = self.superview!.frame
        }
    }
    
    func addItem(_ item: String) {
        if items == nil {
            items = [String]()
        }
        items.append(item)
    }
}
extension BRDropDown: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tableRowHeight
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return items != nil ? 1 : 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? BRDropDownCell
        if cell == nil {
            cell = BRDropDownCell(style: UITableViewCellStyle.default, reuseIdentifier: "Cell")
            cell?.backgroundColor = UIColor.white
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
        }
        cell!.title = items[indexPath.row].uppercased()
        cell!.titleColor = self.dropDownBorderColor
        cell!.titleFont = self.listFont
        
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.labelTitle.text = items[indexPath.row].uppercased()
        self.dropDownButton.tag = indexPath.row + 1
        self.selected = false
        
        self.delegate?.dropDown(self, selectedAtIndex: selectedIndex, selectedOption: selectedOption)
    }
    
    class BRDropDownCell: UITableViewCell {
        
        var labelTitle: UILabel!
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            
            labelTitle = UILabel()
            labelTitle.textColor = titleColor
            labelTitle.numberOfLines = 0
            labelTitle.font = UIFont(name: "WorkSans-Regular", size: 17)
            labelTitle.textAlignment = NSTextAlignment.center
            self.addSubview(labelTitle)
        }
        override func layoutSubviews() {
            super.layoutSubviews()
            labelTitle.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        var title: String! {
            willSet {
                self.labelTitle.text = newValue.uppercased()
            } didSet {
                
            }
        }
        var titleColor: UIColor = UIColor.black {
            willSet {
                self.labelTitle.textColor = newValue
            } didSet {
                
            }
        }
        var titleFont: UIFont = UIFont(name: "WorkSans-Regular", size: 17)! {
            willSet {
                
            } didSet {
                self.labelTitle.font = titleFont
            }
        }
    }
}
