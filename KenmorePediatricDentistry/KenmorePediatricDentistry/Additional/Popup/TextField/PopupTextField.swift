//
//  PopupTextField.swift
//  FusionDental
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PopupTextField: UIView {
    
    class func popUpView() -> PopupTextField {
        return Bundle.main.loadNibNamed("PopupTextField", owner: nil, options: nil)!.first as! PopupTextField
    }
    
    @IBOutlet weak var textField: MCTextField!
    @IBOutlet weak var labelTitle: UILabel!
    
    var completion:((PopupTextField, UITextField)->Void)?
    var count : Int!
    
    
    func show(_ completion : @escaping (_ popUpView: PopupTextField, _ textField : UITextField) -> Void) {
        self.showInViewController(nil, WithTitle: labelTitle.text, placeHolder: "TYPE HERE", textFormat: .Default, completion: completion)
    }
    
    func showInViewController(_ viewController: UIViewController?, WithTitle title: String?, placeHolder : String?, textFormat: TextFormat, completion : @escaping (_ popUpView: PopupTextField, _ textField : UITextField) -> Void) {
        textField.text = ""
        labelTitle.text = title
        textField.textFormat = textFormat
        if let _ = placeHolder {
            textField.placeholder = placeHolder
        }
        
        self.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
        self.completion = completion
        if viewController != nil {
            viewController!.view.addSubview(self)
        } else {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window!.addSubview(self)
        }
        self.subviews[0].transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
    
    func showDatePopupInViewController(_ viewController: UIViewController?, WithTitle title: String?, placeHolder : String?, minDate : Date?, maxDate : Date?, completion : @escaping (_ popUpView: PopupTextField, _ textField : UITextField) -> Void) {
        textField.text = ""
        labelTitle.text = title
        textField.textFormat = .Date
        if let _ = placeHolder {
            textField.placeholder = placeHolder
        }
        
        self.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
        self.completion = completion
        if viewController != nil {
            viewController!.view.addSubview(self)
        } else {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window!.addSubview(self)
        }
        self.subviews[0].transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
    
    @IBAction func buttonActionOK(_ sender: AnyObject) {
        completion?(self, self.textField)
    }
    func close() {
        self.removeFromSuperview()
    }
}
