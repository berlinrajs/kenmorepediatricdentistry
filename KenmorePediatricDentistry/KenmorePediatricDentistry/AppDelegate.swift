//
//  AppDelegate.swift
//  KenmorePediatricDentistry
//
//  Created by Bala Murugan on 1/24/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        if UserDefaults.standard.bool(forKey: "kAppFirstTimeLaunchFinished") == false {
            GTMOAuth2ViewControllerTouch.removeAuthFromKeychain(forName: kKeychainItemName)
            UserDefaults.standard.set(true, forKey: "kAppFirstTimeLaunchFinished")
            UserDefaults.standard.synchronize()
        }
        
        self.checkAutologin()
        return true
    }
    
    func checkAutologin() {
        if window == nil {
            self.window = UIWindow(frame: UIScreen.main.bounds)
        }
        if kAppLoginAvailable == true && UserDefaults.standard.bool(forKey: kAppLoggedInKey) == false {
            let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let loginVC = storyBoard.instantiateViewController(withIdentifier: "kLoginViewController") as! LoginViewController
            
            if window!.rootViewController != nil && !window!.rootViewController!.isKind(of: LoginViewController.self) {
                loginVC.view.frame = CGRect(x: -screenSize.width, y: 0, width: screenSize.width, height: screenSize.height)
                window?.addSubview(loginVC.view)
                UIView.animate(withDuration: 0.3, animations: {
                    self.window!.rootViewController!.view.frame = CGRect(x: screenSize.width/3, y: 0, width: screenSize.width, height: screenSize.height)
                    loginVC.view.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
                }, completion: { (finished) in
                    self.window?.rootViewController = loginVC
                })
            } else {
                self.window?.rootViewController = loginVC
                self.window?.makeKeyAndVisible()
            }
        } else {
//            let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
//            let navigationVC = storyBoard.instantiateViewController(withIdentifier: "kUINavigationViewController") as! UINavigationController
            
            let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
            var navigationVC: UINavigationController!
            
            #if AUTO
                let homeVC = storyBoard.instantiateViewController(withIdentifier: "HomeViewControllerAuto") as! HomeViewControllerAuto
                navigationVC = UINavigationController(rootViewController: homeVC)
                navigationVC.setNavigationBarHidden(true, animated: true)
            #else
                navigationVC = storyBoard.instantiateViewController(withIdentifier: "kUINavigationViewController") as! UINavigationController
            #endif
            
            if window!.rootViewController != nil && !window!.rootViewController!.isKind(of: UINavigationController.self) {
                navigationVC.view.frame = CGRect(x: screenSize.width, y: 0, width: screenSize.width, height: screenSize.height)
                window?.addSubview(navigationVC.view)
                UIView.animate(withDuration: 0.3, animations: {
                    self.window!.rootViewController!.view.frame = CGRect(x: -screenSize.width/3, y: 0, width: screenSize.width, height: screenSize.height)
                    navigationVC.view.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
                }, completion: { (finished) in
                    self.window?.rootViewController = navigationVC
                })
            } else {
                self.window?.rootViewController = navigationVC
                self.window?.makeKeyAndVisible()
            }
        }
    }
    
    func applicationSignificantTimeChange(_ application: UIApplication) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kDateChangedNotification), object: nil)
    }
    
    @available(iOS 9.0, *)
    func application(_ application: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey: Any]) -> Bool {
        return GIDSignIn.sharedInstance().handle(url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
    }
    
    @available(iOS, introduced: 8.0, deprecated: 9.0)
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return GIDSignIn.sharedInstance().handle(url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
    }
}

