//
//  Constants.swift
//  MConsentForms
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

public enum TextFormat : Int {
    case Default = 0
    case SocialSecurity
    case ToothNumber
    case Phone
    case ExtensionCode
    case Zipcode
    case Number
    case Date
    case Month
    case Year
    case DateInCurrentYear
    case DateIn1980
    case Time
    case MiddleInitial
    case State
    case Amount
    case Email
    case SecureText
    case AlphaNumeric
    case NumbersWithoutValidation
}

//KEYS
let kFormsCompletedNotification = "kAllFormsCompletedSuccessfully"
let kDateChangedNotification = "kDateChangedToNextDateNotificaion"

let kAppLoggedInKey = "kApploggedIn"
let kAppLoginUsernameKey = "kAppLoginUserName"
let kAppLoginPasswordKey = "kAppLoginPassword"

//VARIABLES


#if AUTO
let kKeychainItemLoginName = "Kenmore Pediatric Dentistry: Google Login"
let kKeychainItemName = "Kenmore Pediatric Dentistry: Google Drive"
let kClientId = "310425566733-9le8tfep4e062it3sf6it3k1752i8rtj.apps.googleusercontent.com"
let kClientSecret = "2hu6qbSOPTnCy_iXu3RniBn8"
let kFolderName = "KenmorePediatricDentistryAuto"
#else
let kKeychainItemLoginName = "Kenmore Pediatric Dentistry: Google Login"
let kKeychainItemName = "Kenmore Pediatric Dentistry: Google Drive"
let kClientId = "310425566733-9le8tfep4e062it3sf6it3k1752i8rtj.apps.googleusercontent.com"
let kClientSecret = "2hu6qbSOPTnCy_iXu3RniBn8"
let kFolderName = "KenmorePediatricDentistry"
#endif

let kDentistNames: [String] = ["DR. BRADLEY S. MOTT"]
let kDentistNameNeededForms = [kNewPatientSignInForm]
let kClinicName = "KENMORE PEDIATRIC DENTISTRY"
let kAppName = "KENMORE PEDIATRIC DENTISTRY"
let kPlace = "KENMORE, WA"
let kState = "WA"
let kAppKey = "mcKenmore"
let kAppLoginAvailable: Bool = true
let kCommonDateFormat = "MMM dd, yyyy"

let kGoogleID = "demo@srswebsolutions.com"
let kGooglePassword = "Srsweb123#"
let kOutputFileMustBeImage: Bool = false
//END OF VARIABLES

//FORMS
let kChildPatientSignInForm = "CHILD PATIENT SIGN IN FORM"
let kNewChildPatientSignInForm = "NEW CHILD PATIENT SIGN IN FORM"
let kNewPatientSignInForm = "NEW PATIENT SIGN IN FORM"
let kPatientSignInForm = "PATIENT SIGN IN FORM"
let kMedicalHistory = "MEDICAL HISTORY FORM"
let kMedicalHistoryUpdate = "ADULT MEDICAL HISTORY UPDATE FORM"
let kChildMedicalHistoryUpdate = "CHILD MEDICAL HISTORY UPDATE FORM"
let kDentalHistory = "DENTAL HISTORY FORM"
let kSelfieForm = "PATIENT PHOTO"

//CONSENT FORMS
let kConsentForms = "CONSENT FORMS"
let kNitrousOxide = "NITROUS OXIDE SEDATION CONSENT FORM"
let kSilverDiamine = "INFORMED CONSENT FOR SILVER DIAMINE FLUORIDE"
let kOralSurgery = "CONSENT FOR ORAL SURGERY"
let kBoneGrafting = "BONE GRAFTING AND BARRIER MEMBRANE CONSENT FORM"
let kRootCanal = "CONSENT FOR ROOT CANAL TREATMENT"
let kDentalExtract = "DENTAL EXTRACTION/ORAL SURGERY CONSENT"
let kNoticeOfPrivacy = "NOTICE OF PRIVACY PRACTICES"
let kAcknowledgementOfPrivacy = "ACKNOWLEDGEMENT OF PRIVACY PRACTICES"

let kInsuranceCard = "SCAN INSURANCE CARD"
let kDrivingLicense = "SCAN DRIVING LICENSE"

let kFeedBack = "CUSTOMER REVIEW FORM"
let kVisitorCheckForm = "VISITOR CHECK IN FORM"
// END OF FORMS


#if AUTO
let kNewPatient = ["NEW PATIENT" : [kNewPatientSignInForm,kNewChildPatientSignInForm,kMedicalHistoryUpdate,kChildMedicalHistoryUpdate,kInsuranceCard, kDrivingLicense, kSelfieForm]]
    
let kExistingPatient = ["EXISTING PATIENT": [kPatientSignInForm,kChildPatientSignInForm,kMedicalHistoryUpdate,kChildMedicalHistoryUpdate,kInsuranceCard, kDrivingLicense, kSelfieForm]]
    
let kConsentFormsAuto = ["CONSENT FORMS": [kNitrousOxide,kSilverDiamine,kBoneGrafting,kOralSurgery,kRootCanal,kDentalExtract]]
#endif

let toothNumberRequired: [String] = [kRootCanal,kDentalExtract]
//Replace '""' with form names
let consentIndex: Int = 7

