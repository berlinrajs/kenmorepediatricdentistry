//
//  PatientSignInStep2VC.swift
//  MConsentForms
//
//  Created by Berlin Raj on 22/09/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

import UIKit

class AdultAddressVC: MCViewController {

    @IBOutlet weak var textFieldAddress: MCTextField!
    @IBOutlet weak var textFieldCity: MCTextField!
    @IBOutlet weak var textFieldState: MCTextField!
    @IBOutlet weak var textFieldZip: MCTextField!
    @IBOutlet weak var radioGender: RadioButton!
    @IBOutlet weak var dropDownMarital: BRDropDown!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        textFieldState.textFormat = .State
        textFieldZip.textFormat = .Zipcode
        
        dropDownMarital.items = ["Single","Married"]
        dropDownMarital.placeholder = "MARITAL STATUS *"
        autoFill()
        // Do any additional setup after loading the view....
    }

       @IBAction override func buttonBackAction() {
            saveValues()
         _ = self.navigationController?.popViewController(animated: true)
    }

    func autoFill(){
        func loadMan(){
        textFieldAddress.text =  patient.adultAddress
        textFieldCity.text =   patient.adultCity
        textFieldState.text =  patient.adultState
        textFieldZip.text =  patient.adultZip
        radioGender.setSelectedWithTag(patient.adultGender)
        dropDownMarital.selectedIndex = patient.adultMarital
        }
        
        
        #if AUTO
            if let patientDetails = patient.patientDetails {
                //                if let gender = patient.gender {
                //                    radioGender.setSelectedWithTag(gender)
                //                } else
                if let gender = patientDetails.gender {
                    radioGender.setSelectedWithTag(gender.index)
                }
                //                if let maritalStatus = patient.maritalStatus {
                //                    dropDownMaritalStatus.selectedIndex = maritalStatus
                //                } else
                if let maritalStatus = patientDetails.maritalStatus {
                    dropDownMarital.selectedIndex = maritalStatus.index
                }
                textFieldCity.text = patient.adultCity != nil ? patient.adultCity : patientDetails.city
                textFieldState.text = patient.adultState != nil ? patient.adultState : patientDetails.state
                textFieldZip.text = patient.adultZip != nil ? patient.adultZip : patientDetails.zipCode
                textFieldAddress.text = patient.adultAddress != nil ? patient.adultAddress : patientDetails.address
            
            } else {
                loadMan()
            }
        #else
            loadMan()
        #endif
        
        
        
    }
    
    func saveValues(){
        patient.adultAddress = textFieldAddress.text
        patient.adultCity = textFieldCity.text
        patient.adultState = textFieldState.text
        patient.adultZip = textFieldZip.text
        patient.adultGender = radioGender.selected == nil ? 0 : radioGender.selected.tag
        patient.adultMarital = dropDownMarital.selectedIndex
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func buttonNextAction() {
        self.view.endEditing(true)
        if textFieldAddress.isEmpty  || textFieldState.isEmpty {
            self.showAlert("PLEASE ENTER A VALID ADDRESS")
        }else if textFieldCity.isEmpty{
            self.showAlert("PLEASE ENTER A CITY")
        }else if textFieldZip.isEmpty{
            self.showAlert("PLEASE ENTER A ZIPCODE")
        }else if !textFieldZip.text!.isZipCode {
            self.showAlert("PLEASE ENTER A VALID ZIPCODE")
        }else if dropDownMarital.selectedIndex == 0{
            self.showAlert("PLEASE SELECT MARITAL STATUS")
        }else if radioGender.selected == nil{
            self.showAlert("PLEASE SELECT THE GENDER")
        } else {
            saveValues()
            let nitrous = patientStoryBoard1.instantiateViewController(withIdentifier: "AdultContactVC") as! AdultContactVC
            nitrous.patient = self.patient
            self.navigationController?.pushViewController(nitrous, animated: true)
          
        }
    }
    
}
