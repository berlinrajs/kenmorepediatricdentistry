//
//  HipaaRefusalStep1VC.swift
//  WellnessDental
//
//  Created by Berlin Raj on 03/11/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

import UIKit

class AdultAgreeVC: MCViewController {
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var signatureView: SignatureView!
 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadValues()
    }
    
    func loadValues() {
        labelName.text =  patient.fullName
        labelDate.todayDate = patient.dateToday
    
    }
    
    func saveValues() {
        self.view.endEditing(true)
        
        patient.Signature1 = signatureView.signatureImage()

    }
    
    override func buttonBackAction() {
        saveValues()
        super.buttonBackAction()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonNextAction() {
        if !signatureView.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !labelDate.dateTapped {
            self.showAlert("PLEASE TAP TO DATE")
        } else {
            saveValues()
            
//        let  formVC = self.storyboard?.instantiateViewController(withIdentifier: "AdultSignInForm") as! AdultSignInForm
//        formVC.patient = self.patient
//        self.navigationController?.pushViewController(formVC, animated: true)
            
            let medicalHis = medicalStoryboard.instantiateViewController(withIdentifier: "kMedicalHistoryStep1ViewController") as! MedicalHistoryStep1ViewController
            medicalHis.patient = self.patient
            self.navigationController?.pushViewController(medicalHis, animated: true)

        }
    }
}



