//
//  PatientSignInStep2VC.swift
//  MConsentForms
//
//  Created by Berlin Raj on 22/09/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

import UIKit

class AdultContactVC: MCViewController {

    @IBOutlet weak var textFieldHomephone: MCTextField!
    @IBOutlet weak var textFieldBusinessPhone: MCTextField!
    @IBOutlet weak var textFieldCellPhone: MCTextField!
    @IBOutlet weak var textFieldEmail: MCTextField!
    @IBOutlet weak var textFieldSS: MCTextField!
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
      textFieldEmail.textFormat = .Email
        textFieldCellPhone.textFormat = .Phone
        textFieldHomephone.textFormat = .Phone
        textFieldSS.textFormat = .SocialSecurity
        textFieldBusinessPhone.textFormat = .Phone
        autoFill()
        // Do any additional setup after loading the view....
    }

       @IBAction override func buttonBackAction() {
            saveValues()
         _ = self.navigationController?.popViewController(animated: true)
    }

    func autoFill(){
        
        func loadMan(){
         textFieldHomephone.text = patient.adultHomePhone
        textFieldCellPhone.text = patient.adultCellPhone
        textFieldBusinessPhone.text = patient.adultBusinessPhone
        textFieldEmail.text = patient.adultEmail
         textFieldSS.text = patient.adultSS
        }
        
        #if AUTO
            if let patientDetails = patient.patientDetails {
                
                textFieldHomephone.text = patient.adultHomePhone != nil ? patient.adultHomePhone : patientDetails.homePhone
                textFieldCellPhone.text = patient.adultCellPhone != nil ? patient.adultCellPhone : patientDetails.cellPhone
                textFieldBusinessPhone.text = patient.adultBusinessPhone != nil ? patient.adultBusinessPhone : patientDetails.workPhone
                textFieldEmail.text = patient.adultEmail != nil ? patient.adultEmail : patientDetails.email
                textFieldSS.text = patient.adultSS != nil ? patient.adultSS : patientDetails.socialSecurityNumber
            } else {
                loadMan()
            }
        #else
            loadMan()
        #endif
    }
    
    func saveValues(){
        patient.adultHomePhone = textFieldHomephone.text
        patient.adultCellPhone = textFieldCellPhone.text
        patient.adultBusinessPhone = textFieldBusinessPhone.text
        patient.adultEmail = textFieldEmail.text
        patient.adultSS = textFieldSS.text
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func buttonNextAction() {
        self.view.endEditing(true)
        if !textFieldHomephone.isEmpty  && !textFieldHomephone.text!.isPhoneNumber {
            self.showAlert("PLEASE ENTER A VALID HOME PHONE NUMBER")
        }else if !textFieldCellPhone.isEmpty  && !textFieldCellPhone.text!.isPhoneNumber {
            self.showAlert("PLEASE ENTER A VALID CELL PHONE NUMBER")
        }else if !textFieldBusinessPhone.isEmpty  && !textFieldBusinessPhone.text!.isPhoneNumber {
            self.showAlert("PLEASE ENTER A VALID BUSINESS PHONE NUMBER")
        }else if !textFieldEmail.isEmpty  && !textFieldEmail.text!.isValidEmail {
            self.showAlert("PLEASE ENTER A VALID EMAIL")
        }else if !textFieldSS.isEmpty  && !textFieldSS.text!.isSocialSecurityNumber {
            self.showAlert("PLEASE ENTER A VALID SOCIAL SECURITY NUMBER")
        }else {
            saveValues()
            let nitrous = patientStoryBoard1.instantiateViewController(withIdentifier: "AdultEmployerVC") as! AdultEmployerVC
            nitrous.patient = self.patient
            self.navigationController?.pushViewController(nitrous, animated: true)
          
        }
    }
    
}
