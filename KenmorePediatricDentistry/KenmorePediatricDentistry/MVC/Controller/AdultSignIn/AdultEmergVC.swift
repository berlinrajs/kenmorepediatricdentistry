//
//  PatientSignInStep2VC.swift
//  MConsentForms
//
//  Created by Berlin Raj on 22/09/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

import UIKit

class AdultEmergVC: MCViewController {

    @IBOutlet weak var textFieldEmergphone: MCTextField!
    @IBOutlet weak var textFieldEmergName: MCTextField!
    @IBOutlet weak var textFieldResName: MCTextField!
    @IBOutlet weak var textFieldRespRelation: MCTextField!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        textFieldEmergphone.textFormat = .Phone
        autoFill()
        // Do any additional setup after loading the view....
    }

       @IBAction override func buttonBackAction() {
            saveValues()
         _ = self.navigationController?.popViewController(animated: true)
    }

    func autoFill(){
        
        textFieldEmergName.text = patient.adultEmergName
         textFieldEmergphone.text = patient.adultEmergContactPhone
         textFieldResName.text = patient.adultRespName
       textFieldRespRelation.text =  patient.adultRespRelation 
        
    }
    
    func saveValues(){
        
        patient.adultEmergName = textFieldEmergName.text
        patient.adultEmergContactPhone = textFieldEmergphone.text
        patient.adultRespName = textFieldResName.text
        patient.adultRespRelation = textFieldRespRelation.text
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func buttonNextAction() {
        self.view.endEditing(true)
        if !textFieldEmergphone.isEmpty  && !textFieldEmergphone.text!.isPhoneNumber {
            self.showAlert("PLEASE ENTER A VALID PHONE NUMBER")
        }else {
            saveValues()
            let nitrous = patientStoryBoard1.instantiateViewController(withIdentifier: "AdultSignInStep3VC") as! AdultSignInStep3VC
            nitrous.patient = self.patient
            self.navigationController?.pushViewController(nitrous, animated: true)
          
        }
    }
    
}
