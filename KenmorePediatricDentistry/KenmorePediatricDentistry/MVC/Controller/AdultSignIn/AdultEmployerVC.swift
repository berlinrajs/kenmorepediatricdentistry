//
//  PatientSignInStep2VC.swift
//  MConsentForms
//
//  Created by Berlin Raj on 22/09/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

import UIKit

class AdultEmployerVC: MCViewController {

    @IBOutlet weak var textFieldAddress: MCTextField!
    @IBOutlet weak var textFieldCity: MCTextField!
    @IBOutlet weak var textFieldState: MCTextField!
    @IBOutlet weak var textFieldZip: MCTextField!
    @IBOutlet weak var textFieldEmployer: MCTextField!
    @IBOutlet weak var textFieldOccupation: MCTextField!
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        textFieldState.textFormat = .State
        textFieldZip.textFormat = .Zipcode
        autoFill()
        // Do any additional setup after loading the view..
    }

       @IBAction override func buttonBackAction() {
            saveValues()
         _ = self.navigationController?.popViewController(animated: true)
    }

    func autoFill(){
        textFieldAddress.text =  patient.adultEmpAddress
        textFieldCity.text =   patient.adultEmpCity
        textFieldState.text =  patient.adultEmpState
        textFieldZip.text =  patient.adultEmpZip
        textFieldEmployer.text = patient.adultEmployer
         textFieldOccupation.text = patient.adultEmployerOccupation
        
    }
    
    func saveValues(){
        patient.adultEmpAddress = textFieldAddress.text
        patient.adultEmpCity = textFieldCity.text
        patient.adultEmpState = textFieldState.text
        patient.adultEmpZip = textFieldZip.text
        patient.adultEmployer = textFieldEmployer.text
        patient.adultEmployerOccupation = textFieldOccupation.text
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func buttonNextAction() {
        self.view.endEditing(true)
        if !textFieldZip.isEmpty && !textFieldZip.text!.isZipCode {
            self.showAlert("PLEASE ENTER A VALID ZIPCODE")
        }else {
            saveValues()
            if patient.adultMarital == 2{
                let nitrous = patientStoryBoard1.instantiateViewController(withIdentifier: "AdultSpouseVC") as! AdultSpouseVC
                nitrous.patient = self.patient
                self.navigationController?.pushViewController(nitrous, animated: true)

            }else{
                let nitrous = patientStoryBoard1.instantiateViewController(withIdentifier: "AdultEmergVC") as! AdultEmergVC
                nitrous.patient = self.patient
                self.navigationController?.pushViewController(nitrous, animated: true)

            }
          
        }
    }
    
}
