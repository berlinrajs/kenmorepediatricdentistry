//
//  PatientSignInStep5VC.swift
//  MConsentForms
//
//  Created by Berlin Raj on 22/09/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

import UIKit

class AdultPrimaryVC: MCViewController {

  
    @IBOutlet weak var dropDownRelation: BRDropDown!
    @IBOutlet weak var textFieldNameOfInsured: MCTextField!
    @IBOutlet weak var textFieldCompany: MCTextField!
    @IBOutlet weak var textFieldId: MCTextField!
    @IBOutlet weak var textFieldUnion: MCTextField!
    @IBOutlet weak var textFieldGroup: MCTextField!
    @IBOutlet weak var labelInsuranceTitle: UILabel!
    
    var otherRelation: String!

    override func viewDidLoad() {
        
        super.viewDidLoad()
        dropDownRelation.items = ["Self", "Guardian", "Other"]
        dropDownRelation.placeholder = "-- RELATIONSHIP TO PATIENT --"
        dropDownRelation.delegate = self
       
        
        autoFill()
            // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated..
    }
    
    @IBAction override func buttonBackAction() {
        saveValues()
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func autoFill(){
        textFieldNameOfInsured.text = patient.adultPrimaryInsuredName
         dropDownRelation.selectedIndex = patient.adultPrimaryRelationTag
       dropDownRelation.selectedOption =  patient.adultPrimaryRelationValue
         self.otherRelation = patient.adultPrimaryRelationOther
       textFieldCompany.text =  patient.adultPrimaryCompany
        textFieldGroup.text = patient.adultPrimaryGroup
        textFieldId.text =  patient.adultPrimaryId
        textFieldUnion.text =  patient.adultPrimaryUnion

        
    }
    
    func saveValues(){

        patient.adultPrimaryInsuredName = textFieldNameOfInsured.text
        patient.adultPrimaryRelationTag = dropDownRelation.selectedIndex
        patient.adultPrimaryRelationValue = dropDownRelation.selectedOption
        patient.adultPrimaryRelationOther = self.otherRelation
        patient.adultPrimaryCompany = textFieldCompany.text
        patient.adultPrimaryGroup = textFieldGroup.text
        patient.adultPrimaryId = textFieldId.text
        patient.adultPrimaryUnion = textFieldUnion.text

    }

    
    @IBAction func buttonNextAction () {
        self.view.endEditing(true)
        dropDownRelation.selected = false
       
         saveValues()
        if patient.adultSecondaryInsurance == 1 {
            let nitrous = patientStoryBoard1.instantiateViewController(withIdentifier: "AdultSecondaryVC") as! AdultSecondaryVC
            nitrous.patient = self.patient
            self.navigationController?.pushViewController(nitrous, animated: true)
            
        } else {
            let nitrous = patientStoryBoard1.instantiateViewController(withIdentifier: "AdultAgreeVC") as! AdultAgreeVC
            nitrous.patient = self.patient
            self.navigationController?.pushViewController(nitrous, animated: true)
            
        }

        
    }
}
extension AdultPrimaryVC: BRDropDownDelegate {
    func dropDown(_ dropDown: BRDropDown, selectedAtIndex index: Int, selectedOption option: String?) {
        if index == 1{
            textFieldNameOfInsured.text = patient.fullName
        }else{
            textFieldNameOfInsured.text = ""
        }
        if index == 3 {
           
            PopupTextField.popUpView().showInViewController(self, WithTitle: "PLEASE SPECIFY THE RELATION", placeHolder: "RELATIONSHIP TO PATIENT *", textFormat: TextFormat.Default, completion: { (popupView, textField) in
                if textField.isEmpty {
                    self.dropDownRelation.reset()
                    self.otherRelation = ""
                } else {
                    self.otherRelation = textField.text!
                }
                popupView.close()
            })
        }
        
      
    }
}
