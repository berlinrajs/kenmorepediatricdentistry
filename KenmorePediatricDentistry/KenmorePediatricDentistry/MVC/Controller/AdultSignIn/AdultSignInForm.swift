//
//  SilverDiamineFormViewController.swift
//  KenmorePediatricDentistry
//
//  Created by Bala Murugan on 1/25/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class AdultSignInForm: MCViewController {

    @IBOutlet weak var labelPatientName : FormLabel!
    @IBOutlet weak var signature : UIImageView!
    @IBOutlet weak var labelDate1 : FormLabel!
    @IBOutlet weak var labelBdate : FormLabel!
    @IBOutlet weak var labelAge : FormLabel!
    @IBOutlet weak var labelAddress : FormLabel!
    @IBOutlet weak var labelCity : FormLabel!
    @IBOutlet weak var labelState : FormLabel!
    @IBOutlet weak var labelZip : FormLabel!
    @IBOutlet weak var labelHomePhone : FormLabel!
    @IBOutlet weak var labelBusinessPhone : FormLabel!
    @IBOutlet weak var labelCellPhone : FormLabel!
    @IBOutlet weak var labelEmail : FormLabel!
    @IBOutlet weak var labelEmployer : FormLabel!
    @IBOutlet weak var labelOccupation : FormLabel!
    @IBOutlet weak var labelEmpAddress : FormLabel!
    @IBOutlet weak var labelEmpCity : FormLabel!
    @IBOutlet weak var labelEmpState : FormLabel!
    @IBOutlet weak var labelEmpZip : FormLabel!
    
    //resp1
    @IBOutlet weak var labelSpouseName : FormLabel!
    @IBOutlet weak var labelSpouseBdate : FormLabel!
    @IBOutlet weak var labelSpouseEmployer : FormLabel!
    @IBOutlet weak var labelSpouseOccupation : FormLabel!
    @IBOutlet weak var labelSpouseAddres : FormLabel!
    @IBOutlet weak var labelSpouseCity : FormLabel!
    @IBOutlet weak var labelSpouseState : FormLabel!
    @IBOutlet weak var labelSpouseZip : FormLabel!
    
    @IBOutlet weak var labelpatientSS: FormLabel!
    @IBOutlet weak var labelSpouseSS : FormLabel!
    @IBOutlet weak var labelRespName : FormLabel!
    @IBOutlet weak var labelRespRelation : FormLabel!
    
    //resp2
    @IBOutlet weak var primaryInsuredName : FormLabel!
    @IBOutlet weak var primaryInsuranceCompany : FormLabel!
    @IBOutlet weak var primaryGroup : FormLabel!
    @IBOutlet weak var primaryID : FormLabel!
    @IBOutlet weak var primaryUnion : FormLabel!
    
    @IBOutlet weak var secondaryInsuredName : FormLabel!
    @IBOutlet weak var secondaryInsuranceCompany : FormLabel!
    @IBOutlet weak var secondaryGroup : FormLabel!
    @IBOutlet weak var secondaryID : FormLabel!
    @IBOutlet weak var secondaryUnion : FormLabel!

    @IBOutlet weak var labelEmergName : FormLabel!
    @IBOutlet weak var labelEmergPhone : FormLabel!
    @IBOutlet weak var labelReferred : FormLabel!

    @IBOutlet weak var radioMarital: RadioButton!
    @IBOutlet weak var radioPrimary: RadioButton!
    @IBOutlet weak var radioSecondary: RadioButton!
    

    @IBOutlet weak var constraintTableViewForm1Height: NSLayoutConstraint!
    @IBOutlet weak var constraintTableViewForm4Height: NSLayoutConstraint!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelBirthDate: UILabel!
    @IBOutlet weak var labelDateCreated: UILabel!
    @IBOutlet weak var buttonAlergicOthers: UIButton!
    @IBOutlet weak var labelAlergicOthers: MCLabel!
    @IBOutlet weak var radioButtonSubstances: RadioButton!
    @IBOutlet weak var labelSubstances: MCLabel!
    @IBOutlet weak var radioButtonIllness: RadioButton!
    @IBOutlet weak var labelIllness: MCLabel!
    @IBOutlet weak var textViewComments: MCTextView!
    @IBOutlet weak var imageViewSignature: UIImageView!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var patietnSignName: UILabel!
    @IBOutlet var arrayButtons: [UIButton]!
    
    
    
    @IBOutlet weak var labelPrevDentist : UILabel!
    @IBOutlet weak var lastDentalExam : UILabel!
    @IBOutlet weak var lastDentalTreatment : UILabel!
    @IBOutlet weak var signatureDentist : UIImageView!
    @IBOutlet weak var labelDate2 : UILabel!
    @IBOutlet weak var lastDentalXray : UILabel!
    @IBOutlet weak var dentalFloss : UILabel!
    @IBOutlet weak var immediateConcern : UILabel!
    @IBOutlet weak var firstPartial : UILabel!
    @IBOutlet weak var presentDenture : UILabel!
    
    @IBOutlet var radioArray1: [RadioButton]!
    @IBOutlet var radioArray2: [RadioButton]!
    @IBOutlet var radioArray3: [RadioButton]!
    
    @IBOutlet weak var array00 : UILabel!
    @IBOutlet weak var array01 : UILabel!
    @IBOutlet weak var array02 : UILabel!
    @IBOutlet weak var array03 : UILabel!
    @IBOutlet weak var array04 : UILabel!
    @IBOutlet weak var array05 : UILabel!
    @IBOutlet weak var array06 : UILabel!
    @IBOutlet weak var array07 : UILabel!
    @IBOutlet weak var array08 : UILabel!
    @IBOutlet weak var array09 : UILabel!
    @IBOutlet weak var array010 : UILabel!
    
    @IBOutlet weak var array10 : UILabel!
    @IBOutlet weak var array11 : UILabel!
    @IBOutlet weak var array12 : UILabel!
    @IBOutlet weak var array13 : UILabel!
    @IBOutlet weak var array14 : UILabel!
    @IBOutlet weak var array15 : UILabel!
    @IBOutlet weak var array16 : UILabel!
    @IBOutlet weak var array17 : UILabel!
    @IBOutlet weak var array18 : UILabel!
    @IBOutlet weak var array19 : UILabel!
    @IBOutlet weak var array110 : UILabel!
    
    
    @IBOutlet weak var radioTeethClean: RadioButton!
    @IBOutlet weak var radioDentalFloss: RadioButton!
    
    
    //notice of privacy
    @IBOutlet weak var labelPatientName1 : UILabel!
    @IBOutlet weak var patientSignatureForm:
    UIImageView!
    @IBOutlet weak var labelDate3: UILabel!
    
    //acknowledge and office policy
    @IBOutlet weak var labelDateOfficePolicy: UILabel!
    @IBOutlet weak var signOfficePolicy: UIImageView!
    @IBOutlet weak var textFieldDependentFamily: UILabel!
    @IBOutlet weak var labelRelation: FormLabel!
    @IBOutlet weak var labelDate4: FormLabel!
    @IBOutlet weak var labelPatientName4: FormLabel!
    @IBOutlet weak var signImage: UIImageView!
    @IBOutlet weak var labelRefuslaOther : UILabel!
    @IBOutlet weak var radioRefusal: RadioButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadValues()
        self.medicalHistory()
        self.dentalHistory()
        self.noticeFunc()
        self.acknowledFunc()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    


    func loadValues(){
    
        labelPatientName.text = patient.fullName
        signature.image = patient.Signature1
        labelDate1.text = patient.dateToday
        labelBdate.text = patient.dateOfBirth
        labelAge.text = String(patient.patientAge)
        labelAddress.text = patient.adultAddress
        labelCity.text = patient.adultCity
        labelState.text = patient.adultState
        labelZip.text = patient.adultZip
        labelHomePhone.text = patient.adultHomePhone
        labelBusinessPhone.text = patient.adultBusinessPhone
        labelCellPhone.text = patient.adultCellPhone
        labelEmail.text = patient.adultEmail
        labelEmployer.text = patient.adultEmployer
        labelOccupation.text = patient.adultEmployerOccupation
        labelEmpAddress.text = patient.adultEmpAddress
        labelEmpCity.text = patient.adultEmpCity
        labelEmpState.text = patient.adultEmpState
        labelEmpZip.text = patient.adultEmpZip
        
        labelSpouseName.text = patient.adultSpouseName
        labelSpouseBdate.text = patient.adultSpouseBdate
        labelSpouseEmployer.text = patient.adultSpouseEmployer
        labelSpouseOccupation.text = patient.adultSpouseOccupation
        labelSpouseAddres.text = patient.adultSpouseAddress
        labelSpouseCity.text = patient.adultSpouseCity
        labelSpouseState.text = patient.adultSpouseState
        labelSpouseZip.text = patient.adultSpouseZip
        
        labelpatientSS.text = patient.adultSS
        labelSpouseSS.text = patient.adultSpouseSS
        labelRespName.text = patient.adultRespName
        labelRespRelation.text = patient.adultRespRelation
    
        primaryInsuredName.text = patient.adultPrimaryInsuredName
        primaryInsuranceCompany.text = patient.adultPrimaryCompany
        primaryGroup.text = patient.adultPrimaryGroup
        primaryID.text = patient.adultPrimaryId
        primaryUnion.text = patient.adultPrimaryUnion
        
        secondaryInsuredName.text = patient.adultSecondaryInsuredName
        secondaryInsuranceCompany.text = patient.adultSecondaryCompany
        secondaryGroup.text = patient.adultSecondaryGroup
        secondaryID.text = patient.adultSecondaryId
        secondaryUnion.text = patient.adultSecondaryUnion

        labelEmergName.text = patient.adultEmergName
        labelEmergPhone.text = patient.adultEmergContactPhone
        labelReferred.text = patient.adultWhoReferred
        
        radioMarital.setSelectedWithTag(patient.adultMarital)
        radioPrimary.setSelectedWithTag(patient.adultPrimary)
        radioSecondary.setSelectedWithTag(patient.adultSecondary)
    }
    
    
    func dentalHistory(){
    
        
        labelPrevDentist.text = patient.prevDentist
        lastDentalExam.text = patient.lastDentalExam
        lastDentalTreatment.text = patient.lastDentalTreatment
        lastDentalXray.text = patient.lastDentalXray
        dentalFloss.text = patient.dentalFlossValue
        immediateConcern.text = patient.immediateDentalConcern
        firstPartial.text = patient.firstPartialDenture
        presentDenture.text = patient.presentDenture
        
        signatureDentist.image = patient.Signature1
        labelDate2.text = patient.dateToday
        
        array00.text = patient.healthQues.DentalHistoryQues[0][0].answer
        array01.text = patient.healthQues.DentalHistoryQues[0][1].answer
        array02.text = patient.healthQues.DentalHistoryQues[0][2].answer
        array03.text = patient.healthQues.DentalHistoryQues[0][3].answer
        array04.text = patient.healthQues.DentalHistoryQues[0][4].answer
        array05.text = patient.healthQues.DentalHistoryQues[0][5].answer
        array06.text = patient.healthQues.DentalHistoryQues[0][6].answer
        array07.text = patient.healthQues.DentalHistoryQues[0][7].answer
        array08.text = patient.healthQues.DentalHistoryQues[0][8].answer
        array09.text = patient.healthQues.DentalHistoryQues[0][9].answer
        array010.text = patient.healthQues.DentalHistoryQues[0][10].answer
        
        array10.text = patient.healthQues.DentalHistoryQues[1][0].answer
        array11.text = patient.healthQues.DentalHistoryQues[1][1].answer
        array12.text = patient.healthQues.DentalHistoryQues[1][2].answer
        array13.text = patient.healthQues.DentalHistoryQues[1][3].answer
        array14.text = patient.healthQues.DentalHistoryQues[1][4].answer
        array15.text = patient.healthQues.DentalHistoryQues[1][5].answer
        array16.text = patient.healthQues.DentalHistoryQues[1][6].answer
        array17.text = patient.healthQues.DentalHistoryQues[1][7].answer
        array18.text = patient.healthQues.DentalHistoryQues[1][8].answer
        array19.text = patient.healthQues.DentalHistoryQues[1][9].answer
        array110.text = patient.healthQues.DentalHistoryQues[1][10].answer
        
        radioTeethClean.setSelectedWithTag(patient.dropDownTeethClean)
        radioDentalFloss.setSelectedWithTag(patient.dentalFloss)
        
        for btn in radioArray1{
            
            btn.isSelected = patient.healthQues.DentalHistoryQues[0][btn.tag].selectedOption
        }
        
        for btn in radioArray2{
            
            btn.isSelected = patient.healthQues.DentalHistoryQues[1][btn.tag].selectedOption
        }
        
        for btn in radioArray3{
            
            btn.isSelected = patient.healthQues.DentalHistoryQues[2][btn.tag].selectedOption
        }

    
    }
    
    
    func noticeFunc(){
    labelDate3.text = patient.dateToday
    patientSignatureForm.image = patient.signPatient
    labelPatientName1.text = patient.fullName
    
    }
    
    func acknowledFunc(){
        if patient.signRefused == true{
            labelPatientName4.text = patient.fullName
            labelDate4.text = patient.dateToday
            radioRefusal.setSelectedWithTag(patient.signRefusalTag)
            labelRefuslaOther.text = patient.signRefusalOther
        }else{
            labelRelation.text = patient.relation
            labelDate4.text = patient.dateToday
            labelPatientName4.text = patient.fullName
            signImage.image = patient.Signature1
            textFieldDependentFamily.text = patient.dependentFamily
           
        }
        labelDateOfficePolicy.text = patient.dateToday
        signOfficePolicy.image = patient.signPolicy
        
    }
    
    func medicalHistory(){
    
        patietnSignName.text = patient.fullName
        
        buttonSubmit!.backgroundColor = UIColor.green
        constraintTableViewForm1Height.constant = CGFloat(patient.medicalHistoryQuestions1.count * 26)
        constraintTableViewForm4Height.constant = CGFloat((19 * 15) + 18)
        
        labelName.text = "Patient Name: \(patient.fullName)"
        labelBirthDate.text = "Birth Date: \(patient.dateOfBirth!)"
        labelDateCreated.text = "Date Created: \(patient.dateToday!)"
        
        if let alergic = patient.othersTextForm3 {
            buttonAlergicOthers.isSelected = true
            labelAlergicOthers.text = " " + alergic
        }
        if let substances = patient.controlledSubstances {
            radioButtonSubstances.isSelected = true
            labelSubstances.text = " " + substances
        } else if patient.controlledSubstancesClicked != nil {
            radioButtonSubstances.isSelected = false
        }
        
        //        if let illness = patient.otherIllness {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
        //            radioButtonIllness.selected = true
        //            labelIllness.text = " " + illness
        //        } else {
        //            radioButtonIllness.selected = false
        //        }
        
        radioButtonIllness.setSelectedWithTag(patient.buttonIllness)
        labelIllness.text = patient.buttonIllness == 2 ? "" : patient.otherIllness
        
        if let comment = patient.comments {
            textViewComments.text = comment
        }
        
        imageViewSignature.image = patient.medicalSignature
        labelDate.text = patient.dateToday
        
        for (idx, buttonQuestion) in self.arrayButtons.enumerated() {
            let obj = patient.medicalHistoryQuestions2[idx]
            buttonQuestion.setTitle(" " + obj.question, for: .normal)
            if let selected = obj.selectedOption {
                buttonQuestion.isSelected = selected
            } else {
                buttonQuestion.isSelected = false
            }
        }

    
    }
    
    override func onSubmitButtonPressed() {
        
        #if AUTO
            if !Reachability.isConnectedToNetwork() {
                let alertController = UIAlertController(title: kAppName, message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.alert)
                let alertOkAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.destructive) { (action) -> Void in
                    let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                    if let url = settingsUrl {
                        UIApplication.shared.openURL(url as URL)
                    }
                }
                let alertCancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive) { (action) -> Void in
                    
                }
                alertController.addAction(alertOkAction)
                alertController.addAction(alertCancelAction)
                self.present(alertController, animated: true, completion: nil)
                return
            }
            BRProgressHUD.show()
            patient.isChild = false
            ServiceManager.sendPatientDetails(patient: patient, completion: { (success, error) in
                BRProgressHUD.hide()
                if success {
                    super.onSubmitButtonPressed()
                } else {
                    self.showAlert(error!.localizedDescription.uppercased())
                }
            })
        #else
            super.onSubmitButtonPressed()
        #endif
    }


}

extension AdultSignInForm : UICollectionViewDataSource {
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionView.tag == 1 ? patient.medicalHistoryQuestions2.count : patient.medicalHistoryQuestions3.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellMedicalHistory", for: indexPath) as! MedicalHistoryCollectionViewCell
        let obj = collectionView.tag == 1 ? patient.medicalHistoryQuestions2[indexPath.item] : patient.medicalHistoryQuestions3[indexPath.item]
        cell.configureCellOption(obj: obj)
        return cell
    }
}

extension AdultSignInForm : UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 1 {
            return patient.medicalHistoryQuestions1.count
        } else {
            let currentIndex = tableView.tag - 2
            return currentIndex < 3 ? 19 : 20
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.tag == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellMedicalHistoryFrom1", for: indexPath) as! MedicalHistoryFormTableViewCell1
            let obj = patient.medicalHistoryQuestions1[indexPath.row]
            cell.configureCell(obj: obj)
            return cell
        } else {
            let currentIndex = tableView.tag - 2
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellMedicalHistoryFrom4", for: indexPath) as! MedicalHistoryStep1TableViewCell
            let index = (currentIndex * 19) + indexPath.row
            let obj = patient.medicalHistoryQuestions4[index]
            cell.configureCell(obj: obj)
            let stringObj = ["AIDS/HIV Positive", "Artificial heart valve", "Artificial Joint", "Cancer", "Chemotherapy", "Diabetes", "Drug Addiction", "Excessive Bleeding", "Heart Attack / Failure", "Heart Pacemaker", "Heart Trouble / Disease", "Hepatitis A", "Hepatitis B or C", "High Blood Pressure", "Tuberculosis"]
            if let selected = obj.selectedOption {
                cell.labelTitle.textColor = selected && stringObj.contains(obj.question) ? UIColor.red : UIColor.black
                cell.labelTitle.font = UIFont(name:selected ? "WorkSans-Medium" :  "WorkSans-Regular", size: cell.labelTitle.font.pointSize)
                cell.buttonYes.isSelected = selected
            } else {
                cell.labelTitle.textColor = UIColor.black
                cell.labelTitle.font = UIFont(name: "WorkSans-Regular", size: cell.labelTitle.font.pointSize)
                cell.buttonYes.deselectAllButtons()
            }
            return cell
        }
    }
}

