//
//  PatientSignInStep3VC.swift
//  MConsentForms
//
//  Created by Berlin Raj on 22/09/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

import UIKit

class AdultSignInStep3VC: MCViewController {

    @IBOutlet weak var radioPrimary: RadioButton!
    @IBOutlet weak var radioSecondary: RadioButton!
    @IBOutlet weak var textFieldChildReferred: MCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        autoFill()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction override func buttonBackAction() {
        saveValues()
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func autoFill(){
        textFieldChildReferred.text = patient.adultWhoReferred
        radioPrimary.setSelectedWithTag(patient.adultPrimary)
        radioSecondary.setSelectedWithTag(patient.adultSecondary)
        
    }
    
    func saveValues(){
        patient.adultWhoReferred = textFieldChildReferred.text
        patient.adultPrimary = radioPrimary.selected == nil ? 0 : radioPrimary.selected.tag
        patient.adultSecondary = radioSecondary.selected == nil ? 0 : radioSecondary.selected.tag
    }
    
    @IBAction func buttonNextAction() {
         if radioPrimary.selected == nil {
            self.showAlert("PLEASE SELECT ALL THE REQUIRED")
        } else if radioSecondary.selected == nil {
            self.showAlert("PLEASE SELECT ALL THE REQUIRED")
        } else if radioPrimary.isSelected == false && radioSecondary.isSelected == true {
            self.showAlert("SECONDARY INSURANCE CANNOT BE AVAILABLE WITHOUR PRIMARY")
        } else {
            saveValues()
            patient.adultPrimaryInsurance = radioPrimary.selected.tag
            patient.adultSecondaryInsurance = radioSecondary.selected.tag
            
             if patient.adultPrimaryInsurance == 1 {
                let nitrous = patientStoryBoard1.instantiateViewController(withIdentifier: "AdultPrimaryVC") as! AdultPrimaryVC
                nitrous.patient = self.patient
                self.navigationController?.pushViewController(nitrous, animated: true)

             } else if patient.adultSecondaryInsurance == 1 {
                let nitrous = patientStoryBoard1.instantiateViewController(withIdentifier: "AdultSecondaryVC") as! AdultSecondaryVC
                nitrous.patient = self.patient
                self.navigationController?.pushViewController(nitrous, animated: true)

            } else {
                let nitrous = patientStoryBoard1.instantiateViewController(withIdentifier: "AdultAgreeVC") as! AdultAgreeVC
                nitrous.patient = self.patient
                self.navigationController?.pushViewController(nitrous, animated: true)
                
            }
        }
    }
}
