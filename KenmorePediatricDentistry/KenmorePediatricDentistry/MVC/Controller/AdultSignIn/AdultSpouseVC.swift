//
//  PatientSignInStep2VC.swift
//  MConsentForms
//
//  Created by Berlin Raj on 22/09/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

import UIKit

class AdultSpouseVC: MCViewController {

    @IBOutlet weak var textFieldName: MCTextField!
    @IBOutlet weak var textFieldBdate: MCTextField!
    @IBOutlet weak var textFieldSS: MCTextField!
    @IBOutlet weak var textFieldAddress: MCTextField!
    @IBOutlet weak var textFieldCity: MCTextField!
    @IBOutlet weak var textFieldState: MCTextField!
    @IBOutlet weak var textFieldZip: MCTextField!
    @IBOutlet weak var textFieldEmployer: MCTextField!
    @IBOutlet weak var textFieldOccupation: MCTextField!
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        textFieldState.textFormat = .State
        textFieldZip.textFormat = .Zipcode
        textFieldSS.textFormat = .SocialSecurity
        textFieldBdate.textFormat = .DateIn1980
        autoFill()
        // Do any additional setup after loading the view....
    }

       @IBAction override func buttonBackAction() {
            saveValues()
         _ = self.navigationController?.popViewController(animated: true)
    }

    func autoFill(){
        textFieldAddress.text =  patient.adultSpouseAddress
        textFieldCity.text =   patient.adultSpouseCity
        textFieldState.text =  patient.adultSpouseState
        textFieldZip.text =  patient.adultSpouseZip
        textFieldEmployer.text = patient.adultSpouseEmployer
         textFieldOccupation.text = patient.adultSpouseOccupation
        textFieldName.text = patient.adultSpouseName
        textFieldBdate.text = patient.adultSpouseBdate
        textFieldSS.text = patient.adultSpouseSS
        
    }
    
    func saveValues(){
        patient.adultSpouseAddress = textFieldAddress.text
        patient.adultSpouseCity = textFieldCity.text
        patient.adultSpouseState = textFieldState.text
        patient.adultSpouseZip = textFieldZip.text
        patient.adultSpouseEmployer = textFieldEmployer.text
        patient.adultSpouseOccupation = textFieldOccupation.text
        patient.adultSpouseName = textFieldName.text
        patient.adultSpouseBdate = textFieldBdate.text
        patient.adultSpouseSS = textFieldSS.text
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func buttonNextAction() {
        self.view.endEditing(true)
        if !textFieldSS.isEmpty  && !textFieldSS.text!.isSocialSecurityNumber {
            self.showAlert("PLEASE ENTER A VALID SOCIAL SECURITY NUMBER")
        }else if !textFieldZip.isEmpty && !textFieldZip.text!.isZipCode{
            self.showAlert("PLEASE ENTER A VALID ZIPCODE")
        }else {
            saveValues()
            let nitrous = patientStoryBoard1.instantiateViewController(withIdentifier: "AdultEmergVC") as! AdultEmergVC
            nitrous.patient = self.patient
            self.navigationController?.pushViewController(nitrous, animated: true)
          
        }
    }
    
}
