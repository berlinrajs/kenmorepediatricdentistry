//
//  BoneGrafting1ViewController.swift
//  KenmorePediatricDentistry
//
//  Created by Bala Murugan on 1/25/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class BoneGrafting1ViewController: MCViewController {

    @IBOutlet var signatureInitials : [SignatureView]!
    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.todayDate = patient.dateToday
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !signaturePatient.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            let bone = consentStoryBoard.instantiateViewController(withIdentifier: "BoneGraftingFormVC") as! BoneGraftingFormViewController
            bone.patient = self.patient
            bone.signPatient = signaturePatient.signatureImage()
            for imgvw in signatureInitials{
                let image = imgvw.signatureImage() == nil ? UIImage() : imgvw.signatureImage()
                bone.signInitials.append(image!)
            }
            self.navigationController?.pushViewController(bone, animated: true)

        }
    }

}
