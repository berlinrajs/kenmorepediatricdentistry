//
//  BoneGraftingFormViewController.swift
//  KenmorePediatricDentistry
//
//  Created by Bala Murugan on 1/25/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class BoneGraftingFormViewController: MCViewController {

    var signInitials : [UIImage] = [UIImage]()
    var signPatient : UIImage!
    @IBOutlet var signatureInitials : [UIImageView]!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var signaturePatient : UIImageView!
    @IBOutlet weak var labelDate : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelPatientName.text = patient.fullName
        signaturePatient.image = signPatient
        labelDate.text = patient.dateToday
        for (idx,initialView) in signatureInitials.enumerated(){
            initialView.image = signInitials[idx]
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
