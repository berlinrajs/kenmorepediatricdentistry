//
//  CardImageCaptureVC.swift
//  AceDental
//
//  Created by SRS Web Solutions on 29/04/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class CardImageCaptureVC: MCViewController {

    @IBOutlet weak var imageViewFront: ActionImageView!
    @IBOutlet weak var imageViewBack: ActionImageView!
    @IBOutlet weak var labelTitle: UILabel!
    
    var isDrivingLicense: Bool = false
    var isFrontImageSelected: Bool = false
    var isBackImageSelected: Bool = false
    
    var frontPlaceHolderImage: UIImage? {
        get {
            return isDrivingLicense == true ? UIImage(named: "LicenseFront") : UIImage(named: "InsuranceFront")
        }
    }
    
    var backPlaceHolderImage: UIImage? {
        get {
            return isDrivingLicense == true ? UIImage(named: "LicenseBack") : UIImage(named: "InsuranceBack")
        }
    }
    
    var selectedButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelTitle.text = isDrivingLicense ? "ADD/UPDATE DRIVING LICENSE" : "ADD/UPDATE INSURANCE CARD"
      
        imageViewFront.cornerRadius = 5.0
        imageViewBack.cornerRadius = 5.0
        imageViewBack.borderColor = UIColor.clear
        imageViewFront.borderColor = UIColor.clear
        
        imageViewFront.image = self.frontPlaceHolderImage
        imageViewBack.image = self.backPlaceHolderImage
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
    
    @IBAction func nextAction(_ sender: AnyObject) {
        
        if isDrivingLicense && !isFrontImageSelected {
            self.showAlert("PLEASE CAPTURE FRONT SIDE OF THE CARD")
            
        } else if !isDrivingLicense && !isFrontImageSelected  {
            self.showAlert("PLEASE CAPTURE FRONT SIDE OF THE CARD")
            
        } else {
            let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kCardImageFormVC") as! CardImageFormVC
            formVC.patient = self.patient
            formVC.isDrivingLicense = self.isDrivingLicense
            formVC.frontImage = self.imageViewFront.image!
            formVC.backImage = isBackImageSelected ? self.imageViewBack.image : nil
            navigationController?.pushViewController(formVC, animated: true)
        }
    }

    @IBAction func camButtonSelected(_ sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let picker = self.storyboard?.instantiateViewController(withIdentifier: "kCardImagePickerControllerNew") as! CardImagePickerControllerNew
            picker.delegate = self
            self.present(picker, animated: true) {
                self.selectedButton = sender
            }
        }
    }
}
extension CardImageCaptureVC: CardImageCaptureDelegate {
    func cardImagePicker(_ picker: CardImagePickerControllerNew, completedWithCardImage image: UIImage?) {
        if image != nil {
            if selectedButton.tag == 1 {
                imageViewFront.image = image
                isFrontImageSelected = true
                selectedButton.isSelected = true
                
                imageViewFront.layer.borderColor = UIColor.white.cgColor
            } else {
                imageViewBack.image = image
                isBackImageSelected = true
                selectedButton.isSelected = true
                
                imageViewBack.layer.borderColor = UIColor.white.cgColor
            }
        }
        picker.dismiss(animated: true) {
            self.selectedButton = nil
        }
    }
    func cardImagePickerDidCancel(_ picker: CardImagePickerControllerNew){
        picker.dismiss(animated: true) {
            self.selectedButton = nil
        }
    }
}
