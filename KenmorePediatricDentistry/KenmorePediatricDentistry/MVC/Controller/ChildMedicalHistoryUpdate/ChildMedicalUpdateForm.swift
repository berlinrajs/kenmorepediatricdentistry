//
//  ChildMedicalUpdateForm.swift
//  KenmorePediatricDentistry
//
//  Created by Bala Murugan on 1/25/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class ChildMedicalUpdateForm: MCViewController {

   
    @IBOutlet weak var signature : UIImageView!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet var radioArray4: [RadioButton]!
    @IBOutlet var radioArray5: [RadioButton]!
    @IBOutlet var radioArray6: [RadioButton]!
    @IBOutlet var radioArray7: [RadioButton]!
    @IBOutlet var radioArray8: [RadioButton]!
    
    
    @IBOutlet weak var array31Answer : UILabel!
    @IBOutlet weak var array32Answer : UILabel!
    @IBOutlet weak var array33Answer : UILabel!
    @IBOutlet weak var array34Answer : UILabel!

    @IBOutlet weak var childUnderPhysicianCare: RadioButton!
    
    @IBOutlet weak var doesYourChildPblm: UILabel!
    @IBOutlet weak var elaboratePblm : UILabel!
    @IBOutlet weak var childPhysicianName : UILabel!
    @IBOutlet weak var physicianPhone : UILabel!
    @IBOutlet weak var lastPhysicalExam : UILabel!
    @IBOutlet weak var findings : UILabel!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelBdate : UILabel!
 
    @IBOutlet weak var labelMedicalHistoryOther : UILabel!
    @IBOutlet weak var labelGuardianName : UILabel!
  
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadValues()
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func loadValues(){
    
    labelPatientName.text = patient.fullName
    
    labelBdate.text = patient.dateOfBirth
      

       
        
        for btn in radioArray4{
            
            btn.isSelected = patient.healthQues.HealthHistoryQues[3][btn.tag].selectedOption
        }
        
        for btn in radioArray5{
            
            btn.isSelected = patient.healthQues.HealthHistoryQues[4][btn.tag].selectedOption
        }
        
        for btn in radioArray6{
            
            btn.isSelected = patient.healthQues.HealthHistoryQues[5][btn.tag].selectedOption
        }
        
        for btn in radioArray7{
            
            btn.isSelected = patient.healthQues.HealthHistoryQues[6][btn.tag].selectedOption
        }
        
        for btn in radioArray8{
            
            btn.isSelected = patient.healthQues.HealthHistoryQues[7][btn.tag].selectedOption
        }
  
        array31Answer.text = patient.healthQues.HealthHistoryQues[3][1].answer
        array32Answer.text = patient.healthQues.HealthHistoryQues[3][2].answer
        array33Answer.text = patient.healthQues.HealthHistoryQues[3][3].answer
        array34Answer.text = patient.healthQues.HealthHistoryQues[3][4].answer
        
        signature.image = patient.Signature1
        labelDate1.text = patient.dateToday
        
        doesYourChildPblm.text = patient.doesYourChild
        elaboratePblm.text = patient.textViewElaborate
        childPhysicianName.text = patient.childPhysician
        physicianPhone.text = patient.childPhysicianPhone
        lastPhysicalExam.text = patient.childLastPhysicalExam
        findings.text = patient.childFindings

        childUnderPhysicianCare.setSelectedWithTag(patient.childPreviouslyUnderPhysicianCare)
        
        labelMedicalHistoryOther.text = patient.healthQues.HealthHistoryQues[7][6].answer
       
        labelGuardianName.text = patient.medicalGuardianName
    }
   
    
    

}
