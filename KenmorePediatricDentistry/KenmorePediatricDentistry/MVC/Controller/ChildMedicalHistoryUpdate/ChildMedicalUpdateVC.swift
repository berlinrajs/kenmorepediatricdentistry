//
//  ChildMedicalUpdateVC.swift
//  WestKendall
//
//  Created by Bala Murugan on 10/5/16.
//  Copyright © 2016 SRS. All rights reserved..
//

import UIKit

class ChildMedicalUpdateVC: MCViewController {

    @IBOutlet var tableViewQuestions: UITableView!
    @IBOutlet var buttonVerified: UIButton!
    @IBOutlet var buttonNext: UIButton!
    @IBOutlet var buttonBack1: UIButton!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var labelMainHeading: UILabel!
    @IBOutlet var labelHeading : UILabel!
    var selectedIndex: Int = 3
    var medicalHistoryStep1 : [MCQuestion]! = [MCQuestion]()
    var popUpWhySeeingPhysician : String!
    
@IBOutlet weak var textViewNotes: MCTextView!
@IBOutlet weak var extraView: UIView!
 
    override func viewDidLoad() {
        super.viewDidLoad()
        self.activityIndicator.stopAnimating()
        textViewNotes.delegate?.textViewDidBeginEditing?(textViewNotes)
        textViewNotes.text =   patient.notesValue
        textViewNotes.delegate?.textViewDidEndEditing?(textViewNotes)
        
        }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onBackButton1Pressed (withSender sender: AnyObject) {
     
        textViewNotes.delegate?.textViewDidBeginEditing?(textViewNotes)
         patient.notesValue = textViewNotes.text
        textViewNotes.delegate?.textViewDidEndEditing?(textViewNotes)
        if selectedIndex == 3 {
             _ = self.navigationController?.popViewController(animated: true)
        } else {
            buttonBack?.isUserInteractionEnabled = false
            buttonNext.isUserInteractionEnabled = false
            self.activityIndicator.startAnimating()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.activityIndicator.stopAnimating()
                self.selectedIndex = self.selectedIndex - 1
                if self.selectedIndex == 3 {
                    self.labelMainHeading.text = "MEDICAL HISTORY UPDATE"
                    self.labelHeading.text = "Is your child:"
                    self.extraView.isHidden = true
                }else if self.selectedIndex >= 4{
                    self.labelMainHeading.text = "MEDICAL HISTORY UPDATE"
                    self.labelHeading.text = "Does your child have any history of the following conditions (please select):"
                    self.extraView.isHidden = true
                }
                self.tableViewQuestions.reloadData()
                self.buttonBack?.isUserInteractionEnabled = true
                self.buttonNext.isUserInteractionEnabled = true
            }
            
        }
    }
    
    @IBAction func buttonVerifiedAction(withSender sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        
        textViewNotes.delegate?.textViewDidBeginEditing?(textViewNotes)
         patient.notesValue = textViewNotes.text
        textViewNotes.delegate?.textViewDidEndEditing?(textViewNotes)
        
        if buttonVerified.isSelected == false {
            self.showAlert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
        } else {
            if selectedIndex == 7 {
                let step2VC = self.storyboard?.instantiateViewController(withIdentifier: "kChildMedicalUpdateVC1") as! ChildMedicalUpdateVC1
                step2VC.patient = self.patient
                self.navigationController?.pushViewController(step2VC, animated: true)
                
            } else {
                
                buttonBack?.isUserInteractionEnabled = false
                buttonNext.isUserInteractionEnabled = false
                self.buttonVerified.isSelected = false
                self.activityIndicator.startAnimating()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.activityIndicator.stopAnimating()
                    self.selectedIndex = self.selectedIndex + 1
                   if self.selectedIndex == 3 {
                        self.labelMainHeading.text = "MEDICAL HISTORY"
                        self.labelHeading.text = "Is your child:"
                        self.extraView.isHidden = true
                    }else if self.selectedIndex >= 4{
                        self.labelMainHeading.text = "MEDICAL HISTORY"
                        self.labelHeading.text = "Does your child have any history of the following conditions (please select):"
                        self.extraView.isHidden = true
                    }
                    self.tableViewQuestions.reloadData()
                    self.buttonBack?.isUserInteractionEnabled = true
                    self.buttonNext.isUserInteractionEnabled = true
                }
            }
        }
    }
    
}

extension ChildMedicalUpdateVC : UITableViewDataSource,UITableViewDelegate {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patient.healthQues.HealthHistoryQues[selectedIndex].count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
     
        return 50
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PatientInfoCell
        cell.delegate = self
        cell.tag = indexPath.row
        cell.configCell(question: self.patient.healthQues.HealthHistoryQues[selectedIndex][indexPath.row])
        
        return cell
    }
}

extension ChildMedicalUpdateVC : PatientInfoCellDelegate{
    
    func radioButtonTappedForCell(cell: PatientInfoCell) {
        
       let indexPath = tableViewQuestions.indexPath(for: cell)!
        if selectedIndex == 3 && indexPath.row >= 1 {
            
            PopupTextField.popUpView().showInViewController(self, WithTitle: "If Yes,", placeHolder: "Please Explain", textFormat: .Default, completion: { (popUpView, textField) in
                
                if !textField.isEmpty {
                    self.patient.healthQues.HealthHistoryQues[self.selectedIndex][indexPath.row].answer = textField.text!
                    
                } else {
                    self.patient.healthQues.HealthHistoryQues[self.selectedIndex][indexPath.row].answer = ""
                    self.patient.healthQues.HealthHistoryQues[self.selectedIndex][indexPath.row].selectedOption = false
                    self.tableViewQuestions.reloadData()
                }
                
                popUpView.removeFromSuperview()
            })
 
        }else if selectedIndex == 7 && indexPath.row == 6{
            PopupTextField.popUpView().showInViewController(self, WithTitle: "", placeHolder: "PLEASE SPECIFY", textFormat: .Default, completion: { (popUpView, textField) in
                
                if !textField.isEmpty {
                    self.patient.healthQues.HealthHistoryQues[self.selectedIndex][indexPath.row].answer = textField.text!
                    
                } else {
                    self.patient.healthQues.HealthHistoryQues[self.selectedIndex][indexPath.row].answer = ""
                    self.patient.healthQues.HealthHistoryQues[self.selectedIndex][indexPath.row].selectedOption = false
                    self.tableViewQuestions.reloadData()
                }
                
                popUpView.removeFromSuperview()
            })

        }
        

}
}
