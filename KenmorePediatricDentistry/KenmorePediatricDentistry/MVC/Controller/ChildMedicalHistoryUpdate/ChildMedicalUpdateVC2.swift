//
//  ChildMedicalUpdateVC2.swift
//  KenmorePediatricDentistry
//
//  Created by Bala Murugan on 1/24/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class ChildMedicalUpdateVC2: MCViewController {

    @IBOutlet weak var textfieldParentName : MCTextField!
    @IBOutlet weak var signatureParent : SignatureView!
    @IBOutlet weak var labelDate1 : DateLabel!
    @IBOutlet weak var textView1 : MCTextView!
    @IBOutlet weak var textView2 : MCTextView!
    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate1.todayDate = patient.dateToday
        
        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue (){
       textView1.textValue =  patient.textViewElaborate
         textView2.textValue = patient.doesYourChild 
        textfieldParentName.text = patient.medicalGuardianName
    }
    
    func saveValue (){
        patient.textViewElaborate = textView1.textValue
        patient.doesYourChild  = textView2.textValue
        patient.medicalGuardianName = textfieldParentName.text
        patient.Signature1 = signatureParent.signatureImage()
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }

    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if textfieldParentName.isEmpty{
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        }else if !signatureParent.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate1.dateTapped {
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            saveValue()

            let updateForm = self.storyboard?.instantiateViewController(withIdentifier: "kChildMedicalUpdateForm") as! ChildMedicalUpdateForm
            updateForm.patient = self.patient
            self.navigationController?.pushViewController(updateForm, animated: true)

        }
    }


}
