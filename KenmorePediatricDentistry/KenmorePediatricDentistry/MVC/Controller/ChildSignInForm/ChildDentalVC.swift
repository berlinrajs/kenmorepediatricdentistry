//
//  PatientSignInStep3VC.swift
//  MConsentForms
//
//  Created by Berlin Raj on 22/09/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

import UIKit

class ChildDentalVC: MCViewController {

    @IBOutlet weak var radioPrimary: RadioButton!
    @IBOutlet weak var textFieldBYWHOM: MCTextField!
    @IBOutlet weak var textFieldPossibleCare: MCTextField!
    @IBOutlet weak var dropDownBrush: BRDropDown!
    @IBOutlet weak var dropDownFloss: BRDropDown!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        dropDownBrush.items = ["ONCE A DAY", "TWICE A DAY", "THREE TIMES A DAY","DON'T BRUSH"]
        dropDownBrush.placeholder = "-- PLEASE SELECT --"

        dropDownFloss.items = ["ONCE A WEEK", "TWICE A WEEK", "THREE TIMES A WEEK","DON'T FLOSS"]
        dropDownFloss.placeholder = "-- PLEASE SELECT --"
        autoFill()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction override func buttonBackAction() {
        saveValues()
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func autoFill(){
        radioPrimary.setSelectedWithTag(patient.radioGraphPrevOffice)
       dropDownBrush.selectedIndex =  patient.brushTag
        dropDownBrush.selectedOption =  patient.brushOption
        dropDownFloss.selectedIndex = patient.flossTag
        dropDownFloss.selectedOption =  patient.flossOption
         textFieldBYWHOM.text = patient.byWhom
         textFieldPossibleCare.text = patient.bestCareChild
    }
    
    func saveValues(){
        patient.radioGraphPrevOffice = radioPrimary.selected == nil ? 0 : radioPrimary.selected.tag
        patient.brushTag = dropDownBrush.selectedIndex
        patient.brushOption = dropDownBrush.selectedOption
        patient.flossTag = dropDownFloss.selectedIndex
        patient.flossOption = dropDownFloss.selectedOption
        patient.byWhom = textFieldBYWHOM.text
        patient.bestCareChild = textFieldPossibleCare.text
    }
    
    @IBAction func buttonNextAction() {
        
            saveValues()
        let nitrous = patientStoryBoard.instantiateViewController(withIdentifier: "DentalHistoryVC") as! DentalHistoryVC
        nitrous.patient = self.patient
        self.navigationController?.pushViewController(nitrous, animated: true)
        
        
        }
   
}
