//
//  PatientSignInStep3VC.swift
//  MConsentForms
//
//  Created by Berlin Raj on 22/09/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

import UIKit

class ChildMedicalVC: MCViewController {

    @IBOutlet weak var radioSecondary: RadioButton!
    @IBOutlet weak var textFieldChildPhysician: MCTextField!
    @IBOutlet weak var textFieldPhysicianPhone: MCTextField!
    @IBOutlet weak var textFieldExam: MCTextField!
    @IBOutlet weak var textFieldFindings: MCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        autoFill()
        textFieldExam.textFormat = .DateInCurrentYear
        textFieldPhysicianPhone.textFormat = .Phone
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction override func buttonBackAction() {
        saveValues()
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func autoFill(){
         textFieldChildPhysician.text = patient.childPhysician
         textFieldPhysicianPhone.text = patient.childPhysicianPhone
         textFieldExam.text = patient.childLastPhysicalExam
         textFieldFindings.text = patient.childFindings
        radioSecondary.setSelectedWithTag(patient.childPreviouslyUnderPhysicianCare)
    }
    
    func saveValues(){
        patient.childPreviouslyUnderPhysicianCare = radioSecondary.selected == nil ? 0 : radioSecondary.selected.tag
        patient.childPhysician = textFieldChildPhysician.text
        patient.childPhysicianPhone = textFieldPhysicianPhone.text
        patient.childLastPhysicalExam = textFieldExam.text
        patient.childFindings = textFieldFindings.text
    }
    
    @IBAction func buttonNextAction() {
        
        if !textFieldPhysicianPhone.isEmpty && !textFieldPhysicianPhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER A VALID PHONE NUMBER")
        }else{
            saveValues()
            let step2VC = self.storyboard?.instantiateViewController(withIdentifier: "ChildMedicalVC2") as! ChildMedicalVC2
            step2VC.patient = self.patient
            self.navigationController?.pushViewController(step2VC, animated: true)

            
            
        }
    
    }
}
