//
//  NitrousOxide1ViewController.swift
//  KenmorePediatricDentistry
//
//  Created by Bala Murugan on 1/24/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class ChildMedicalVC2: MCViewController {

    @IBOutlet weak var textfieldParentName : MCTextField!
    @IBOutlet weak var signatureParent : SignatureView!
    @IBOutlet weak var labelDate1 : DateLabel!
    @IBOutlet weak var textView1 : MCTextView!
    @IBOutlet weak var textView2 : MCTextView!
    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate1.todayDate = patient.dateToday
        
        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue (){
       textView1.textValue =  patient.textViewElaborate
         textView2.textValue = patient.doesYourChild 
        textfieldParentName.text = patient.medicalGuardianName
    }
    
    func saveValue (){
        patient.textViewElaborate = textView1.textValue
        patient.doesYourChild  = textView2.textValue
        patient.medicalGuardianName = textfieldParentName.text
        patient.Signature1 = signatureParent.signatureImage()
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }

    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if textfieldParentName.isEmpty{
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        }else if !signatureParent.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate1.dateTapped {
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            saveValue()
            
//            HipaaSignatureRefusalPopup.popUpView().showInViewController(viewController: self, completion: { (popUpView, refused, textFieldOther, refusalTag) in
//                popUpView.close()
//                self.patient.signRefused = refused
//                self.patient.signRefusalTag = refused == false ? 0 : refusalTag
//                self.patient.signRefusalOther = refused == false ? "" : refusalTag == 4 ? textFieldOther.text! : ""
//                let policy = consentStoryBoard1.instantiateViewController(withIdentifier: "acknowledgement") as! AcknowledgementOfPrivacyPractices1ViewController
//                policy.patient = self.patient
//                self.navigationController?.pushViewController(policy, animated: true)
//                
//                
//            }, error: { (message) in
//                self.showAlert(message)
//            })

            
            
//            let step2VC = self.storyboard?.instantiateViewController(withIdentifier: "ChildSignInForm") as! ChildSignInForm
//            step2VC.patient = self.patient
//            self.navigationController?.pushViewController(step2VC, animated: true)
            
            
            let silver = consentStoryBoard1.instantiateViewController(withIdentifier: "PrivacyPracticesVC") as! NoticeOfPrivacyPractices1ViewController
            silver.patient = self.patient
            self.navigationController?.pushViewController(silver, animated: true)

        }
    }


}
