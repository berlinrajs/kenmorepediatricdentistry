//
//  SilverDiamineFormViewController.swift
//  KenmorePediatricDentistry
//
//  Created by Bala Murugan on 1/25/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class ChildSignInForm: MCViewController {

    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelPreferedName : UILabel!
    @IBOutlet weak var signature : UIImageView!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var labelBdate : UILabel!
    @IBOutlet weak var labelAge : UILabel!
    @IBOutlet weak var labelSchool : UILabel!
    @IBOutlet weak var labelAddress : UILabel!
    @IBOutlet weak var labelCity : UILabel!
    @IBOutlet weak var labelState : UILabel!
    @IBOutlet weak var labelZip : UILabel!
    @IBOutlet weak var labelMainContact : UILabel!
    @IBOutlet weak var labelNoOfChildren : UILabel!
    @IBOutlet weak var labelsiblings : UILabel!
    
    //resp1
    @IBOutlet weak var labelfatherName : UILabel!
    @IBOutlet weak var labelRespBdate1 : UILabel!
    @IBOutlet weak var labelRespSSN1 : UILabel!
    @IBOutlet weak var labelRespEmail1 : UILabel!
    @IBOutlet weak var labelRespAddress1 : UILabel!
    @IBOutlet weak var labelRespCity1 : UILabel!
    @IBOutlet weak var labelRespState1 : UILabel!
    @IBOutlet weak var labelRespZip1 : UILabel!
    @IBOutlet weak var labelRespOccupation1 : UILabel!
    @IBOutlet weak var labelRespEmployer1 : UILabel!
    @IBOutlet weak var labelRespWorkPhone1 : UILabel!
    @IBOutlet weak var labelRespHomePhone1 : UILabel!
    @IBOutlet weak var labelRespCellPhone1 : UILabel!
    @IBOutlet weak var labelRespBestContact1 : UILabel!
    
    //resp2
    @IBOutlet weak var labelMotherName : UILabel!
    @IBOutlet weak var labelRespBdate2 : UILabel!
    @IBOutlet weak var labelRespSSN2 : UILabel!
    @IBOutlet weak var labelRespEmail2 : UILabel!
    @IBOutlet weak var labelRespAddress2 : UILabel!
    @IBOutlet weak var labelRespCity2 : UILabel!
    @IBOutlet weak var labelRespState2 : UILabel!
    @IBOutlet weak var labelRespZip2 : UILabel!
    @IBOutlet weak var labelRespOccupation2 : UILabel!
    @IBOutlet weak var labelRespEmployer2 : UILabel!
    @IBOutlet weak var labelRespWorkPhone2 : UILabel!
    @IBOutlet weak var labelRespHomePhone2 : UILabel!
    @IBOutlet weak var labelRespCellPhone2 : UILabel!
    @IBOutlet weak var labelRespBestContact2 : UILabel!

    @IBOutlet weak var labelChildReside : UILabel!
    @IBOutlet weak var labelChildRefered : UILabel!
    
    @IBOutlet weak var labelPrimaryInsuredName: UILabel!
    @IBOutlet weak var labelPrimaryRelation : UILabel!
    @IBOutlet weak var labelPrimaryBdate : UILabel!
    @IBOutlet weak var labelPriamrySSN : UILabel!
    @IBOutlet weak var labelPriamaryDateEmployed : UILabel!
    @IBOutlet weak var labelPrimaryEmployer : UILabel!
    @IBOutlet weak var labelPrimaryworkPhone : UILabel!
    @IBOutlet weak var labelPrimaryInsurCompany : UILabel!
    @IBOutlet weak var labelPrimaryGroup : UILabel!
    @IBOutlet weak var labelPrimaryAddress : UILabel!
    @IBOutlet weak var labelPrimaryCity : UILabel!
    @IBOutlet weak var labelPrimaryState : UILabel!
    @IBOutlet weak var labelPrimaryZip : UILabel!
    
    @IBOutlet weak var labelPrimaryInsuredName1: UILabel!
    @IBOutlet weak var labelPrimaryRelation1 : UILabel!
    @IBOutlet weak var labelPrimaryBdate1 : UILabel!
    @IBOutlet weak var labelPriamrySSN1 : UILabel!
    @IBOutlet weak var labelPriamaryDateEmployed1 : UILabel!
    @IBOutlet weak var labelPrimaryEmployer1 : UILabel!
    @IBOutlet weak var labelPrimaryworkPhone1 : UILabel!
    @IBOutlet weak var labelPrimaryInsurCompany1 : UILabel!
    @IBOutlet weak var labelPrimaryGroup1 : UILabel!
    @IBOutlet weak var labelPrimaryAddress1: UILabel!
    @IBOutlet weak var labelPrimaryCity1 : UILabel!
    @IBOutlet weak var labelPrimaryState1 : UILabel!
    @IBOutlet weak var labelPrimaryZip1 : UILabel!
    
    @IBOutlet var radioArray1: [RadioButton]!
    @IBOutlet var radioArray2: [RadioButton]!
    @IBOutlet var radioArray3: [RadioButton]!
    @IBOutlet var radioArray4: [RadioButton]!
    @IBOutlet var radioArray5: [RadioButton]!
    @IBOutlet var radioArray6: [RadioButton]!
    @IBOutlet var radioArray7: [RadioButton]!
    @IBOutlet var radioArray8: [RadioButton]!
    
    @IBOutlet weak var array01Answer : UILabel!
    @IBOutlet weak var array01Second_Answer : UILabel!
    @IBOutlet weak var array02Answer : UILabel!
    @IBOutlet weak var array03Answer : UILabel!
    @IBOutlet weak var array04Answer : UILabel!
    @IBOutlet weak var array05Answer : UILabel!
    @IBOutlet weak var array06Answer : UILabel!
    @IBOutlet weak var array010Answer : UILabel!
    @IBOutlet weak var array16Answer : UILabel!
    @IBOutlet weak var array31Answer : UILabel!
    @IBOutlet weak var array32Answer : UILabel!
    @IBOutlet weak var array33Answer : UILabel!
     @IBOutlet weak var array34Answer : UILabel!
    
    
    @IBOutlet weak var radioGender: RadioButton!
    @IBOutlet weak var response1: RadioButton!
    @IBOutlet weak var response2: RadioButton!
    @IBOutlet weak var maritalStatus1: RadioButton!
    @IBOutlet weak var maritalStatus2: RadioButton!
    @IBOutlet weak var childUnderPhysicianCare: RadioButton!
    
    @IBOutlet weak var doesYourChildPblm: UILabel!
    @IBOutlet weak var elaboratePblm : UILabel!
    @IBOutlet weak var childPhysicianName : UILabel!
    @IBOutlet weak var physicianPhone : UILabel!
    @IBOutlet weak var lastPhysicalExam : UILabel!
    @IBOutlet weak var findings : UILabel!
    
    @IBOutlet weak var teethBrushed : UILabel!
    @IBOutlet weak var teethFloss : UILabel!
    @IBOutlet weak var brushWhom : UILabel!
    @IBOutlet weak var bestPossibleCare : UILabel!
    
    @IBOutlet weak var labelMedicalHistoryOther : UILabel!
    @IBOutlet weak var buttonRadiographs : RadioButton!
    @IBOutlet weak var labelGuardianName : UILabel!
    
    //acknowledge and office policy
    @IBOutlet weak var labelDateOfficePolicy: UILabel!
    @IBOutlet weak var signOfficePolicy: UIImageView!
    @IBOutlet weak var textFieldDependentFamily: UILabel!
    @IBOutlet weak var labelRelation: FormLabel!
    @IBOutlet weak var labelDate4: FormLabel!
    @IBOutlet weak var labelPatientName4: FormLabel!
    @IBOutlet weak var signImage: UIImageView!
    @IBOutlet weak var labelRefuslaOther : UILabel!
    @IBOutlet weak var radioRefusal: RadioButton!
    
    //notice of privacy
    @IBOutlet weak var labelPatientName1 : UILabel!
    @IBOutlet weak var patientSignatureForm:
    UIImageView!
    @IBOutlet weak var labelDate3: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadValues()
        self.acknowledFunc()
        self.noticeFunc()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func loadValues(){
    
    labelPatientName.text = patient.fullName
    labelPreferedName.text = patient.preferredName
    labelBdate.text = patient.dateOfBirth
    labelAge.text = String(patient.patientAge)
    labelSchool.text = patient.childSchool
    labelAddress.text = patient.childAddress
    labelCity.text = patient.childCity
    labelState.text = patient.childState
    labelZip.text = patient.childZip
    labelMainContact.text = patient.childMainContact
    labelNoOfChildren.text = patient.childNumberOfChildren
    labelsiblings.text = patient.childSiblings
    
        //resp1
        labelfatherName.text = patient.childRespFatherName
        labelRespBdate1.text = patient.childRespBdate1
        labelRespSSN1.text = patient.childRespSSN1
        labelRespEmail1.text = patient.childRespEmail1
        labelRespAddress1.text = patient.childRespAddress1
        labelRespCity1.text = patient.childRespCity1
        labelRespState1.text = patient.childRespState1
        labelRespZip1.text = patient.childRespZipCode1
        labelRespOccupation1.text = patient.childRespOccupation1
        labelRespEmployer1.text = patient.childRespEmployer1
        labelRespWorkPhone1.text = patient.childRespWorkPhone1
        labelRespHomePhone1.text = patient.childRespHomePhone1
        labelRespCellPhone1.text = patient.childRespCellPhone1
        labelRespBestContact1.text = patient.childRespMainContact1
        //resp2
        labelMotherName.text = patient.childRespMotherName
        labelRespBdate2.text = patient.childRespBdate2
        labelRespSSN2.text = patient.childRespSSN2
        labelRespEmail2.text = patient.childRespEmail2
        labelRespAddress2.text = patient.childRespAddress2
        labelRespCity2.text = patient.childRespCity2
        labelRespState2.text = patient.childRespState2
        labelRespZip2.text = patient.childRespZipCode2
        labelRespOccupation2.text = patient.childRespOccupation2
        labelRespEmployer2.text = patient.childRespEmployer2
        labelRespWorkPhone2.text = patient.childRespWorkPhone2
        labelRespHomePhone2.text = patient.childRespHomePhone2
        labelRespCellPhone2.text = patient.childRespCellPhone2
        labelRespBestContact2.text = patient.childRespMainContact2
    
    labelChildReside.text = patient.withWhomChildReside
    labelChildRefered.text = patient.howDidYouHearOffice
        
        labelPrimaryInsuredName.text = patient.primaryInsuredName
        labelPrimaryRelation.text = patient.primaryRelationValue
        labelPrimaryBdate.text = patient.primaryBdate
        labelPriamrySSN.text = patient.primarySSN
        labelPriamaryDateEmployed.text = patient.primaryDateEmployed
        labelPrimaryEmployer.text = patient.primaryEmployer
        labelPrimaryworkPhone.text = patient.primaryWorkPhone
        labelPrimaryInsurCompany.text = patient.primaryInsurCompany
        labelPrimaryGroup.text = patient.primaryGroup
        labelPrimaryAddress.text = patient.primaryAddress
        labelPrimaryCity.text = patient.primaryCity
        labelPrimaryState.text = patient.primaryState
        labelPrimaryZip.text = patient.primaryZip
        
        labelPrimaryInsuredName1.text = patient.secondaryInsuredName
        labelPrimaryRelation1.text = patient.secondaryRelationValue
        labelPrimaryBdate1.text = patient.secondaryBdate
        labelPriamrySSN1.text = patient.secondarySSN
        labelPriamaryDateEmployed1.text = patient.secondaryDateEmployed
        labelPrimaryEmployer1.text = patient.secondaryEmployer
        labelPrimaryworkPhone1.text = patient.secondaryWorkPhone
        labelPrimaryInsurCompany1.text = patient.secondaryInsurCompany
        labelPrimaryGroup1.text = patient.secondaryGroup
        labelPrimaryAddress1.text = patient.secondaryAddress
        labelPrimaryCity1.text = patient.secondaryCity
        labelPrimaryState1.text = patient.secondaryState
        labelPrimaryZip1.text = patient.secondaryZip

        for btn in radioArray1{
            
            btn.isSelected = patient.healthQues.HealthHistoryQues[0][btn.tag].selectedOption
        }
        
        for btn in radioArray2{
            
            btn.isSelected = patient.healthQues.HealthHistoryQues[1][btn.tag].selectedOption
        }
        
        for btn in radioArray3{
            
            btn.isSelected = patient.healthQues.HealthHistoryQues[2][btn.tag].selectedOption
        }
        
        for btn in radioArray4{
            
            btn.isSelected = patient.healthQues.HealthHistoryQues[3][btn.tag].selectedOption
        }
        
        for btn in radioArray5{
            
            btn.isSelected = patient.healthQues.HealthHistoryQues[4][btn.tag].selectedOption
        }
        
        for btn in radioArray6{
            
            btn.isSelected = patient.healthQues.HealthHistoryQues[5][btn.tag].selectedOption
        }
        
        for btn in radioArray7{
            
            btn.isSelected = patient.healthQues.HealthHistoryQues[6][btn.tag].selectedOption
        }
        
        for btn in radioArray8{
            
            btn.isSelected = patient.healthQues.HealthHistoryQues[7][btn.tag].selectedOption
        }
        
        array01Answer.text = patient.healthQues.HealthHistoryQues[0][1].answer
        array01Second_Answer.text = patient.healthQues.HealthHistoryQues[0][1].second_Answer
        array02Answer.text = patient.healthQues.HealthHistoryQues[0][2].answer
        array03Answer.text = patient.healthQues.HealthHistoryQues[0][3].answer
        array04Answer.text = patient.healthQues.HealthHistoryQues[0][4].answer
        array05Answer.text = patient.healthQues.HealthHistoryQues[0][5].answer
        array06Answer.text = patient.healthQues.HealthHistoryQues[0][6].answer
        array010Answer.text = patient.healthQues.HealthHistoryQues[0][10].answer
        
        array16Answer.text = patient.healthQues.HealthHistoryQues[1][6].answer
        array31Answer.text = patient.healthQues.HealthHistoryQues[3][1].answer
        array32Answer.text = patient.healthQues.HealthHistoryQues[3][2].answer
        array33Answer.text = patient.healthQues.HealthHistoryQues[3][3].answer
        array34Answer.text = patient.healthQues.HealthHistoryQues[3][4].answer
        
        signature.image = patient.Signature1
        labelDate1.text = patient.dateToday
        
        doesYourChildPblm.text = patient.doesYourChild
        elaboratePblm.text = patient.textViewElaborate
        childPhysicianName.text = patient.childPhysician
        physicianPhone.text = patient.childPhysicianPhone
        lastPhysicalExam.text = patient.childLastPhysicalExam
        findings.text = patient.childFindings
        
        teethBrushed.text = patient.brushOption
        teethFloss.text = patient.flossOption
        brushWhom.text = patient.byWhom
        bestPossibleCare.text = patient.bestCareChild
        
        radioGender.setSelectedWithTag(patient.childGender)
        response1.setSelectedWithTag(patient.childRespRelation1)
        response2.setSelectedWithTag(patient.childRespRelation2)
        maritalStatus1.setSelectedWithTag(patient.childRespMaritalStatus1)
        maritalStatus2.setSelectedWithTag(patient.childRespMaritalStatus2)
        childUnderPhysicianCare.setSelectedWithTag(patient.childPreviouslyUnderPhysicianCare)
        
        labelMedicalHistoryOther.text = patient.healthQues.HealthHistoryQues[7][6].answer
        buttonRadiographs.setSelectedWithTag(patient.radioGraphPrevOffice)
        labelGuardianName.text = patient.medicalGuardianName
    }
    
    func noticeFunc(){
        labelDate3.text = patient.dateToday
        patientSignatureForm.image = patient.signPatient
        labelPatientName1.text = patient.fullName
        
    }
    
    func acknowledFunc(){
        if patient.signRefused == true{
            labelPatientName4.text = patient.fullName
            labelDate4.text = patient.dateToday
            radioRefusal.setSelectedWithTag(patient.signRefusalTag)
            labelRefuslaOther.text = patient.signRefusalOther
        }else{
            labelRelation.text = patient.relation1
            labelDate4.text = patient.dateToday
            labelPatientName4.text = patient.fullName
            signImage.image = patient.Signature1
            textFieldDependentFamily.text = patient.dependentFamily1
            
        }
        labelDateOfficePolicy.text = patient.dateToday
        signOfficePolicy.image = patient.signPolicy
        
    }
    
    override func onSubmitButtonPressed() {
        
        #if AUTO
            if !Reachability.isConnectedToNetwork() {
                let alertController = UIAlertController(title: kAppName, message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.alert)
                let alertOkAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.destructive) { (action) -> Void in
                    let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                    if let url = settingsUrl {
                        UIApplication.shared.openURL(url as URL)
                    }
                }
                let alertCancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive) { (action) -> Void in
                    
                }
                alertController.addAction(alertOkAction)
                alertController.addAction(alertCancelAction)
                self.present(alertController, animated: true, completion: nil)
                return
            }
            BRProgressHUD.show()
            patient.isChild = true
            ServiceManager.sendPatientDetails(patient: patient, completion: { (success, error) in
                BRProgressHUD.hide()
                if success {
                    super.onSubmitButtonPressed()
                } else {
                    self.showAlert(error!.localizedDescription.uppercased())
                }
            })
        #else
            super.onSubmitButtonPressed()
        #endif
    }



}
