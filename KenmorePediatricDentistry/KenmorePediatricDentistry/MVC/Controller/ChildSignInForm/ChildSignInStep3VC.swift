//
//  PatientSignInStep3VC.swift
//  MConsentForms
//
//  Created by Berlin Raj on 22/09/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

import UIKit

class ChildSignInStep3VC: MCViewController {

    @IBOutlet weak var radioPrimary: RadioButton!
    @IBOutlet weak var radioSecondary: RadioButton!
    @IBOutlet weak var textFieldChildReside: MCTextField!
    @IBOutlet weak var textFieldChildReferred: MCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        autoFill()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction override func buttonBackAction() {
        saveValues()
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func autoFill(){
        textFieldChildReside.text = patient.withWhomChildReside
        textFieldChildReferred.text = patient.howDidYouHearOffice
        radioPrimary.setSelectedWithTag(patient.primaryTag)
        radioSecondary.setSelectedWithTag(patient.secondaryTag)
        
    }
    
    func saveValues(){
        patient.withWhomChildReside = textFieldChildReside.text
        patient.howDidYouHearOffice = textFieldChildReferred.text
        patient.primaryTag = radioPrimary.selected == nil ? 0 : radioPrimary.selected.tag
        patient.secondaryTag = radioSecondary.selected == nil ? 0 : radioSecondary.selected.tag
    }
    
    @IBAction func buttonNextAction() {
         if radioPrimary.selected == nil {
            self.showAlert("PLEASE SELECT ALL THE REQUIRED")
        } else if radioSecondary.selected == nil {
            self.showAlert("PLEASE SELECT ALL THE REQUIRED")
        } else if radioPrimary.isSelected == false && radioSecondary.isSelected == true {
            self.showAlert("SECONDARY INSURANCE CANNOT BE AVAILABLE WITHOUR PRIMARY")
        } else {
            saveValues()
            patient.childPrimaryInsurance = radioPrimary.selected.tag
            patient.childSecondaryInsurance = radioSecondary.selected.tag
            
             if patient.childPrimaryInsurance == 1 {
                let nitrous = patientStoryBoard.instantiateViewController(withIdentifier: "PrimaryInsurVC1") as! PrimaryInsurVC1
                nitrous.patient = self.patient
                self.navigationController?.pushViewController(nitrous, animated: true)

             } else if patient.childSecondaryInsurance == 1 {
                let nitrous = patientStoryBoard.instantiateViewController(withIdentifier: "SecondaryInsurVC1") as! SecondaryInsurVC1
                nitrous.patient = self.patient
                self.navigationController?.pushViewController(nitrous, animated: true)

            } else {
                let nitrous = patientStoryBoard.instantiateViewController(withIdentifier: "ChildDentalVC") as! ChildDentalVC
                nitrous.patient = self.patient
                self.navigationController?.pushViewController(nitrous, animated: true)
                
            }
        }
    }
}
