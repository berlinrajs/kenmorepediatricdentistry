//
//  PatientSignInStep2VC.swift
//  MConsentForms
//
//  Created by Berlin Raj on 22/09/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

import UIKit

class PatientAddressVC: MCViewController {

    @IBOutlet weak var textFieldAddress: MCTextField!
    @IBOutlet weak var textFieldCity: MCTextField!
    @IBOutlet weak var textFieldState: MCTextField!
    @IBOutlet weak var textFieldZip: MCTextField!
    @IBOutlet weak var textFieldSchool: MCTextField!
    @IBOutlet weak var textFieldMainContact: MCTextField!
    @IBOutlet weak var textFieldNumberOfChild: MCTextField!
    @IBOutlet weak var textFieldSiblings: MCTextField!
    @IBOutlet weak var radioGender: RadioButton!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        textFieldState.textFormat = .State
        textFieldZip.textFormat = .Zipcode
        textFieldNumberOfChild.textFormat = .Number
        textFieldMainContact.textFormat = .Phone
        autoFill()
        // Do any additional setup after loading the view....
    }

       @IBAction override func buttonBackAction() {
            saveValues()
         _ = self.navigationController?.popViewController(animated: true)
    }

    func autoFill(){
        
        
        textFieldSchool.text = patient.childSchool
        textFieldNumberOfChild.text = patient.childNumberOfChildren
        textFieldSiblings.text = patient.childSiblings
        
        
        func loadMan(){
            textFieldAddress.text =  patient.childAddress
            textFieldCity.text =   patient.childCity
            textFieldState.text =  patient.childState
            textFieldZip.text =  patient.childZip
            radioGender.setSelectedWithTag(patient.childGender)
            textFieldMainContact.text = patient.childMainContact
        }
        
        
        #if AUTO
            if let patientDetails = patient.patientDetails {

                if let gender = patientDetails.gender {
                    radioGender.setSelectedWithTag(gender.index)
                }

                textFieldMainContact.text = patient.childMainContact != nil ? patient.childMainContact : patientDetails.homePhone
                textFieldCity.text = patient.childCity != nil ? patient.childCity : patientDetails.city
                textFieldState.text = patient.childState != nil ? patient.childState : patientDetails.state
                textFieldZip.text = patient.childZip != nil ? patient.childZip : patientDetails.zipCode
                textFieldAddress.text = patient.childAddress != nil ? patient.childAddress : patientDetails.address
                
            } else {
                loadMan()
            }
        #else
            loadMan()
        #endif

    }
    
    func saveValues(){
        patient.childAddress = textFieldAddress.text
        patient.childCity = textFieldCity.text
        patient.childState = textFieldState.text
        patient.childZip = textFieldZip.text
        patient.childMainContact = textFieldMainContact.text
        patient.childSchool = textFieldSchool.text
        patient.childNumberOfChildren = textFieldNumberOfChild.text
        patient.childSiblings = textFieldSiblings.text
        patient.childGender = radioGender.selected == nil ? 0 : radioGender.selected.tag
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func buttonNextAction() {
        self.view.endEditing(true)
        if textFieldAddress.isEmpty  || textFieldState.isEmpty {
            self.showAlert("PLEASE ENTER A VALID ADDRESS")
        }else if textFieldCity.isEmpty{
            self.showAlert("PLEASE ENTER A CITY")
        }else if textFieldZip.isEmpty{
            self.showAlert("PLEASE ENTER A ZIPCODE")
        }else if !textFieldZip.text!.isZipCode {
            self.showAlert("PLEASE ENTER A VALID ZIPCODE")
        }else if !textFieldMainContact.isEmpty && !textFieldMainContact.text!.isPhoneNumber {
            self.showAlert("PLEASE ENTER A VALID PHONE NUMBER")
        }else if radioGender.selected == nil{
            self.showAlert("PLEASE SELECT GENDER")
        } else {
            saveValues()
            let nitrous = patientStoryBoard.instantiateViewController(withIdentifier: "PatientRespVC1") as! PatientRespVC1
            nitrous.patient = self.patient
            self.navigationController?.pushViewController(nitrous, animated: true)
          
        }
    }
    
}
