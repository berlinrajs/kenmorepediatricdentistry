//
//  PatientSignInStep5VC.swift
//  MConsentForms
//
//  Created by Berlin Raj on 22/09/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

import UIKit

class PatientResp2VC1: MCViewController {

  
    @IBOutlet weak var dropDownRelation: BRDropDown!
    @IBOutlet weak var textFieldDateOfBirth: MCTextField!
    @IBOutlet weak var textFieldSocialSecurity: MCTextField!
    @IBOutlet weak var textFieldFatherName: MCTextField!
    @IBOutlet weak var textFieldAddress: MCTextField!
    @IBOutlet weak var textFieldCity: MCTextField!
    @IBOutlet weak var textFieldState: MCTextField!
    @IBOutlet weak var textFieldZip: MCTextField!
    
    @IBOutlet weak var labelInsuranceTitle: UILabel!
    
    var otherRelation: String = ""
    override func viewDidLoad() {
        
        super.viewDidLoad()
        dropDownRelation.items = ["Guardian", "Stepmother", "Other"]
        dropDownRelation.placeholder = "-- RELATIONSHIP TO PATIENT --"
        dropDownRelation.delegate = self
        
        textFieldDateOfBirth.textFormat = .DateIn1980
        textFieldSocialSecurity.textFormat = .SocialSecurity
        textFieldState.textFormat = .State
        textFieldZip.textFormat = .Zipcode
        autoFill()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated..
    }
    
    @IBAction override func buttonBackAction() {
        saveValues()
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func autoFill(){
        dropDownRelation.selectedIndex =  patient.childRespRelation2
        textFieldFatherName.text = patient.childRespMotherName
        textFieldDateOfBirth.text = patient.childRespBdate2
        textFieldSocialSecurity.text = patient.childRespSSN2
        textFieldAddress.text = patient.childRespAddress2
        textFieldCity.text = patient.childRespCity2
        textFieldState.text = patient.childRespState2
        textFieldZip.text = patient.childRespZipCode2
        
    }
    
    func saveValues(){
        patient.childRespRelation2 = dropDownRelation.selectedIndex
        patient.childRespMotherName = textFieldFatherName.text
        patient.childRespBdate2 = textFieldDateOfBirth.text
        patient.childRespSSN2 = textFieldSocialSecurity.text
        patient.childRespAddress2 = textFieldAddress.text
        patient.childRespCity2 = textFieldCity.text
        patient.childRespState2 = textFieldState.text
        patient.childRespZipCode2 = textFieldZip.text
        
    }
    
    
    @IBAction func buttonNextAction () {
        self.view.endEditing(true)
        dropDownRelation.selected = false
        if !textFieldSocialSecurity.isEmpty && !textFieldSocialSecurity.text!.isSocialSecurityNumber{
            self.showAlert("PLEASE ENTER A VALID SOCIAL SECURITY NUMBER")
        }else if !textFieldZip.isEmpty && !textFieldZip.text!.isZipCode{
            self.showAlert("PLEASE ENTER A VALID ZIPCODE")
        } else {
            saveValues()
            let nitrous = patientStoryBoard.instantiateViewController(withIdentifier: "PatientResp2VC2") as! PatientResp2VC2
            nitrous.patient = self.patient
            self.navigationController?.pushViewController(nitrous, animated: true)
        }

    }
}
extension PatientResp2VC1: BRDropDownDelegate {
    func dropDown(_ dropDown: BRDropDown, selectedAtIndex index: Int, selectedOption option: String?) {
      
        if index == 3 {
            PopupTextField.popUpView().showInViewController(self, WithTitle: "PLEASE SPECIFY THE RELATION", placeHolder: "RELATIONSHIP TO PATIENT *", textFormat: TextFormat.Default, completion: { (popupView, textField) in
                if textField.isEmpty {
                    self.dropDownRelation.reset()
                    self.otherRelation = ""
                } else {
                    self.otherRelation = textField.text!
                }
                popupView.close()
            })
        } else {
            self.otherRelation = option == nil ? "" : option!.uppercased()
            
        }
    }
}
