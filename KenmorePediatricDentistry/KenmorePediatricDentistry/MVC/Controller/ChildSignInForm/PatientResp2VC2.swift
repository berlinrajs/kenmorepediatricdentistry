//
//  PatientSignInStep5VC.swift
//  MConsentForms
//
//  Created by Berlin Raj on 22/09/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

import UIKit

class PatientResp2VC2: MCViewController {
    
    
    @IBOutlet weak var dropDownMarital: BRDropDown!
    @IBOutlet weak var textFieldOccupation: MCTextField!
    @IBOutlet weak var textFieldEmployer: MCTextField!
    @IBOutlet weak var textFieldWorkPhone: MCTextField!
    @IBOutlet weak var textFieldHomePhone: MCTextField!
    @IBOutlet weak var textFieldCellPhone: MCTextField!
    @IBOutlet weak var textFieldBestContact: MCTextField!
    @IBOutlet weak var textFieldEmail: MCTextField!
    @IBOutlet weak var labelInsuranceTitle: UILabel!
    
    var otherRelation: String = ""
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        textFieldWorkPhone.textFormat = .Phone
        textFieldHomePhone.textFormat = .Phone
        textFieldCellPhone.textFormat = .Phone
        textFieldBestContact.textFormat = .Phone
        textFieldEmail.textFormat = .Email
        dropDownMarital.items = ["Married", "Single", "Divorced", "Separated","Widowed"]
        dropDownMarital.placeholder = "-- MARITAL STATUS --"
        
        autoFill()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated..
    }
    
    @IBAction override func buttonBackAction() {
        saveValues()
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    func autoFill(){
        textFieldOccupation.text = patient.childRespOccupation2
        textFieldEmployer.text = patient.childRespEmployer2
        textFieldWorkPhone.text = patient.childRespWorkPhone2
        textFieldHomePhone.text = patient.childRespHomePhone2
        textFieldCellPhone.text = patient.childRespCellPhone2
        textFieldBestContact.text = patient.childRespMainContact2
        dropDownMarital.selectedIndex =  patient.childRespMaritalStatus2
        textFieldEmail.text = patient.childRespEmail2
    }
    
    func saveValues(){
        patient.childRespOccupation2 = textFieldOccupation.text
        patient.childRespEmployer2 = textFieldEmployer.text
        patient.childRespWorkPhone2 = textFieldWorkPhone.text
        patient.childRespHomePhone2 = textFieldHomePhone.text
        patient.childRespCellPhone2 = textFieldCellPhone.text
        patient.childRespMainContact2 = textFieldBestContact.text
        patient.childRespMaritalStatus2 = dropDownMarital.selectedIndex
        patient.childRespEmail2 = textFieldEmail.text
    }
    
    
    @IBAction func buttonNextAction () {
        self.view.endEditing(true)
        dropDownMarital.selected = false
        if !textFieldWorkPhone.isEmpty && !textFieldWorkPhone.text!.isPhoneNumber {
            self.showAlert("PLEASE ENTER A VALID WORK PHONE NUMBER")
        }else if !textFieldHomePhone.isEmpty && !textFieldHomePhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER A VALID HOME PHONE NUMBER")
        }else if !textFieldCellPhone.isEmpty && !textFieldCellPhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER A VALID CELL PHONE NUMBER")
        }else if !textFieldBestContact.isEmpty && !textFieldBestContact.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER A VALID BEST CONTACT PHONE NUMBER")
        }else if !textFieldEmail.isEmpty && !textFieldEmail.text!.isValidEmail{
            self.showAlert("PLEASE ENTER A VALID EMAIL")
        } else {
            saveValues()
            let nitrous = patientStoryBoard.instantiateViewController(withIdentifier: "ChildSignInStep3VC") as! ChildSignInStep3VC
            nitrous.patient = self.patient
            self.navigationController?.pushViewController(nitrous, animated: true)

        }
    }
}
