//
//  PatientSignInStep5VC.swift
//  MConsentForms
//
//  Created by Berlin Raj on 22/09/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

import UIKit

class PrimaryInsurVC2: MCViewController {


    @IBOutlet weak var textFieldInsurCompany: MCTextField!
    @IBOutlet weak var textFieldGroup: MCTextField!
    @IBOutlet weak var textFieldAddress: MCTextField!
    @IBOutlet weak var textFieldCity: MCTextField!
    @IBOutlet weak var textFieldState: MCTextField!
    @IBOutlet weak var textFieldZip: MCTextField!
 
    @IBOutlet weak var labelInsuranceTitle: UILabel!
    
    var otherRelation: String = ""

    override func viewDidLoad() {
        
        super.viewDidLoad()
       
     textFieldGroup.textFormat = .AlphaNumeric
    textFieldState.textFormat = .State
    textFieldZip.textFormat = .Zipcode
        autoFill()
            // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated..
    }
    
    @IBAction override func buttonBackAction() {
        saveValues()
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func autoFill(){
        textFieldInsurCompany.text =  patient.primaryInsurCompany
        textFieldGroup.text = patient.primaryGroup
        textFieldAddress.text = patient.primaryAddress
         textFieldCity.text = patient.primaryCity
         textFieldState.text = patient.primaryState
         textFieldZip.text = patient.primaryZip

    }
    
    func saveValues(){

        patient.primaryInsurCompany = textFieldInsurCompany.text
        patient.primaryGroup = textFieldGroup.text
        patient.primaryAddress = textFieldAddress.text
        patient.primaryCity = textFieldCity.text
        patient.primaryState = textFieldState.text
        patient.primaryZip = textFieldZip.text
    }

    
    @IBAction func buttonNextAction () {
        self.view.endEditing(true)
        if !textFieldZip.isEmpty && !textFieldZip.text!.isZipCode {
            self.showAlert("PLEASE ENTER A VALID VALID ZIP CODE")
        } else {
         saveValues()
        
            if patient.childSecondaryInsurance == 1 {
                let nitrous = patientStoryBoard.instantiateViewController(withIdentifier: "SecondaryInsurVC1") as! SecondaryInsurVC1
                nitrous.patient = self.patient
                self.navigationController?.pushViewController(nitrous, animated: true)
                
            } else {
                let nitrous = patientStoryBoard.instantiateViewController(withIdentifier: "ChildDentalVC") as! ChildDentalVC
                nitrous.patient = self.patient
                self.navigationController?.pushViewController(nitrous, animated: true)
                
            }
        }
    }
}

