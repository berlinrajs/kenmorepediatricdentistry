//
//  PatientSignInStep5VC.swift
//  MConsentForms
//
//  Created by Berlin Raj on 22/09/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

import UIKit

class SecondaryInsurVC1: MCViewController {

    
    @IBOutlet weak var dropDownRelation: BRDropDown!
    @IBOutlet weak var textFieldNameOfInsured: MCTextField!
    @IBOutlet weak var textFieldDateOfBirth: MCTextField!
    @IBOutlet weak var textFieldSSN: MCTextField!
    @IBOutlet weak var textFieldDateEmployed: MCTextField!
    @IBOutlet weak var textFieldEmployer: MCTextField!
    @IBOutlet weak var textFieldWorkPhone: MCTextField!
    @IBOutlet weak var labelInsuranceTitle: UILabel!
    
    var otherRelation: String!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        dropDownRelation.items = ["Self", "Guardian", "Other"]
        dropDownRelation.placeholder = "-- RELATIONSHIP TO PATIENT --"
        dropDownRelation.delegate = self
        
        textFieldDateOfBirth.textFormat = .DateIn1980
        textFieldSSN.textFormat = .SocialSecurity
        textFieldDateEmployed.textFormat = .DateInCurrentYear
        textFieldWorkPhone.textFormat = .Phone
        autoFill()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated..
    }
    
    @IBAction override func buttonBackAction() {
        saveValues()
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func autoFill(){
        textFieldNameOfInsured.text = patient.secondaryInsuredName
        dropDownRelation.selectedIndex = patient.secondaryRelationTag
        dropDownRelation.selectedOption =  patient.secondaryRelationValue
        self.otherRelation = patient.secondaryRelationOther
        textFieldDateOfBirth.text = patient.secondaryBdate
        textFieldSSN.text = patient.secondarySSN
        textFieldDateEmployed.text = patient.secondaryDateEmployed
        textFieldEmployer.text = patient.secondaryEmployer
        textFieldWorkPhone.text = patient.secondaryWorkPhone
    }
    
    func saveValues(){
        
        patient.secondaryInsuredName = textFieldNameOfInsured.text
        patient.secondaryRelationTag = dropDownRelation.selectedIndex
        patient.secondaryRelationValue = dropDownRelation.selectedOption
        patient.secondaryRelationOther = self.otherRelation
        patient.secondaryBdate = textFieldDateOfBirth.text
        patient.secondarySSN = textFieldSSN.text
        patient.secondaryDateEmployed = textFieldDateEmployed.text
        patient.secondaryEmployer = textFieldEmployer.text
        patient.secondaryWorkPhone = textFieldWorkPhone.text
    }
    
    
    @IBAction func buttonNextAction () {
        self.view.endEditing(true)
        dropDownRelation.selected = false
        if !textFieldSSN.isEmpty && !textFieldSSN.text!.isSocialSecurityNumber {
            self.showAlert("PLEASE ENTER A VALID SOCIAL SECURITY NUMBER")
        }else if !textFieldWorkPhone.isEmpty && !textFieldWorkPhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER A VALID WORK PHONE NUMBER")
        } else {
            saveValues()
            let nitrous = patientStoryBoard.instantiateViewController(withIdentifier: "SecondaryInsurVC2") as! SecondaryInsurVC2
            nitrous.patient = self.patient
            self.navigationController?.pushViewController(nitrous, animated: true)
        }
    }
}
extension SecondaryInsurVC1: BRDropDownDelegate {
    func dropDown(_ dropDown: BRDropDown, selectedAtIndex index: Int, selectedOption option: String?) {
        if index == 1{
            textFieldNameOfInsured.text = patient.fullName
            textFieldDateOfBirth.text = patient.dateOfBirth
        }else{
            textFieldNameOfInsured.text = ""
            textFieldDateOfBirth.text = ""
            
        }

        if index == 3 {
            
            PopupTextField.popUpView().showInViewController(self, WithTitle: "PLEASE SPECIFY THE RELATION", placeHolder: "RELATIONSHIP TO PATIENT *", textFormat: TextFormat.Default, completion: { (popupView, textField) in
                if textField.isEmpty {
                    self.dropDownRelation.reset()
                    self.otherRelation = ""
                } else {
                    self.otherRelation = textField.text!
                }
                popupView.close()
            })
        }
        
        
    }
}
