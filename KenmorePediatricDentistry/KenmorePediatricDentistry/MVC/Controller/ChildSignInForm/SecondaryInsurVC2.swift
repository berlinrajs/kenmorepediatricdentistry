//
//  PatientSignInStep5VC.swift
//  MConsentForms
//
//  Created by Berlin Raj on 22/09/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

import UIKit

class SecondaryInsurVC2: MCViewController {

    
    @IBOutlet weak var textFieldInsurCompany: MCTextField!
    @IBOutlet weak var textFieldGroup: MCTextField!
    @IBOutlet weak var textFieldAddress: MCTextField!
    @IBOutlet weak var textFieldCity: MCTextField!
    @IBOutlet weak var textFieldState: MCTextField!
    @IBOutlet weak var textFieldZip: MCTextField!
    
    @IBOutlet weak var labelInsuranceTitle: UILabel!
    
    var otherRelation: String = ""
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        textFieldGroup.textFormat = .AlphaNumeric
        textFieldState.textFormat = .State
        textFieldZip.textFormat = .Zipcode
        autoFill()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated..
    }
    
    @IBAction override func buttonBackAction() {
        saveValues()
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func autoFill(){
        textFieldInsurCompany.text =  patient.secondaryInsurCompany
        textFieldGroup.text = patient.secondaryGroup
        textFieldAddress.text = patient.secondaryAddress
        textFieldCity.text = patient.secondaryCity
        textFieldState.text = patient.secondaryState
        textFieldZip.text = patient.secondaryZip
        
    }
    
    func saveValues(){
        
        patient.secondaryInsurCompany = textFieldInsurCompany.text
        patient.secondaryGroup = textFieldGroup.text
        patient.secondaryAddress = textFieldAddress.text
        patient.secondaryCity = textFieldCity.text
        patient.secondaryState = textFieldState.text
        patient.secondaryZip = textFieldZip.text
    }
    
    
    @IBAction func buttonNextAction () {
        self.view.endEditing(true)
        if !textFieldZip.isEmpty && !textFieldZip.text!.isZipCode {
            self.showAlert("PLEASE ENTER A VALID VALID ZIP CODE")
        } else {
            saveValues()
            let nitrous = patientStoryBoard.instantiateViewController(withIdentifier: "ChildDentalVC") as! ChildDentalVC
            nitrous.patient = self.patient
            self.navigationController?.pushViewController(nitrous, animated: true)
        }
    }
}

