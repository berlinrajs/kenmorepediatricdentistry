//
//  NitrousOxideFormViewController.swift
//  KenmorePediatricDentistry
//
//  Created by Bala Murugan on 1/24/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class DentalExtractForm: MCViewController {

    var signPatient : UIImage!
    var signDentist : UIImage!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelParentName : UILabel!
    @IBOutlet weak var signaturePatient : UIImageView!
    @IBOutlet weak var signatureDentist : UIImageView!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var labelDate2 : UILabel!
    @IBOutlet weak var labelProcedure1 : UILabel!
    @IBOutlet weak var labelProcedure2 : UILabel!
    
    @IBOutlet var btnCollection1: [UIButton]!
    
    @IBOutlet weak var labelPatientName1 : UILabel!
    @IBOutlet weak var date : UILabel!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        labelPatientName1.text = patient.fullName
        date.text = patient.dateToday
        labelPatientName.text = patient.fullName
        labelParentName.text = patient.dentalExtractParentName
        signaturePatient.image = signPatient
        signatureDentist.image = signDentist
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        
         let form : Forms = patient.selectedForms.first!
        labelProcedure1.text = form.toothNumbers
        labelProcedure2.text = patient.dentalExtractOther
        
        for btn in btnCollection1 {
            btn.isSelected = patient.dentalExtractArray.contains(btn.tag)
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
