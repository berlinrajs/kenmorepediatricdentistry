//
//  NitrousOxide1ViewController.swift
//  KenmorePediatricDentistry
//
//  Created by Bala Murugan on 1/24/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class DentalExtractVC: MCViewController {

    @IBOutlet weak var textfieldParentName : MCTextField!
    @IBOutlet weak var signatureParent : SignatureView!
    @IBOutlet weak var signatureDentist : SignatureView!
    @IBOutlet weak var labelDate1 : DateLabel!
    @IBOutlet weak var labelDate2 : DateLabel!
    @IBOutlet weak var signLabel : UILabel!
    @IBOutlet weak var SignNameLabel : UILabel!
    @IBOutlet weak var toothNumber : FormLabel!
    @IBOutlet var btnCollection: [UIButton]!
    
    var multiSelectArray: NSMutableArray! = NSMutableArray()
    
    var otherValue: String!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday
        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue (){
        
        if patient.dentalExtractArray != nil {
            self.multiSelectArray = patient.dentalExtractArray
            for btn in btnCollection {
                btn.isSelected = self.multiSelectArray.contains(btn.tag)
            }
        }
        
        let form : Forms = patient.selectedForms.first!
        toothNumber.text = form.toothNumbers
        

    textfieldParentName.isUserInteractionEnabled = true
    textfieldParentName.text = patient.dentalExtractParentName
    
      self.otherValue = patient.dentalExtractOther
        
        // textfieldParentName.text = patient.oralParentName
    }
    
    
    @IBAction func MultiSelectButtonAction(wihSender sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if multiSelectArray.contains(sender.tag) {
            multiSelectArray.remove(sender.tag)
            if sender.tag == 6 { self.otherValue = "" }
        } else {
            multiSelectArray.add(sender.tag)
            
            if sender.tag == 6{
                PopupTextField.popUpView().showInViewController(self, WithTitle: "IF SO,", placeHolder: "SPECIFY HERE", textFormat:.Default, completion: { (popUpView, textField) in
                    if textField.isEmpty{
                        self.otherValue = ""
                       
                    }else{
                        self.otherValue = textField.text
                    }
                    popUpView.removeFromSuperview()
                })
            
            
            }
        }
    }
    
    func saveValue (){
        
        patient.dentalExtractArray = self.multiSelectArray
        patient.dentalExtractParentName = textfieldParentName.text
        patient.dentalExtractOther = self.otherValue
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }

    @IBAction func onNextButtonPressed (withSender sender : UIButton){
         if !signatureParent.isSigned() || !signatureDentist.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate1.dateTapped || !labelDate2.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            saveValue()
            let nitrous = consentStoryBoard1.instantiateViewController(withIdentifier: "DentalExtractForm") as! DentalExtractForm
            nitrous.patient = self.patient
            nitrous.signPatient = signatureParent.signatureImage()
            nitrous.signDentist = signatureDentist.signatureImage()
            self.navigationController?.pushViewController(nitrous, animated: true)

        }
    }


}
