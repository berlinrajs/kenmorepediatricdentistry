//
//  NitrousOxideFormViewController.swift
//  KenmorePediatricDentistry
//
//  Created by Bala Murugan on 1/24/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class DentalHistoryForm: MCViewController {


    @IBOutlet weak var labelPrevDentist : UILabel!
    @IBOutlet weak var lastDentalExam : UILabel!
    @IBOutlet weak var lastDentalTreatment : UILabel!
    @IBOutlet weak var signatureDentist : UIImageView!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var lastDentalXray : UILabel!
    @IBOutlet weak var dentalFloss : UILabel!
    @IBOutlet weak var immediateConcern : UILabel!
    @IBOutlet weak var firstPartial : UILabel!
    @IBOutlet weak var presentDenture : UILabel!
    
    @IBOutlet var radioArray1: [RadioButton]!
    @IBOutlet var radioArray2: [RadioButton]!
    @IBOutlet var radioArray3: [RadioButton]!
   
    @IBOutlet weak var array00 : UILabel!
    @IBOutlet weak var array01 : UILabel!
    @IBOutlet weak var array02 : UILabel!
    @IBOutlet weak var array03 : UILabel!
    @IBOutlet weak var array04 : UILabel!
    @IBOutlet weak var array05 : UILabel!
    @IBOutlet weak var array06 : UILabel!
    @IBOutlet weak var array07 : UILabel!
    @IBOutlet weak var array08 : UILabel!
    @IBOutlet weak var array09 : UILabel!
    @IBOutlet weak var array010 : UILabel!
    
    @IBOutlet weak var array10 : UILabel!
    @IBOutlet weak var array11 : UILabel!
    @IBOutlet weak var array12 : UILabel!
    @IBOutlet weak var array13 : UILabel!
    @IBOutlet weak var array14 : UILabel!
    @IBOutlet weak var array15 : UILabel!
    @IBOutlet weak var array16 : UILabel!
    @IBOutlet weak var array17 : UILabel!
    @IBOutlet weak var array18 : UILabel!
    @IBOutlet weak var array19 : UILabel!
    @IBOutlet weak var array110 : UILabel!
    
    
    @IBOutlet weak var radioTeethClean: RadioButton!
    @IBOutlet weak var radioDentalFloss: RadioButton!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        labelPrevDentist.text = patient.prevDentist
        lastDentalExam.text = patient.lastDentalExam
        lastDentalTreatment.text = patient.lastDentalTreatment
        lastDentalXray.text = patient.lastDentalXray
        dentalFloss.text = patient.dentalFlossValue
        immediateConcern.text = patient.immediateDentalConcern
        firstPartial.text = patient.firstPartialDenture
        presentDenture.text = patient.presentDenture
        
        signatureDentist.image = patient.Signature1
        labelDate1.text = patient.dateToday
        
        array00.text = patient.healthQues.DentalHistoryQues[0][0].answer
        array01.text = patient.healthQues.DentalHistoryQues[0][1].answer
        array02.text = patient.healthQues.DentalHistoryQues[0][2].answer
        array03.text = patient.healthQues.DentalHistoryQues[0][3].answer
        array04.text = patient.healthQues.DentalHistoryQues[0][4].answer
        array05.text = patient.healthQues.DentalHistoryQues[0][5].answer
        array06.text = patient.healthQues.DentalHistoryQues[0][6].answer
        array07.text = patient.healthQues.DentalHistoryQues[0][7].answer
        array08.text = patient.healthQues.DentalHistoryQues[0][8].answer
        array09.text = patient.healthQues.DentalHistoryQues[0][9].answer
        array010.text = patient.healthQues.DentalHistoryQues[0][10].answer
    
        array10.text = patient.healthQues.DentalHistoryQues[1][0].answer
        array11.text = patient.healthQues.DentalHistoryQues[1][1].answer
        array12.text = patient.healthQues.DentalHistoryQues[1][2].answer
        array13.text = patient.healthQues.DentalHistoryQues[1][3].answer
        array14.text = patient.healthQues.DentalHistoryQues[1][4].answer
        array15.text = patient.healthQues.DentalHistoryQues[1][5].answer
        array16.text = patient.healthQues.DentalHistoryQues[1][6].answer
        array17.text = patient.healthQues.DentalHistoryQues[1][7].answer
        array18.text = patient.healthQues.DentalHistoryQues[1][8].answer
        array19.text = patient.healthQues.DentalHistoryQues[1][9].answer
        array110.text = patient.healthQues.DentalHistoryQues[1][10].answer
        
        radioTeethClean.setSelectedWithTag(patient.dropDownTeethClean)
        radioDentalFloss.setSelectedWithTag(patient.dentalFloss)
        
        for btn in radioArray1{
            
            btn.isSelected = patient.healthQues.DentalHistoryQues[0][btn.tag].selectedOption
        }
        
        for btn in radioArray2{
            
            btn.isSelected = patient.healthQues.DentalHistoryQues[1][btn.tag].selectedOption
        }
        
        for btn in radioArray3{
            
            btn.isSelected = patient.healthQues.DentalHistoryQues[2][btn.tag].selectedOption
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
