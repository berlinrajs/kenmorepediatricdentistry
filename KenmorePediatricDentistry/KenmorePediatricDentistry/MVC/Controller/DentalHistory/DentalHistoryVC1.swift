//
//  PatientSignInStep2VC.swift
//  MConsentForms
//
//  Created by Berlin Raj on 22/09/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

import UIKit

class DentalHistoryVC1: MCViewController {

    @IBOutlet weak var prevDentist: MCTextField!
    @IBOutlet weak var lastDentalExam: MCTextField!
    @IBOutlet weak var lastDentalTreatment: MCTextField!
    @IBOutlet weak var lastDentalXray: MCTextField!
    @IBOutlet weak var radioDentalFloss: RadioButton!
    @IBOutlet weak var dropDownDentalClean: BRDropDown!
    var otherValue: String!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        lastDentalExam.textFormat = .DateInCurrentYear
        lastDentalTreatment.textFormat = .DateInCurrentYear
        lastDentalXray.textFormat = .DateInCurrentYear
        
        dropDownDentalClean.items = ["3 months","4 months","6 months","1 year or Longer"]
        dropDownDentalClean.placeholder = "PLEASE SELECT"
        autoFill()
        // Do any additional setup after loading the view.
    }

       @IBAction override func buttonBackAction() {
            saveValues()
         _ = self.navigationController?.popViewController(animated: true)
    }

    
    
    @IBAction func radioButtonAction(sender: RadioButton) {
    
        if sender.tag == 1{
            PopupTextField.popUpView().showInViewController(self, WithTitle: "IF YES,", placeHolder: "HOW OFTEN", textFormat: .Default, completion: { (popUpView, textField) in
                
                if textField.isEmpty{
                    self.otherValue = ""
                    self.radioDentalFloss.setSelectedWithTag(2)
                }else{
                    self.otherValue = textField.text
                }
                popUpView.removeFromSuperview()
            })
        
        }else{
            self.otherValue = ""
        
        }
    
    }
    
    
    func autoFill(){
        prevDentist.text = patient.prevDentist
         lastDentalExam.text = patient.lastDentalExam
        lastDentalTreatment.text =  patient.lastDentalTreatment
        lastDentalXray.text = patient.lastDentalXray
         dropDownDentalClean.selectedIndex = patient.dropDownTeethClean
         radioDentalFloss.setSelectedWithTag(patient.dentalFloss)
         self.otherValue = patient.dentalFlossValue
    }
    
    func saveValues(){
        patient.prevDentist = prevDentist.text
        patient.lastDentalExam = lastDentalExam.text
        patient.lastDentalTreatment = lastDentalTreatment.text
        patient.lastDentalXray = lastDentalXray.text
        patient.dropDownTeethClean = dropDownDentalClean.selectedIndex
        patient.dentalFloss = radioDentalFloss.selected.tag
        patient.dentalFlossValue = self.otherValue
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func buttonNextAction() {
        self.view.endEditing(true)
            saveValues()
            let nitrous = medicalStoryboard.instantiateViewController(withIdentifier: "DentalHistoryVC2") as! DentalHistoryVC2
            nitrous.patient = self.patient
            self.navigationController?.pushViewController(nitrous, animated: true)
        
    }
    
}
