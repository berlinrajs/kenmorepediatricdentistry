//
//  HealthHistory2ViewController.swift
//  WestKendall
//
//  Created by Bala Murugan on 10/5/16.
//  Copyright © 2016 SRS. All rights reserved....

import UIKit

class DentalHistoryVC2: MCViewController {

    @IBOutlet var tableViewQuestions: UITableView!
    @IBOutlet var buttonVerified: UIButton!
    @IBOutlet var buttonNext: UIButton!
    @IBOutlet var buttonBack1: UIButton!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var labelMainHeading: UILabel!
    @IBOutlet var labelHeading : UILabel!
    var selectedIndex: Int = 0
    var medicalHistoryStep1 : [MCQuestion]! = [MCQuestion]()
    var popUpWhySeeingPhysician : String!
    
@IBOutlet weak var textViewNotes: MCTextView!
@IBOutlet weak var extraView: UIView!
 
    override func viewDidLoad() {
        super.viewDidLoad()
        self.activityIndicator.stopAnimating()
        textViewNotes.delegate?.textViewDidBeginEditing?(textViewNotes)
        textViewNotes.text =   patient.notesValue
        textViewNotes.delegate?.textViewDidEndEditing?(textViewNotes)
        
        }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onBackButton1Pressed (withSender sender: AnyObject) {
     
        textViewNotes.delegate?.textViewDidBeginEditing?(textViewNotes)
         patient.notesValue = textViewNotes.text
        textViewNotes.delegate?.textViewDidEndEditing?(textViewNotes)
        if selectedIndex == 0 {
             _ = self.navigationController?.popViewController(animated: true)
        } else {
            buttonBack1.isUserInteractionEnabled = false
            buttonNext.isUserInteractionEnabled = false
            self.activityIndicator.startAnimating()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.activityIndicator.stopAnimating()
                self.selectedIndex = self.selectedIndex - 1
//                self.labelHeading.text = self.selectedIndex == 0 ? "" : "Have you ever had any of the following: *"
//                self.labelMainHeading.text = self.selectedIndex == 6 ? "DENTAL HISTORY" : "MEDICAL HISTORY"
                
                if self.selectedIndex == 0 {
                    self.labelMainHeading.text = "DENTAL HISTORY"
                    self.labelHeading.text = "Please check if you have, or have ever had the following:"
                    self.extraView.isHidden = true
                }else if self.selectedIndex == 1 {
                    self.labelMainHeading.text = "DENTAL HISTORY"
                    self.labelHeading.text = "Please check if you have, or have ever had the following:"
                    self.extraView.isHidden = true
                }else if self.selectedIndex == 2 {
                    self.labelMainHeading.text = "SUPPLEMENTAL DENTAL HISTORY"
                    self.labelHeading.text = "If you are wearing a partial or complete artificial denture, please complete the following:"
                    self.extraView.isHidden = true
                }
                self.tableViewQuestions.reloadData()
                self.buttonBack1.isUserInteractionEnabled = true
                self.buttonNext.isUserInteractionEnabled = true
            }
            
        }
    }
    
    @IBAction func buttonVerifiedAction(withSender sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        
        textViewNotes.delegate?.textViewDidBeginEditing?(textViewNotes)
         patient.notesValue = textViewNotes.text
        textViewNotes.delegate?.textViewDidEndEditing?(textViewNotes)
        
        if buttonVerified.isSelected == false {
            self.showAlert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
        } else {
            if selectedIndex == 2 {
                let step2VC = self.storyboard?.instantiateViewController(withIdentifier: "DentalHistoryVC3") as! DentalHistoryVC3
                step2VC.patient = self.patient
                self.navigationController?.pushViewController(step2VC, animated: true)
                
            } else {
                
                buttonBack1.isUserInteractionEnabled = false
                buttonNext.isUserInteractionEnabled = false
                self.buttonVerified.isSelected = false
                self.activityIndicator.startAnimating()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.activityIndicator.stopAnimating()
                    self.selectedIndex = self.selectedIndex + 1
//                    self.labelHeading.text = self.selectedIndex == 0 ? "" : "Have you ever had any of the following: *"
//                    self.labelMainHeading.text = self.selectedIndex == 7 ? "DENTAL HISTORY" : "MEDICAL HISTORY"
                    if self.selectedIndex == 0 {
                        self.labelMainHeading.text = "DENTAL HISTORY"
                        self.labelHeading.text = "Please check if you have, or have ever had the following:"
                        self.extraView.isHidden = true
                    }else if self.selectedIndex == 1 {
                        self.labelMainHeading.text = "DENTAL HISTORY"
                        self.labelHeading.text = "Please check if you have, or have ever had the following:"
                        self.extraView.isHidden = true
                    }else if self.selectedIndex == 2 {
                        self.labelMainHeading.text = "SUPPLEMENTAL DENTAL HISTORY"
                        self.labelHeading.text = "If you are wearing a partial or complete artificial denture, please complete the following:"
                        self.extraView.isHidden = true
                    }
                    self.tableViewQuestions.reloadData()
                    self.buttonBack1.isUserInteractionEnabled = true
                    self.buttonNext.isUserInteractionEnabled = true
                }
            }
        }
    }
    
}

extension DentalHistoryVC2 : UITableViewDataSource,UITableViewDelegate {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patient.healthQues.DentalHistoryQues[selectedIndex].count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
     
        return 50
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PatientInfoCell
        cell.delegate = self
        cell.tag = indexPath.row
        cell.configCell(question: self.patient.healthQues.DentalHistoryQues[selectedIndex][indexPath.row])
        
        return cell
    }
}

extension DentalHistoryVC2 : PatientInfoCellDelegate{
    
    func radioButtonTappedForCell(cell: PatientInfoCell) {
        
       let indexPath = tableViewQuestions.indexPath(for: cell)!
        
        
            PopupTextField.popUpView().showInViewController(self, WithTitle: "If So,", placeHolder: "Please Explain", textFormat: .Default, completion: { (popUpView, textField) in
                
                if !textField.isEmpty {
                    self.patient.healthQues.DentalHistoryQues[self.selectedIndex][indexPath.row].answer = textField.text!
                    
                } else {
                    self.patient.healthQues.DentalHistoryQues[self.selectedIndex][indexPath.row].answer = ""
                    self.patient.healthQues.DentalHistoryQues[self.selectedIndex][indexPath.row].selectedOption = false
                    self.tableViewQuestions.reloadData()
                }
                
                popUpView.removeFromSuperview()
            })
        
        }

}

