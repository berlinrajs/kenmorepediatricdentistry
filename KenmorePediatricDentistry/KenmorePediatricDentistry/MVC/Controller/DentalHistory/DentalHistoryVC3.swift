//
//  HipaaRefusalStep1VC.swift
//  WellnessDental
//
//  Created by Berlin Raj on 03/11/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved..
//

import UIKit

class DentalHistoryVC3: MCViewController {
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var firstPartial: MCTextField!
    @IBOutlet weak var presentDenture: MCTextField!
    @IBOutlet weak var ImmediateDentalConcern: MCTextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadValues()
    }
    
    func loadValues() {
        labelName.text =  patient.fullName
        labelDate.todayDate = patient.dateToday
        firstPartial.text = patient.immediateDentalConcern
         presentDenture.text = patient.firstPartialDenture
         ImmediateDentalConcern.textValue = patient.presentDenture
    }
    
    func saveValues() {
        self.view.endEditing(true)
        
        patient.Signature1 = signatureView.signatureImage()
         patient.immediateDentalConcern = firstPartial.text
         patient.firstPartialDenture = presentDenture.text
         patient.presentDenture = ImmediateDentalConcern.textValue
    }
    
    override func buttonBackAction() {
        saveValues()
        super.buttonBackAction()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonNextAction() {
        if !signatureView.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !labelDate.dateTapped {
            self.showAlert("PLEASE TAP TO DATE")
        } else {
            saveValues()
            
//        let  formVC = self.storyboard?.instantiateViewController(withIdentifier: "DentalHistoryForm") as! DentalHistoryForm
//        formVC.patient = self.patient
//        self.navigationController?.pushViewController(formVC, animated: true)
            
            let silver = consentStoryBoard1.instantiateViewController(withIdentifier: "PrivacyPracticesVC") as! NoticeOfPrivacyPractices1ViewController
            silver.patient = self.patient
            self.navigationController?.pushViewController(silver, animated: true)

        }
    }
}



