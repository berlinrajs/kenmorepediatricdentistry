//
//  ViewController.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/25/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class HomeViewController: MCViewController {
    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var tableViewForms: UITableView!
    @IBOutlet weak var labelVersion : UILabel!
    @IBOutlet weak var labelPlace: UILabel!
    
    var selectedForms : [Forms]! = [Forms]()
    var formList : [Forms]! = [Forms]()
    
    var signRefused: Bool!
    var signRefusalTag: Int!
    var signRefusalOther: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.showCompletionAlert), name: NSNotification.Name(rawValue: kFormsCompletedNotification), object: nil)
        
        if let text = Bundle.main.infoDictionary?[kCFBundleVersionKey as String] as? String {
            labelVersion.text = text
        }
        
        labelPlace.text = kPlace
        NotificationCenter.default.addObserver(self, selector: #selector(dateChangedNotification), name: NSNotification.Name(rawValue: kDateChangedNotification), object: nil)
        self.dateChangedNotification()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func dateChangedNotification() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = kCommonDateFormat
        labelDate.text = dateFormatter.string(from: NSDate() as Date).uppercased()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if kAppLoginAvailable {
            loginValidation()
        }
        Forms.getAllForms { (isConnectionFailed, forms) -> Void in
            self.formList = forms
            self.tableViewForms.reloadData()
            if isConnectionFailed == true {
                self.showAlert("Your device is not connected to internet. Please go to settings to connect.", buttonTitles: ["Settings", "Cancel"], completion: { (buttonIndex) in
                    if buttonIndex == 0 {
                        let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
                        if let url = settingsUrl {
                            if #available(iOS 10.0, *) {
                                UIApplication.shared.open(url, options: [:], completionHandler: { (finished) in
                                    
                                })
                            } else {
                                // Fallback on earlier versions
                            }
                        }
                    } else {
                        
                    }
                })
            }
        }
    }
    func loginValidation() {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
            let defaults = UserDefaults.standard
            ServiceManager.loginWithUsername(defaults.value(forKey: kAppLoginUsernameKey) as! String, password: defaults.value(forKey: kAppLoginPasswordKey) as! String) { (success, error) -> (Void) in
                if success {
                    
                } else {
                    if error == nil {
                        
                    } else {
                        DispatchQueue.main.async(execute: {
                            UserDefaults.standard.set(false, forKey: "kApploggedIn")
                            UserDefaults.standard.synchronize()
                            (UIApplication.shared.delegate as! AppDelegate).checkAutologin()
                        })
                    }
                }
            }
        }
    }
    
    @IBAction func buttonActionNext(WithSender sender: AnyObject) {
        self.view.endEditing(true)
        selectedForms.removeAll()
        for (_, form) in formList.enumerated() {
            if form.isSelected == true {
                if form.formTitle == kConsentForms  {
                    for subForm in form.subForms {
                        if subForm.isSelected == true {
                            selectedForms.append(subForm)
                        }
                    }
                } else {
                    selectedForms.append(form)
                }
            }
        }
        
        let patient = MCPatient(forms: selectedForms)
        patient.dateToday = labelDate.text
        patient.signRefused = self.signRefused
        patient.signRefusalOther = self.signRefusalOther
        patient.signRefusalTag = self.signRefusalTag
        //let formNames = (patient.selectedForms as NSArray).value(forKey: "formTitle") as! [String]
 
        if selectedForms.count > 0 {
            let patientInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kPatientInfoVC") as! PatientInfoViewController
            patientInfoVC.patient = patient
            self.navigationController?.pushViewController(patientInfoVC, animated: true)
        } else {
            self.showAlert("PLEASE SELECT ANY FORM")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    func showCompletionAlert() {
        self.showCustomAlert("PLEASE HANDOVER THE DEVICE BACK TO FRONT DESK")
    }
}


extension HomeViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == consentIndex {
            let subForms = formList[consentIndex].subForms
            for subFrom in subForms! {
                subFrom.isSelected = false
            }
            let form = self.formList[consentIndex]
            form.isSelected = !form.isSelected
            var indexPaths : [IndexPath] = [IndexPath]()
            for (idx, _) in form.subForms.enumerated() {
                let indexPath = IndexPath(row: consentIndex + 1 + idx, section: 0)
                indexPaths.append(indexPath)
            }
            if form.isSelected == true {
                tableView.beginUpdates()
                tableView.insertRows(at: indexPaths, with: .bottom)
                tableView.endUpdates()
                let delayTime = DispatchTime.now() + Double(Int64(0.2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delayTime) {
                    if form.isSelected == true && indexPaths.count > 0 {
                        tableView.scrollToRow(at: IndexPath(row: tableView.numberOfRows(inSection: 0) - 1, section: 0), at: .bottom, animated: true)
                    }
                }
            } else {
                tableView.beginUpdates()
                tableView.deleteRows(at: indexPaths, with: .bottom)
                tableView.endUpdates()
            }
            tableView.reloadRows(at: [indexPath], with: .none)
            return
        }
        
        var form : Forms!
        if (indexPath.row <= consentIndex) {
            form = formList[indexPath.row]
        } else if formList[consentIndex].isSelected && indexPath.row <= consentIndex + formList[consentIndex].subForms.count {
            form = formList[consentIndex].subForms[indexPath.row - (consentIndex + 1)]
        } else {
            form = formList.last
        }
        
        if !form.isSelected && form.formTitle == kNewPatientSignInForm {
            form.isSelected = true
            self.tableViewForms.reloadData()
        } else if (form.formTitle == kAcknowledgementOfPrivacy && !form.isSelected) {
            HipaaSignatureRefusalPopup.popUpView().showInViewController(viewController: self, completion: { (popUpView, refused, textFieldOther, refusalTag) in
                form.isSelected = true
                tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
                popUpView.close()
                self.signRefused = refused
                self.signRefusalTag = refused == false ? 0 : refusalTag
                self.signRefusalOther = refused == false ? "" : refusalTag == 4 ? textFieldOther.text! : ""
            }, error: { (message) in
                self.showAlert(message)
            })
        } else {
            form.isSelected = !form.isSelected
            tableView.reloadData()
        }
        if form.isSelected == true && form.isToothNumberRequired == true {
            PopupTextField.popUpView().showInViewController(self, WithTitle: "ENTER TOOTH NUMBERS", placeHolder: "01, 02, 03, 04", textFormat:.AlphaNumeric, completion: { (popUpView, textField) in
                if textField.isEmpty{
                    form.isSelected = false
                    tableView.reloadData()
                }else{
                    form.toothNumbers = textField.text!
                }
                popUpView.removeFromSuperview()
            })
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cell = tableView.dequeueReusableCell(withIdentifier: (indexPath.row <= consentIndex) ? "cellMainForm" : (formList[consentIndex].isSelected && indexPath.row <= consentIndex + formList[consentIndex].subForms.count) ? "cellSubForm" : "cellMainForm") as! FormsTableViewCell
        var form : Forms!
        if (indexPath.row <= consentIndex) {
            form = formList[indexPath.row]
            let height = form.formTitle.heightWithConstrainedWidth(560, font: cell.labelFormName.font) + 24
            return height
        } else if formList[consentIndex].isSelected && indexPath.row <= consentIndex + formList[consentIndex].subForms.count {
            form = formList[consentIndex].subForms[indexPath.row - (consentIndex + 1)]
            let height = form.formTitle.heightWithConstrainedWidth(520, font: cell.labelFormName.font) + 24
            return height
        } else {
            form = formList.last!
            let height = form.formTitle.heightWithConstrainedWidth(560, font: cell.labelFormName.font) + 24
            return height
        }
    }
}

extension HomeViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if formList.count > 0 {
            if formList.count > consentIndex {
                return formList[consentIndex].isSelected == true ? formList.count + formList[consentIndex].subForms.count : formList.count
            } else {
                return formList.count
            }
            
            //return formList.count
        } else {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row <= consentIndex) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellMainForm", for: indexPath) as! FormsTableViewCell
            let form = formList[indexPath.row]
            cell.labelFormName.text = form.formTitle
            cell.imageViewCheckMark.isHidden = !form.isSelected
            cell.backgroundColor = UIColor.clear
            cell.contentView.backgroundColor = UIColor.clear
            return cell
        } else if formList[consentIndex].isSelected && indexPath.row <= consentIndex + formList[consentIndex].subForms.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellSubForm", for: indexPath) as!FormsTableViewCell
            let form = formList[consentIndex].subForms[indexPath.row - (consentIndex + 1)]
            cell.labelFormName.text = form.formTitle
            cell.imageViewCheckMark.isHidden = !form.isSelected
            cell.backgroundColor = UIColor.clear
            cell.contentView.backgroundColor = UIColor.clear
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellMainForm", for: indexPath) as! FormsTableViewCell
            let form = formList[consentIndex + 1]
            cell.labelFormName.text = form.formTitle
            cell.imageViewCheckMark.isHidden = !form.isSelected
            cell.backgroundColor = UIColor.clear
            cell.contentView.backgroundColor = UIColor.clear
            return cell
        }
    }
    
}
