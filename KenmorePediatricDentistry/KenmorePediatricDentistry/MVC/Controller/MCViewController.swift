//
//  MCViewController.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/25/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


let mainStoryBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
let consentStoryBoard = UIStoryboard(name: "Consent", bundle: Bundle.main)
let consentStoryBoard1 = UIStoryboard(name: "Consent1", bundle: Bundle.main)
let patientStoryBoard = UIStoryboard(name: "Patient", bundle: Bundle.main)
let patientStoryBoard1 = UIStoryboard(name: "Adult", bundle: Bundle.main)
let medicalStoryboard = UIStoryboard(name: "MedicalHistory", bundle: Bundle.main)
let medicalUpdateStoryboard = UIStoryboard(name: "MedicalHistoryUpdate", bundle: Bundle.main)
let medicalUpdateChildStoryboard = UIStoryboard(name: "ChildMedicalHistoryUpdate", bundle: Bundle.main)


class MCViewController: UIViewController {
    
    @IBOutlet var buttonSubmit: MCButton?
    @IBOutlet var buttonBack: MCButton?
    
    @IBOutlet var pdfView: UIScrollView?
    
    var patient: MCPatient!
    var completion: ((_ success: Bool) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        edgesForExtendedLayout = UIRectEdge()
        
        configureNavigationButtons()
        // Do any additional setup after loading the view.
    }
    
    func configureNavigationButtons() {
        self.buttonSubmit?.backgroundColor = UIColor.green
        self.buttonBack?.isHidden = self.buttonSubmit == nil && isFromPreviousForm
        
        if self.buttonBack != nil && self.buttonSubmit == nil && isFromPreviousForm {
            self.navigationController?.viewControllers.removeSubrange(1...self.navigationController!.viewControllers.index(of: self)! - 1)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
    }
    
    var isFromPreviousForm: Bool {
        get {
            return navigationController!.viewControllers.count > 3 ? true : false
        }
    }
    
    @IBAction func buttonBackAction() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onSubmitButtonPressed() {
        self.buttonBack?.isUserInteractionEnabled = false
        self.buttonSubmit?.isUserInteractionEnabled = false
        if !Reachability.isConnectedToNetwork() {
            self.showAlert("Your device is not connected to internet. Please go to settings to connect.", buttonTitles: ["Settings", "Cancel"], completion: { (buttonIndex) in
                if buttonIndex == 0 {
                    let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                    if let url = settingsUrl {
                        UIApplication.shared.openURL(url as URL)
                    }
                } else {
                    
                }
            })
            self.buttonBack?.isUserInteractionEnabled = true
            self.buttonSubmit?.isUserInteractionEnabled = true
            return
        }
        uploadPdfToDrive()
    }
    
    func sendFormToGoogleDrive(completion: @escaping (_ success: Bool) -> Void) {
        self.uploadPdfToDrive()
    }
    
    func sendFormToServer() {
        if !Reachability.isConnectedToNetwork() {
            self.showAlert("Your device is not connected to internet. Please go to settings to connect.", buttonTitles: ["Settings", "Cancel"], completion: { (buttonIndex) in
                if buttonIndex == 1 {
                    let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                    if let url = settingsUrl {
                        UIApplication.shared.openURL(url as URL)
                    }
                } else {
                    
                }
            })
            return
        }
        var image: UIImage?
        self.buttonSubmit?.isHidden = true
        self.buttonBack?.isHidden = true
        self.pdfView?.isScrollEnabled = false
        self.pdfView?.clipsToBounds = false
        let size: CGSize = CGSize(width: self.pdfView!.contentSize.width, height: self.pdfView!.contentSize.height)
        
        let savedContentOffset = self.pdfView!.contentOffset
        UIGraphicsBeginImageContext(size)
        self.pdfView?.layer.render(in: UIGraphicsGetCurrentContext()!)
        
        image = UIGraphicsGetImageFromCurrentImageContext()
        self.pdfView?.contentOffset = savedContentOffset
        UIGraphicsEndImageContext()
        
        func resetValues() {
            self.buttonSubmit?.isHidden = false
            self.buttonBack?.isHidden = false
            self.pdfView?.isScrollEnabled = true
            self.pdfView?.clipsToBounds = true
            BRProgressHUD.hide()
        }
        
        
        if let imageData = image {
            BRProgressHUD.show()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM'_'dd'_'yyyy"
            let dateString = dateFormatter.string(from: Date()).uppercased()
            dateFormatter.dateFormat = "MM'-'dd'-'yyyy"
            let manager = AFHTTPSessionManager(baseURL: URL(string: serverPath))
            manager.responseSerializer.acceptableContentTypes = ["text/html"]
            //http://opcommunity.mncell.com/formreviewapp/upload_medical_history_form.php?first_name=&last_name=&doc_date=&form_name=&client_id=&file_name=
            manager.post("upload_medical_history_form.php", parameters: ["first_name": patient.firstName, "last_name": patient.lastName, "doc_date": dateFormatter.string(from: Date()), "form_name": self.patient.selectedForms.first!.formTitle.fileName, "client_id": kAppKey, "file_name": self.patient.selectedForms.first!.formTitle.fileName], constructingBodyWith: { (data) in
                dateFormatter.dateFormat = "MMddyyyyHHmmSSS"
                let randomString = dateFormatter.string(from: Date()).uppercased()
                let fileName = "\(self.patient.fullName.fileName)_\(dateString)_\(self.patient.selectedForms.first!.formTitle.fileName)_\(randomString).jpg"
                data.appendPart(withFileData: UIImageJPEGRepresentation(imageData, 1.0)!, name: self.patient.selectedForms.first!.formTitle.fileName, fileName: fileName, mimeType: "image/jpeg")
            }, progress: { (progress) in
                
            }, success: { (task, result) in
                resetValues()
                self.patient.selectedForms.removeFirst()
                self.gotoNextForm()
            }) { (task, error) in
                resetValues()
                self.showAlert(error.localizedDescription)
            }
        } else {
            self.showAlert("SOMETHIG WENT WRONG\nPLEASE TRY AGAIN")
            resetValues()
        }
    }
    
    func uploadPdfToDrive (){
        let pdfManager = PDFManager()
        pdfManager.authorizeDrive(self) { (success) -> Void in
            self.buttonBack?.isUserInteractionEnabled = false
            self.buttonSubmit?.isUserInteractionEnabled = false
            if success {
                self.buttonSubmit?.isHidden = true
                self.buttonBack?.isHidden = true
                
                pdfManager.uploadToGoogleDrive(self.pdfView == nil ? self.view : self.pdfView!, patient: self.patient, completionBlock: { (finished) -> Void in
                    if finished {
                        if let _ = self.completion {
                            self.completion!(true)
                        } else {
                            self.patient.selectedForms.removeFirst()
                            self.gotoNextForm()
                        }
                    } else {
                        self.buttonSubmit?.isHidden = false
                        self.buttonBack?.isHidden = false
                        self.buttonBack?.isUserInteractionEnabled = true
                        self.buttonSubmit?.isUserInteractionEnabled = true
                    }
                })
            } else {
                self.buttonSubmit?.isHidden = false
                self.buttonBack?.isHidden = false
                self.buttonBack?.isUserInteractionEnabled = true
                self.buttonSubmit?.isUserInteractionEnabled = true
                
            }
        }
    }
    
    
    
    func gotoNextForm() {
        let formNames = (patient.selectedForms as NSArray).value(forKey: "formTitle") as! [String]
        
        if formNames.contains(kNewPatientSignInForm) || formNames.contains(kPatientSignInForm){
            let patientSignIn = patientStoryBoard1.instantiateViewController(withIdentifier: "AdultAddressVC") as! AdultAddressVC
            patientSignIn.patient = self.patient
            self.navigationController?.pushViewController(patientSignIn, animated: true)
        }else if formNames.contains(kMedicalHistory) {
            let medicalHis = medicalStoryboard.instantiateViewController(withIdentifier: "kMedicalHistoryStep1ViewController") as! MedicalHistoryStep1ViewController
            medicalHis.patient = self.patient
            self.navigationController?.pushViewController(medicalHis, animated: true)
            
        }else if formNames.contains(kDentalHistory) {
            let medicalHis = medicalStoryboard.instantiateViewController(withIdentifier: "DentalHistoryVC1") as! DentalHistoryVC1
            medicalHis.patient = self.patient
            self.navigationController?.pushViewController(medicalHis, animated: true)
            
        }else if formNames.contains(kChildPatientSignInForm) || formNames.contains(kNewChildPatientSignInForm)  {
            let patientSignIn = patientStoryBoard.instantiateViewController(withIdentifier: "kPatientAddressVC") as! PatientAddressVC
            patientSignIn.patient = self.patient
            self.navigationController?.pushViewController(patientSignIn, animated: true)
        }else if formNames.contains(kMedicalHistoryUpdate) {
            let medicalHisUpdate = medicalUpdateStoryboard.instantiateViewController(withIdentifier: "kMedicalHistoryUpdateStep1ViewController") as! MedicalHistoryUpdateStep1ViewController
            medicalHisUpdate.patient = self.patient
            self.navigationController?.pushViewController(medicalHisUpdate, animated: true)
            
        }else if formNames.contains(kChildMedicalHistoryUpdate) {
            let medicalHisUpdate = medicalUpdateChildStoryboard.instantiateViewController(withIdentifier: "kChildMedicalUpdateVC") as! ChildMedicalUpdateVC
            medicalHisUpdate.patient = self.patient
            self.navigationController?.pushViewController(medicalHisUpdate, animated: true)
            
        }else if formNames.contains(kInsuranceCard) {
            let insuranceCard = mainStoryBoard.instantiateViewController(withIdentifier: "kCardImageCaptureVC") as! CardImageCaptureVC
            insuranceCard.patient = self.patient
            self.navigationController?.pushViewController(insuranceCard, animated: true)
        } else if formNames.contains(kDrivingLicense) {
            let drivingLicense = mainStoryBoard.instantiateViewController(withIdentifier: "kCardImageCaptureVC") as! CardImageCaptureVC
            drivingLicense.patient = self.patient
            drivingLicense.isDrivingLicense = true
            self.navigationController?.pushViewController(drivingLicense, animated: true)
        } else if formNames.contains(kSelfieForm) {
            let drivingLicense = mainStoryBoard.instantiateViewController(withIdentifier: "kSelfieStep1VC") as! SelfieStep1VC
            drivingLicense.patient = self.patient
            self.navigationController?.pushViewController(drivingLicense, animated: true)
         } else if formNames.contains(kNitrousOxide) {
            let nitrous = consentStoryBoard.instantiateViewController(withIdentifier: "NitrousOxide1VC") as! NitrousOxide1ViewController
            nitrous.patient = self.patient
            self.navigationController?.pushViewController(nitrous, animated: true)
         } else if formNames.contains(kSilverDiamine) {
            let silver = consentStoryBoard.instantiateViewController(withIdentifier: "SilverDiamine1VC") as! SilverDiamine1ViewController
            silver.patient = self.patient
            self.navigationController?.pushViewController(silver, animated: true)
         } else if formNames.contains(kBoneGrafting) {
            let bone = consentStoryBoard.instantiateViewController(withIdentifier: "BoneGrafting1VC") as! BoneGrafting1ViewController
            bone.patient = self.patient
            self.navigationController?.pushViewController(bone, animated: true)

         } else if formNames.contains(kOralSurgery) {
            let silver = consentStoryBoard1.instantiateViewController(withIdentifier: "OralSurgerVC") as! OralSurgerVC
            silver.patient = self.patient
            self.navigationController?.pushViewController(silver, animated: true)
            
         }else if formNames.contains(kRootCanal) {
            let silver = consentStoryBoard1.instantiateViewController(withIdentifier: "RootCanalVC") as! RootCanalVC
            silver.patient = self.patient
            self.navigationController?.pushViewController(silver, animated: true)
            
         } else if formNames.contains(kDentalExtract) {
            let silver = consentStoryBoard1.instantiateViewController(withIdentifier: "DentalExtractVC") as! DentalExtractVC
            silver.patient = self.patient
            self.navigationController?.pushViewController(silver, animated: true)
        }else if formNames.contains(kNoticeOfPrivacy) {
            let silver = consentStoryBoard1.instantiateViewController(withIdentifier: "PrivacyPracticesVC") as! NoticeOfPrivacyPractices1ViewController
            silver.patient = self.patient
            self.navigationController?.pushViewController(silver, animated: true)
            
        }else if formNames.contains(kAcknowledgementOfPrivacy) {
            if patient.signRefused == true{
                let policy = consentStoryBoard1.instantiateViewController(withIdentifier: "acknowledgementVC") as! AcknowledgementOfPrivacyPracticesViewController
                policy.patient = patient
                navigationController?.pushViewController(policy, animated: true)
            }else{
            let silver = consentStoryBoard1.instantiateViewController(withIdentifier: "acknowledgement") as! AcknowledgementOfPrivacyPractices1ViewController
            silver.patient = self.patient
            self.navigationController?.pushViewController(silver, animated: true)
            }
        } else {
            NotificationCenter.default.post(name: Notification.Name(rawValue: kFormsCompletedNotification), object: nil)
            _ = self.navigationController?.popToRootViewController(animated: true)
        }
    }
}
