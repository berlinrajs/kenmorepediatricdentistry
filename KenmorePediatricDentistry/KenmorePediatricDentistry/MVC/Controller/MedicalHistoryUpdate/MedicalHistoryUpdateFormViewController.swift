//
//  MedicalHistoryUpdateFormViewController.swift
//  ABC Clinic
//
//  Created by Leojin Bose on 2/29/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class MedicalHistoryUpdateFormViewController: MCViewController {

    @IBOutlet weak var constraintTableViewForm1Height: NSLayoutConstraint!
    @IBOutlet weak var constraintTableViewForm4Height: NSLayoutConstraint!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelBirthDate: UILabel!
    @IBOutlet weak var labelDateCreated: UILabel!
    @IBOutlet weak var buttonAlergicOthers: UIButton!
    @IBOutlet weak var labelAlergicOthers: MCLabel!
    @IBOutlet weak var radioButtonSubstances: RadioButton!
    @IBOutlet weak var labelSubstances: MCLabel!
    @IBOutlet weak var radioButtonIllness: RadioButton!
    @IBOutlet weak var labelIllness: MCLabel!
    @IBOutlet weak var textViewComments: MCTextView!
    @IBOutlet weak var imageViewSignature: UIImageView!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var patietnSignName: UILabel!
    @IBOutlet var arrayButtons: [UIButton]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        patietnSignName.text = patient.fullName
        
        buttonSubmit!.backgroundColor = UIColor.green
        constraintTableViewForm1Height.constant = CGFloat(patient.medicalHistoryQuestions1.count * 26)
        constraintTableViewForm4Height.constant = CGFloat((19 * 15) + 18)

        labelName.text = "Patient Name: \(patient.fullName)"
        labelBirthDate.text = "Birth Date: \(patient.dateOfBirth!)"
        labelDateCreated.text = "Date Created: \(patient.dateToday!)"
        
        if let alergic = patient.othersTextForm3 {
            buttonAlergicOthers.isSelected = true
            labelAlergicOthers.text = " " + alergic
        }
        if let substances = patient.controlledSubstances {
            radioButtonSubstances.isSelected = true
            labelSubstances.text = " " + substances
        } else if patient.controlledSubstancesClicked != nil {
            radioButtonSubstances.isSelected = false
        }
        
//        if let illness = patient.otherIllness {
//            radioButtonIllness.selected = true
//            labelIllness.text = " " + illness
//        } else {
//            radioButtonIllness.selected = false
//        }
        
        radioButtonIllness.setSelectedWithTag(patient.buttonIllness)
        labelIllness.text = patient.buttonIllness == 2 ? "" : patient.otherIllness
        
        if let comment = patient.comments {
            textViewComments.text = comment
        }
        
        imageViewSignature.image = patient.medicalSignature
        labelDate.text = patient.dateToday
        
        for (idx, buttonQuestion) in self.arrayButtons.enumerated() {
            let obj = patient.medicalHistoryQuestions2[idx]
            buttonQuestion.setTitle(" " + obj.question, for: .normal)
            if let selected = obj.selectedOption {
                buttonQuestion.isSelected = selected
            } else {
                buttonQuestion.isSelected = false
            }
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


extension MedicalHistoryUpdateFormViewController : UICollectionViewDataSource {
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionView.tag == 1 ? patient.medicalHistoryQuestions2.count : patient.medicalHistoryQuestions3.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellMedicalHistory", for: indexPath) as! MedicalHistoryCollectionViewCell
        let obj = collectionView.tag == 1 ? patient.medicalHistoryQuestions2[indexPath.item] : patient.medicalHistoryQuestions3[indexPath.item]
        cell.configureCellOption(obj: obj)
        return cell
    }
}

extension MedicalHistoryUpdateFormViewController : UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 1 {
            return patient.medicalHistoryQuestions1.count
        } else {
            let currentIndex = tableView.tag - 2
            return currentIndex < 3 ? 19 : 20
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.tag == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellMedicalHistoryFrom1", for: indexPath) as! MedicalHistoryFormTableViewCell1
            let obj = patient.medicalHistoryQuestions1[indexPath.row]
            cell.configureCell(obj: obj)
            return cell
        } else {
            let currentIndex = tableView.tag - 2
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellMedicalHistoryFrom4", for: indexPath) as! MedicalHistoryStep1TableViewCell
            let index = (currentIndex * 19) + indexPath.row
            let obj = patient.medicalHistoryQuestions4[index]
            cell.configureCell(obj: obj)
            let stringObj = ["AIDS/HIV Positive", "Artificial heart valve", "Artificial Joint", "Cancer", "Chemotherapy", "Diabetes", "Drug Addiction", "Excessive Bleeding", "Heart Attack / Failure", "Heart Pacemaker", "Heart Trouble / Disease", "Hepatitis A", "Hepatitis B or C", "High Blood Pressure", "Tuberculosis"]
            if let selected = obj.selectedOption {
                cell.labelTitle.textColor = selected && stringObj.contains(obj.question) ? UIColor.red : UIColor.black
                cell.labelTitle.font = UIFont(name:selected ? "WorkSans-Medium" :  "WorkSans-Regular", size: cell.labelTitle.font.pointSize)
                cell.buttonYes.isSelected = selected
            } else {
                cell.labelTitle.textColor = UIColor.black
                cell.labelTitle.font = UIFont(name: "WorkSans-Regular", size: cell.labelTitle.font.pointSize)
                cell.buttonYes.deselectAllButtons()
            }
            return cell
        }
    }
}
