//
//  MedicalHistoryUpdateStep1ViewController.swift
//  ABC Clinic
//
//  Created by Leojin Bose on 2/26/16.
//  Copyright © 2016 SRS. All rights reserved..
//

import UIKit

class MedicalHistoryUpdateStep1ViewController: MCViewController {
    
    @IBOutlet weak var tableViewQuestions: UITableView!
    @IBOutlet weak var buttonVerified: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.fetchData()
    }
    
    func fetchData() {
        if patient.medicalHistoryQuestions1 == nil {
            self.patient.medicalHistoryQuestions1 = MCQuestion.getArrayOfQuestions(questions: [
                "Are you under a physician's care now?",
                "Have you ever been hospitalized or had a major operation?",
                "Have you ever had a serious head or neck injury?",
                "Are you taking any medications, pills or drugs?",
                "Do you take, or have you taken, Phen-fen or Redux?",
                "Have you ever taken Fosamax, Boniva, Actonel or any other medications containing bisphosphonates?",
                "Are you on a special diet?",
                "Do you use tobacco?"])
            
            self.patient.medicalHistoryQuestions1[0].isAnswerRequired = true
            self.patient.medicalHistoryQuestions1[1].isAnswerRequired = true
            self.patient.medicalHistoryQuestions1[2].isAnswerRequired = true
            self.patient.medicalHistoryQuestions1[3].isAnswerRequired = true
            self.patient.medicalHistoryQuestions1[4].isAnswerRequired = true
            self.patient.medicalHistoryQuestions1[5].isAnswerRequired = true
        }
        self.tableViewQuestions.reloadData()
    }
    
    @IBAction func buttonActionNext(sender: AnyObject) {
        if !buttonVerified.isSelected {
            self.showAlert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
        } else {
            let medicalHistoryUpdateStep2VC = self.storyboard?.instantiateViewController(withIdentifier: "kMedicalHistoryUpdateStep2ViewController") as! MedicalHistoryUpdateStep2ViewController
            medicalHistoryUpdateStep2VC.patient = patient
            self.navigationController?.pushViewController(medicalHistoryUpdateStep2VC, animated: true)
        }
    }
    
    @IBAction func buttonVerifiedAction(sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    func findEmptyValue() -> MCQuestion? {
        for question in self.patient.medicalHistoryQuestions1 {
            if question.selectedOption == nil {
                return question
            }
        }
        return nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension MedicalHistoryUpdateStep1ViewController : MedicalHistoryCellDelegate {
    func radioButtonAction(sender: RadioButton) {
        let obj = patient.medicalHistoryQuestions1[sender.tag]
        PopupTextView.popUpView().showWithPlaceHolder("IF YES TYPE HERE") { (popUpView, textView) in
            if !textView.isEmpty {
                obj.answer = textView.text
                obj.selectedOption = true
            } else {
                sender.isSelected = false
                obj.selectedOption = false
            }
            popUpView.removeFromSuperview()
            self.tableViewQuestions.reloadData()
        }
    }
}


extension MedicalHistoryUpdateStep1ViewController : UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return patient.medicalHistoryQuestions1.count
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellMedicalHistoryStep1", for: indexPath as IndexPath) as! MedicalHistoryStep1TableViewCell
        let obj = patient.medicalHistoryQuestions1[indexPath.row]
        cell.configureCell(obj: obj)
        cell.buttonYes.tag = indexPath.row
        cell.delegate = self
        return cell
        
        
    }
}
