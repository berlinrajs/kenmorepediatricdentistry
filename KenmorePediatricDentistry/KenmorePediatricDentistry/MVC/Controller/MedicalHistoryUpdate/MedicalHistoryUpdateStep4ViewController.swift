//
//  MedicalHistoryStep4ViewController.swift
//  ABC Clinic
//
//  Created by Leojin Bose on 2/26/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class MedicalHistoryUpdateStep4ViewController: MCViewController {

    var currentIndex : Int = 0
    @IBOutlet weak var buttonNext: MCButton!
    @IBOutlet weak var buttonBack1: MCButton!
    @IBOutlet weak var tableViewQuestions: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var buttonVerified: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchData()
        // Do any additional setup after loading the view.
    }
    
    func fetchData() {
        if self.patient.medicalHistoryQuestions4 == nil {
            self.patient.medicalHistoryQuestions4 = MCQuestion.getArrayOfQuestions(questions: ["AIDS / HIV Positive", "Alzheimer's disease", "Anaphylaxis", "Anemia", "Angina", "Arthritis / Gout", "Artificial heart valve", "Artificial Joint", "Asthma", "Blood Disease", "Blood Transfusion", "Breathing Problem", "Bruise Easily", "Cancer", "Chemotherapy", "Chest pains", "Cold sores / Fever blisters", "Congenital heart disorder", "Convulsions", "Cortisone medicine", "Diabetes", "Drug Addiction", "Easily Winded", "Emphysema", "Epilepsy or Seizures", "Excessive Bleeding", "Excessive Thirst", "Fainting spells / Dizziness", "Frequent Cough", "Frequent Diarrhea", "Frequent Headaches", "Genital Herpes", "Glaucoma", "Hay Fever", "Heart Attack / Failure", "Heart Murmur", "Heart Pacemaker", "Heart Trouble / Disease", "Hemophilia", "Hepatitis A", "Hepatitis B or C", "Herpes", "High Blood Pressure", "High Cholesterol", "Hives or Rash", "Hypoglycemia", "Irregular Heartbeat", "Kidney Problems", "Leukemia", "Liver Disease", "Low Blood Pressure", "Lung disease", "Mitral Valve Prolapse", "Osteoporosis", "Pain in Jaw Joints", "Parathyroid Disease", "Psychiatric Care", "Radiation Treatments", "Recent Weight Loss", "Renal Dialysis", "Rheumatic Fever", "Rheumatism", "Scarlet Fever", "Shingles", "Sickle Cell Disease", "Sinus Trouble", "Spina Bifida", "Stomach / Intestinal Disease", "Stroke", "Swelling of Limbs", "Thyroid Disease", "Tonsillitis", "Tuberculosis", "Tumors or Growths", "Ulcers", "Venereal Disease", "Yellow Jaundice"])
        }
        self.activityIndicator.stopAnimating()
        self.tableViewQuestions.reloadData()
    }
    
    @IBAction func buttonActionBack1(withSender sender: AnyObject) {
        if currentIndex == 0 {
            _ = self.navigationController?.popViewController(animated: true)
        } else {
            buttonBack1.isUserInteractionEnabled = false
            buttonNext.isUserInteractionEnabled = false
            self.buttonVerified.isSelected = true
            self.activityIndicator.startAnimating()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.activityIndicator.stopAnimating()
                self.currentIndex = self.currentIndex - 1
                self.tableViewQuestions.reloadData()
                self.buttonBack1!.isUserInteractionEnabled = true
                self.buttonNext.isUserInteractionEnabled = true
            }
        }
    }
    @IBAction func buttonVerifiedAction(withSender sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    @IBAction func buttonActionNext(withsender sender: AnyObject) {
        if ((currentIndex + 1) * 20) < self.patient.medicalHistoryQuestions4.count {
            if !buttonVerified.isSelected {
                self.showAlert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
            } else {
                buttonBack1.isUserInteractionEnabled = false
                buttonNext.isUserInteractionEnabled = false
                self.buttonVerified.isSelected = false
                self.activityIndicator.startAnimating()
                 DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)  {
                    self.activityIndicator.stopAnimating()
                    self.currentIndex = self.currentIndex + 1
                    self.tableViewQuestions.reloadData()
                    self.buttonBack1.isUserInteractionEnabled = true
                    self.buttonNext.isUserInteractionEnabled = true
                }
            }
            
        } else {
            if !buttonVerified.isSelected {
                self.showAlert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
            } else {
                patient.medicalHistoryQuestions4 = self.patient.medicalHistoryQuestions4
                let medicalHistoryUpdateStep5VC = self.storyboard?.instantiateViewController(withIdentifier: "kMedicalHistoryUpdateStep5ViewController") as! MedicalHistoryUpdateStep5ViewController
                medicalHistoryUpdateStep5VC.patient = patient
                self.navigationController?.pushViewController(medicalHistoryUpdateStep5VC, animated: true)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension MedicalHistoryUpdateStep4ViewController : MedicalHistoryCellDelegate {
    func radioButtonAction(sender: RadioButton) {
        self.tableViewQuestions.reloadData()
    }
}


extension MedicalHistoryUpdateStep4ViewController : UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.patient.medicalHistoryQuestions4 == nil ? 0 : 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentIndex < 3 ? 20 : 17
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellMedicalHistoryStep4", for: indexPath as IndexPath) as! MedicalHistoryStep1TableViewCell
        let index = (currentIndex * 20) + indexPath.row
        let obj = patient.medicalHistoryQuestions4[index]
        cell.configureCell(obj: obj)
        if let selected = obj.selectedOption {
            cell.buttonYes.isSelected = selected
        } else {
            cell.buttonYes.deselectAllButtons()
        }
        cell.buttonYes.tag = index
        cell.delegate = self
        return cell
    }
}
