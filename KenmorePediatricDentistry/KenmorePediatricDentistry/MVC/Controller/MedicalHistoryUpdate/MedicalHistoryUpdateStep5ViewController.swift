//
//  MedicalHistoryUpdateStep5ViewController.swift
//  ABC Clinic
//
//  Created by Leojin Bose on 2/29/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit
class MedicalHistoryUpdateStep5ViewController: MCViewController {
    
    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var labelDate: MCLabel!
    @IBOutlet weak var radioButton: RadioButton!
    @IBOutlet weak var textViewComments: MCTextView!
    @IBOutlet weak var signName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        signName.text = patient.fullName
        signatureView.layer.cornerRadius = 3.0
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(MedicalHistoryStep5ViewController.setDateOnLabel))
        tapGesture.numberOfTapsRequired = 1
        labelDate.addGestureRecognizer(tapGesture)
        if patient.buttonIllness != nil {
        radioButton.setSelectedWithTag(patient.buttonIllness)
        }
        textViewComments.placeholder = "PLEASE TYPE HERE"
        textViewComments.delegate!.textViewDidBeginEditing!(textViewComments)
        textViewComments.text = patient.comments
        textViewComments.delegate!.textViewDidEndEditing!(textViewComments)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setDateOnLabel() {
        labelDate.text = patient.dateToday
        labelDate.textColor = UIColor.black
    }
    
    func saveValues() {
      patient.buttonIllness = radioButton.selected.tag
        patient.comments = textViewComments.text == textViewComments.placeholder ? "" : textViewComments.text
        
        patient.medicalSignature = signatureView.signatureImage()
    }
    
    override func buttonBackAction() {
        saveValues()
        super.buttonBackAction()
    }

    
@IBAction func buttonActionSubmit(withsender sender: AnyObject) {
        self.view.endEditing(true)
        if !signatureView.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if labelDate.text == "Tap to date" {
            self.showAlert("PLEASE SELECT DATE")
        } else {
            
            saveValues()

            
            let medicalHisUpdateForm = self.storyboard?.instantiateViewController(withIdentifier: "kMedicalHistoryUpdateFormViewController") as! MedicalHistoryUpdateFormViewController
            medicalHisUpdateForm.patient = self.patient
            self.navigationController?.pushViewController(medicalHisUpdateForm, animated: true)
        }
    }
    
    @IBAction func buttonActionYesNo(sender: RadioButton) {
        if sender.tag == 2 {
            patient.otherIllness = nil
        } else {
            PopupTextView.popUpView().showWithPlaceHolder("IF YES, PLEASE EXPLAIN", completion: { (popUpView, textView) in
                if !textView.isEmpty {
                    self.patient.otherIllness = textView.text
                } else {
                    self.patient.otherIllness = nil
                    self.radioButton.setSelectedWithTag(2)
                }
                popUpView.removeFromSuperview()
            })
        }
    }
}
