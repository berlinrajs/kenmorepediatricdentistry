//
//  NitrousOxideFormViewController.swift
//  KenmorePediatricDentistry
//
//  Created by Bala Murugan on 1/24/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class NitrousOxideFormViewController: MCViewController {

    var signPatient : UIImage!
    var signDentist : UIImage!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelParentName : UILabel!
    @IBOutlet weak var signaturePatient : UIImageView!
    @IBOutlet weak var signatureDentist : UIImageView!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var labelDate2 : UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        labelPatientName.text = patient.fullName
        labelParentName.text = patient.nitrousParentName
        signaturePatient.image = signPatient
        signatureDentist.image = signDentist
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
