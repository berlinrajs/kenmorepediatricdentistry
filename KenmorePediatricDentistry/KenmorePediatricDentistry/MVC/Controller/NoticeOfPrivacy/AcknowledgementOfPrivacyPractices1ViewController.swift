//
//  AcknowledgementOfPrivacyPractices1ViewController.swift
//  KenmorePediatricDentistry
//
//  Created by SRS Websolutions on 2/7/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class AcknowledgementOfPrivacyPractices1ViewController: MCViewController {
    
    
    @IBOutlet weak var viewDropDown: BRDropDown!
    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var labelPatientName: UILabel!
    @IBOutlet weak var textFieldDependentFamily: MCTextView!
    @IBOutlet weak var signature: SignatureView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        labelDate.todayDate = patient.dateToday
        viewDropDown.placeholder = "-- PLEASE SELECT --"
        viewDropDown.items = ["SELF","PARENT","GUARDIAN","OTHER"]
//        viewDropDown.dropDownBorderColor = UIColor.gray
        labelPatientName.text = patient.fullName
        
        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func loadValue (){
        let formNames = (patient.selectedForms as NSArray).value(forKey: "formTitle") as! [String]
        if formNames.contains(kNewPatientSignInForm){

         viewDropDown.selectedIndex = patient.relationTAG
         viewDropDown.selectedOption = patient.relation
        textFieldDependentFamily.text = patient.dependentFamily == "" ? "TYPE DETAILS HERE" : patient.dependentFamily
        textFieldDependentFamily.textColor = patient.dependentFamily == "" ? UIColor.lightGray : UIColor.black
        }else{
            viewDropDown.selectedIndex = patient.relationTAG1
            viewDropDown.selectedOption = patient.relation1
            textFieldDependentFamily.text = patient.dependentFamily1 == "" ? "TYPE DETAILS HERE" : patient.dependentFamily1
            textFieldDependentFamily.textColor = patient.dependentFamily1 == "" ? UIColor.lightGray : UIColor.black
        }
    }
    
    func saveValue (){
       let formNames = (patient.selectedForms as NSArray).value(forKey: "formTitle") as! [String]
        if formNames.contains(kNewPatientSignInForm){
        
            patient.relationTAG = viewDropDown.selectedIndex
            patient.relation = viewDropDown.selectedOption
            patient.dependentFamily = textFieldDependentFamily.isEmpty ? "" : textFieldDependentFamily.text!
            patient.patientNamePrivacy = labelPatientName.text
            

            
        }else{
        
            patient.relationTAG1 = viewDropDown.selectedIndex
            patient.relation1 = viewDropDown.selectedOption
            patient.dependentFamily1 = textFieldDependentFamily.isEmpty ? "" : textFieldDependentFamily.text!
            patient.patientNamePrivacy1 = labelPatientName.text

        }
        patient.Signature1 = signature.image
      
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    
    
    @IBAction func buttonDoneAction(_ sender: Any) {
 
        if !signature.isSigned()
        {
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped
        {
            self.showAlert("PLEASE SELECT THE DATE")
            
        }else{
            saveValue()
            let acknowledge = consentStoryBoard1.instantiateViewController(withIdentifier: "officePolicyVC") as! OfficePolicyViewController
            acknowledge.patient = patient
            navigationController?.pushViewController(acknowledge, animated: true)
     
        }
        
    }
    
    
 

}
