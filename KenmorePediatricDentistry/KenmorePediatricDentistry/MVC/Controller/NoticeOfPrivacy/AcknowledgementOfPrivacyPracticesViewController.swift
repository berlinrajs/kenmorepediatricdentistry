//
//  AcknowledgementOfPrivacyPracticesViewController.swift
//  KenmorePediatricDentistry
//
//  Created by SRS Websolutions on 2/7/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class AcknowledgementOfPrivacyPracticesViewController: MCViewController {
    
    
    @IBOutlet weak var labelDateOfficePolicy: UILabel!
    @IBOutlet weak var signOfficePolicy: UIImageView!
    @IBOutlet weak var textFieldDependentFamily: UILabel!
    @IBOutlet weak var labelRelation: MCLabel!
    @IBOutlet weak var labelDate: FormLabel!
    @IBOutlet weak var labelPatientName: FormLabel!
    @IBOutlet weak var signImage: UIImageView!
    @IBOutlet weak var labelRefuslaOther : UILabel!
    @IBOutlet weak var radioRefusal: RadioButton!
   
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        if patient.signRefused == true{
            labelPatientName.text = patient.fullName
            labelDate.text = patient.dateToday
            radioRefusal.setSelectedWithTag(patient.signRefusalTag)
            labelRefuslaOther.text = patient.signRefusalOther
        }else{
        labelRelation.text = patient.relation
        labelDate.text = patient.dateToday
        labelPatientName.text = patient.fullName
        signImage.image = patient.Signature1
        textFieldDependentFamily.text = patient.dependentFamily
        labelDateOfficePolicy.text = patient.dateToday
        signOfficePolicy.image = patient.signPolicy
        }
        
        buttonBack?.isHidden = (patient.signRefused == true) && isFromPreviousForm

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
