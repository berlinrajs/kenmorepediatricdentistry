//
//  NoticeOfPrivacyPractices1ViewController.swift
//  KenmorePediatricDentistry
//
//  Created by SRS Websolutions on 2/6/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class NoticeOfPrivacyPractices1ViewController: MCViewController {

    @IBOutlet weak var patientSignature: SignatureView!
    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var labelPatientName : UILabel!
    
    var signRefused: Bool!
    var signRefusalTag: Int!
    var signRefusalOther: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        labelDate.todayDate = patient.dateToday
        labelPatientName.text = patient.fullName
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func buttonBackAction(_ sender: Any) {
        
        super.buttonBackAction()
    }


    @IBAction func buttonDoneAction(_ sender: Any) {
        
        if !patientSignature.isSigned()
        {
            self.showAlert("PLEASE SIGN THE FORM")
        }
        else if !labelDate.dateTapped
        {
            self.showAlert("PLEASE SELECT DATE")
        }
        else
        {
            patient.signPatient = patientSignature.signatureImage()
//            let privacy = consentStoryBoard1.instantiateViewController(withIdentifier: "privacyPractice") as! NoticeOfPrivacyPracticesViewController
//            privacy.patient = self.patient
//         navigationController?.pushViewController(privacy, animated: true)
            
            
            HipaaSignatureRefusalPopup.popUpView().showInViewController(viewController: self, completion: { (popUpView, refused, textFieldOther, refusalTag) in
                popUpView.close()
                self.patient.signRefused = refused
                self.patient.signRefusalTag = refused == false ? 0 : refusalTag
                self.patient.signRefusalOther = refused == false ? "" : refusalTag == 4 ? textFieldOther.text! : ""
              
                let policy = consentStoryBoard1.instantiateViewController(withIdentifier: "acknowledgement") as! AcknowledgementOfPrivacyPractices1ViewController
                policy.patient = self.patient
                self.navigationController?.pushViewController(policy, animated: true)
              
                
            }, error: { (message) in
                self.showAlert(message)
            })
            
        }
    }
}
