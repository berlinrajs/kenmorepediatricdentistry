//
//  NoticeOfPrivacyPracticesViewController.swift
//  KenmorePediatricDentistry
//
//  Created by SRS Websolutions on 2/6/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class NoticeOfPrivacyPracticesViewController: MCViewController {
   
    var date : String!
    @IBOutlet weak var labelPatientName : UILabel!

    @IBOutlet weak var patientSignatureForm:
    UIImageView!
    
    @IBOutlet weak var labelDate: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        patientSignatureForm.image = patient.signPatient
        labelDate.text = patient.dateToday
        labelPatientName.text = patient.fullName

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
