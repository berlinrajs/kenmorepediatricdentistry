//
//  OfficePolicyViewController.swift
//  KenmorePediatricDentistry
//
//  Created by SRS Websolutions on 2/7/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class OfficePolicyViewController: MCViewController {

    var date : String!
    var sign : UIImage!
    @IBOutlet weak var signPolicy: SignatureView!
    @IBOutlet weak var datePolicy: DateLabel!
    @IBOutlet weak var labelPatientName : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view..
        
        datePolicy.todayDate = patient.dateToday
        labelPatientName.text = patient.fullName
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    @IBAction func buttonActionDone(_ sender: Any) {
        if !signPolicy.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !datePolicy.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            patient.signPolicy = signPolicy.signatureImage()
//            let policy = consentStoryBoard1.instantiateViewController(withIdentifier: "acknowledgementVC") as! AcknowledgementOfPrivacyPracticesViewController
//            policy.patient = patient
//            navigationController?.pushViewController(policy, animated: true)
            
            let formNames = (patient.selectedForms as NSArray).value(forKey: "formTitle") as! [String]
            if formNames.contains(kNewPatientSignInForm) || formNames.contains(kPatientSignInForm){
                    let  formVC = patientStoryBoard1.instantiateViewController(withIdentifier: "AdultSignInForm") as! AdultSignInForm
                    formVC.patient = self.patient
                    self.navigationController?.pushViewController(formVC, animated: true)
            }else{
            
            let step2VC = patientStoryBoard.instantiateViewController(withIdentifier: "ChildSignInForm") as! ChildSignInForm
                step2VC.patient = self.patient
            self.navigationController?.pushViewController(step2VC, animated: true)

            }
        }
    }
    
}
