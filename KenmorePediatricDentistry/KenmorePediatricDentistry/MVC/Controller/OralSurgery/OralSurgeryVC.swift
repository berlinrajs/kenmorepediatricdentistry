//
//  NitrousOxide1ViewController.swift
//  KenmorePediatricDentistry
//
//  Created by Bala Murugan on 1/24/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class OralSurgerVC: MCViewController {

    @IBOutlet weak var textfieldParentName : MCTextField!
    @IBOutlet weak var signatureParent : SignatureView!
    @IBOutlet weak var signatureDentist : SignatureView!
    @IBOutlet weak var labelDate1 : DateLabel!
    @IBOutlet weak var labelDate2 : DateLabel!
    @IBOutlet weak var signLabel : UILabel!
    @IBOutlet weak var SignNameLabel : UILabel!
    @IBOutlet weak var textViewProcedure : MCTextView!
    @IBOutlet weak var textViewProcedure1 : MCTextView!
    @IBOutlet weak var labelDetails : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday
        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue (){
        let string : NSString = labelDetails.text!.replacingOccurrences(of: "KPATIENTNAME", with: patient.fullName) as NSString
        let range = string.range(of: patient.fullName)
        let attributedString = NSMutableAttributedString(string: string as String)
        attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range)
        labelDetails.attributedText = attributedString

//        if !patient.is18YearsOld{
//                signLabel.text = "Parent or Guardian Signature"
//                SignNameLabel.text = ""
//                textfieldParentName.isUserInteractionEnabled = true
//                textfieldParentName.text = patient.oralParentName
//        }else{
//            signLabel.text = "Patient Signature"
//            SignNameLabel.text = patient.fullName
//            textfieldParentName.text = ""
//            textfieldParentName.alpha = 0.5
//        }
      
        textViewProcedure.textValue = patient.oralProcedure
        textViewProcedure1.textValue = patient.oralProcedure1
        // textfieldParentName.text = patient.oralParentName
    }
    
    func saveValue (){
        patient.oralProcedure = textViewProcedure.textValue
        patient.oralProcedure1 = textViewProcedure1.textValue
        patient.oralParentName = textfieldParentName.text
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }

    @IBAction func onNextButtonPressed (withSender sender : UIButton){
         if !signatureParent.isSigned() || !signatureDentist.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate1.dateTapped || !labelDate2.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            saveValue()
            let nitrous = consentStoryBoard1.instantiateViewController(withIdentifier: "OralSurgerForm") as! OralSurgerForm
            nitrous.patient = self.patient
            nitrous.signPatient = signatureParent.signatureImage()
            nitrous.signDentist = signatureDentist.signatureImage()
            self.navigationController?.pushViewController(nitrous, animated: true)

        }
    }


}
