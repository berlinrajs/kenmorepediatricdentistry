//
//  PatientInfoViewController.swift
//  Always Great Smiles
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientInfoViewController: MCViewController {
    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var textFieldFirstName: MCTextField!
    @IBOutlet weak var textFieldLastName: MCTextField!
    @IBOutlet weak var textFieldMiddleInitial: MCTextField!
    @IBOutlet weak var textFieldPreferredName: MCTextField!
    @IBOutlet weak var textFieldDate: MCTextField!
    @IBOutlet weak var textFieldMonth: MCTextField!
    @IBOutlet weak var textFieldYear: MCTextField!
    @IBOutlet weak var labelDentist: UILabel!
    @IBOutlet weak var labelPlace: UILabel!
    @IBOutlet weak var dropDownDentistName: BRDropDown!
    
      var isNewPateint: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textFieldMiddleInitial.textFormat = .MiddleInitial
        textFieldDate.textFormat = .Date
        textFieldMonth.textFormat = .Month
        textFieldYear.textFormat = .Year

        if kDentistNames.count == 0 {
            dropDownDentistName.isHidden = true
            labelDentist.isHidden = true
        } else if kDentistNames.count > 1 {
            dropDownDentistName.items = kDentistNames
            dropDownDentistName.placeholder = isDentistNameNeeded ? "-- DENTIST NAME * --" : "-- DENTIST NAME --"
            dropDownDentistName.isHidden = false
            labelDentist.isHidden = true
        } else {
            labelDentist.text = "DENTIST: " + kDentistNames[0].uppercased()
            dropDownDentistName.isHidden = true
            labelDentist.isHidden = false
        }
        
        labelPlace.text = kPlace
        labelDate.text = patient.dateToday
        
//        textFieldFirstName.text = patient.visitorFirstName == nil ? "" : patient.visitorFirstName
//        textFieldLastName.text = patient.visitorLastName == nil ? "" : patient.visitorLastName
        // Do any additional setup after loading the view.
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonActionNext(_ sender : AnyObject) {
        dropDownDentistName.selected = false
        self.view.endEditing(true)
        if kDentistNames.count > 1 && isDentistNameNeeded && dropDownDentistName.selectedIndex == 0 {
            self.showAlert("PLEASE SELECT THE DENTIST NAME")
        } else if textFieldFirstName.isEmpty {
            self.showAlert("PLEASE ENTER PATIENT FIRST NAME")
        } else if textFieldLastName.isEmpty {
            self.showAlert("PLEASE ENTER PATIENT LAST NAME")
        } else if invalidDateofBirth {
            self.showAlert("PLEASE ENTER THE VALID DATE OF BIRTH")
        } else {
            
            patient.firstName = textFieldFirstName.text
            patient.lastName = textFieldLastName.text
            patient.initial = textFieldMiddleInitial.text
            patient.preferredName = textFieldPreferredName.text
            patient.dateOfBirth = getDateOfBirth()
            patient.dentistName = isDentistNameNeeded ? (kDentistNames.count > 1 ? dropDownDentistName.selectedOption! : (kDentistNames.count > 0 ? kDentistNames[0] : "")) : ""
            
            #if AUTO
                
                func showMoreThanOneUserAlert()  {
                    let alertController = UIAlertController(title: kAppName, message: "More than one user found. Please handover the device to front desk to enter your patient id", preferredStyle: UIAlertControllerStyle.alert)
                    let alertYesAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
                        let newPatientStep1VC = mainStoryBoard.instantiateViewController(withIdentifier: "VerificationVC") as! VerificationViewController
                        newPatientStep1VC.patient = self.patient
                        self.navigationController?.pushViewController(newPatientStep1VC, animated: true)
                    }
                    alertController.addAction(alertYesAction)
                    self.present(alertController, animated: true, completion: nil)
                }
                
                func showUnableToFindAlert (){
                    let alertController = UIAlertController(title: kAppName, message: "Unable to find the patient \(textFieldFirstName.text!) \(textFieldLastName.text!) - \(patient.dateOfBirth!) mismatch", preferredStyle: UIAlertControllerStyle.alert)
                    let alertYesAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
                        showCreateNewPatientAlert()
                    }
                    alertController.addAction(alertYesAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                }
                
                func showCreateNewPatientAlert (){
                    let alertController = UIAlertController(title: kAppName, message: "Do you wish to create a new patient", preferredStyle: UIAlertControllerStyle.alert)
                    let alertYesAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (action) -> Void in
                        self.patient.patientDetails = nil
                        gotoPatientSignInForm()
                        
                    }
                    alertController.addAction(alertYesAction)
                    let alertNoAction = UIAlertAction(title: "No", style: UIAlertActionStyle.destructive) { (action) -> Void in
                        _ = self.navigationController?.popToRootViewController(animated: true)
                    }
                    alertController.addAction(alertNoAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                    
                }
                
                func gotoPatientSignInForm() {
                    let patientSignIn = patientStoryBoard1.instantiateViewController(withIdentifier: "AdultAddressVC") as! AdultAddressVC
                    patientSignIn.patient = self.patient
                    self.navigationController?.pushViewController(patientSignIn, animated: true)
                }
                
                func APICall() {
                    BRProgressHUD.show()
                    //                    self.buttonNext.isUserInteractionEnabled = false
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "MMM dd, yyyy"
                    let date = dateFormatter.date(from: patient.dateOfBirth)
                    dateFormatter.dateFormat = "yyyy-MM-dd"
                    let manager = AFHTTPSessionManager(baseURL: URL(string: hostUrl))
                    manager.responseSerializer.acceptableContentTypes = ["text/html"]
                    manager.post("consent_fetch_patient_info.php", parameters: ["first_name" : self.textFieldFirstName.text!, "last_name": self.textFieldLastName.text!, "dob": dateFormatter.string(from: date!)], progress: { (progress) in
                    }, success: { (task, result) in
                        //                        self.buttonNext.isUserInteractionEnabled = true
                        BRProgressHUD.hide()
                        if self.navigationController?.topViewController == self {
                            let response = result as! [String : AnyObject]
                            if response["status"] as! String == "success"  {
                                let patientDetails = response["patientData"] as! [String: AnyObject]
                                self.patient.patientDetails = PatientDetails(details: patientDetails)
                                self.gotoNextForm()
                            } else {
                                if response["status"] as! String == "failed" && (response["message"] as! String).contains("Not connected") {
                                    self.showAlert((response["message"] as! String).uppercased())
                                } else if response["status"] as! String == "matching_name"  {
                                    let patientDetails = response["matchingData"] as! [String: String]
                                    let alertController = UIAlertController(title: kAppName, message: "Did you mean \(patientDetails["Fname"]!) \(patientDetails["Lname"]!)? If so select YES and choose the correct date of birth", preferredStyle: UIAlertControllerStyle.alert)
                                    let alertOkAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (action) -> Void in
                                        self.textFieldFirstName.text = patientDetails["Fname"]
                                        self.textFieldLastName.text = patientDetails["Lname"]
                                        self.textFieldDate.text = ""
                                        self.textFieldMonth.text = ""
                                        self.textFieldYear.text = ""
                                    }
                                    alertController.addAction(alertOkAction)
                                    let alertNoAction = UIAlertAction(title: "No", style: UIAlertActionStyle.destructive) { (action) -> Void in
                                        showUnableToFindAlert()
                                    }
                                    alertController.addAction(alertNoAction)
                                    self.present(alertController, animated: true, completion: nil)
                                } else if response["status"] as! String == "multiple_patient_found" {
                                    showMoreThanOneUserAlert()
                                } else {
                                    self.patient.patientDetails = nil
                                    let alertController = UIAlertController(title: kAppName, message: "Patient not found. Are you sure the provided details are correct?", preferredStyle: UIAlertControllerStyle.alert)
                                    let alertYesAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (action) -> Void in
                                        showUnableToFindAlert()
                                    }
                                    alertController.addAction(alertYesAction)
                                    let alertNoAction = UIAlertAction(title: "No", style: UIAlertActionStyle.destructive) { (action) -> Void in
                                        
                                    }
                                    alertController.addAction(alertNoAction)
                                    self.present(alertController, animated: true, completion: nil)
                                }
                            }
                        }
                    }, failure: { (task, error) in
                        //                        self.buttonNext.isUserInteractionEnabled = true
                        BRProgressHUD.hide()
                        self.patient.patientDetails = nil
                        if self.navigationController?.topViewController == self {
                            self.showAlert(error.localizedDescription)
                        }
                    })
                }
                
                if self.isNewPateint == true {
                    
                    self.gotoNextForm()
                    
                } else {
                    resetValues()
                    APICall()
                }
            #else
                self.gotoNextForm()
                
            #endif
            

        }
    }
    
    
    func resetValues(){
        patient.adultAddress = nil
        patient.adultState = nil
        patient.adultZip = nil
        patient.adultCity = nil
        patient.adultHomePhone = nil
        patient.adultBusinessPhone = nil
        patient.adultCellPhone = nil
        patient.adultEmail = nil
        patient.adultSS = nil
        
    }
    
    var isDentistNameNeeded: Bool {
        get {
            for (_, form) in patient.selectedForms.enumerated() {
                if kDentistNameNeededForms.contains(form.formTitle) {
                    return true
                }
            }
            return false
        }
    }
    
    func getDateOfBirth() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        let dob = dateFormatter.date(from: textFieldMonth.text! + " " + textFieldDate.text! + ", " + textFieldYear.text!)!
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = kCommonDateFormat
        return dateFormatter1.string(from: dob).uppercased()
    }
    
    var invalidDateofBirth: Bool {
        get {
            if textFieldMonth.isEmpty || textFieldDate.isEmpty || textFieldYear.isEmpty {
                return true
            } else if Int(textFieldDate.text!)! == 0 {
                return true
            } else if !textFieldYear.text!.isValidYear {
                return true
            } else {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MMM-yyyy"
                
                let todayDate = dateFormatter.date(from: dateFormatter.string(from: Date()))
                let currentDate = dateFormatter.date(from: "\(textFieldDate.text!)-\(textFieldMonth.text!)-\(textFieldYear.text!)")
                
                if todayDate == nil || currentDate == nil {
                    return true
                }
                if todayDate!.timeIntervalSince(currentDate!) < 0 {
                    return true
                }
                return false
            }
        }
    }
}
