//
//  NitrousOxideFormViewController.swift
//  KenmorePediatricDentistry
//
//  Created by Bala Murugan on 1/24/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class RootCanalForm: MCViewController {

    var signPatient : UIImage!
    var signDentist : UIImage!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelParentName : UILabel!
    @IBOutlet weak var signaturePatient : UIImageView!
    @IBOutlet weak var signatureDentist : UIImageView!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var labelDate2 : UILabel!
    @IBOutlet weak var labelProcedure2 : UILabel!
    @IBOutlet weak var labelDetails : UILabel!

    override func viewDidLoad() {
        
        super.viewDidLoad()
        labelPatientName.text = patient.fullName
        labelParentName.text = patient.rootParentName
        signaturePatient.image = signPatient
        signatureDentist.image = signDentist
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        let form : Forms = patient.selectedForms.first!
        
        labelProcedure2.text = patient.rootProcedure1
        var string : NSString = labelDetails.text!.replacingOccurrences(of: "KPATIENTNAME", with: patient.fullName) as NSString
        string = string.replacingOccurrences(of: "KTOOTHNUMBERS", with:form.toothNumbers) as NSString
        let range = string.range(of: patient.fullName)
        let attributedString = NSMutableAttributedString(string: string as String)
        attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range)
        let range1 = string.range(of: form.toothNumbers)
        attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range1)
        labelDetails.attributedText = attributedString

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
