//
//  SilverDiamine1ViewController.swift
//  KenmorePediatricDentistry
//
//  Created by Bala Murugan on 1/25/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class SilverDiamine1ViewController: MCViewController {

    @IBOutlet weak var textfieldParentName : MCTextField!
    @IBOutlet weak var signatureParent : SignatureView!
    @IBOutlet weak var signatureDentist : SignatureView!
    @IBOutlet weak var labelDate1 : DateLabel!
    @IBOutlet weak var labelDate2 : DateLabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday
        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue (){
        textfieldParentName.setSavedText(text: patient.silverParentName)
    }
    
    func saveValue (){
        patient.silverParentName = textfieldParentName.getText()
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if textfieldParentName.isEmpty{
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        }else if !signatureParent.isSigned() || !signatureDentist.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate1.dateTapped || !labelDate2.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            saveValue()
            let silver = consentStoryBoard.instantiateViewController(withIdentifier: "SilverDiamineFormVC") as! SilverDiamineFormViewController
            silver.patient = self.patient
            silver.signPatient = signatureParent.signatureImage()
            silver.signDentist = signatureDentist.signatureImage()
            self.navigationController?.pushViewController(silver, animated: true)
        }
    }


}
