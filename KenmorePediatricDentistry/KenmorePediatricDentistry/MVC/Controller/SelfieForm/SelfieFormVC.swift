//
//  SelfieFormVC.swift
//  Bellevue Dental
//
//  Created by Berlin Raj on 13/12/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

import UIKit

class SelfieFormVC: MCViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    
    @IBOutlet weak var buttonInfo: UIButton!
    @IBOutlet weak var labelComment1: UILabel!
    @IBOutlet weak var labelComment2: UILabel!
    
    var selfieImage: UIImage!
    
    var isDrivingLicense: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.layer.borderColor = UIColor.lightGray.cgColor
        imageView.layer.cornerRadius = 3.0
        imageView.layer.masksToBounds = true
        imageView.layer.borderWidth = 1.0
        
        labelDate.text = "DATE: " + patient.dateToday
        
        labelName.text = "PATIENT NAME: " + patient.fullName.uppercased()
        
        self.imageView.image = selfieImage
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction override func onSubmitButtonPressed() {
        buttonInfo.isHidden = true
        
        if !Reachability.isConnectedToNetwork() {
            self.showAlert("Your device is not connected to internet. Please go to settings to connect.", buttonTitles: ["Settings", "Cancel"], completion: { (buttonIndex) in
                if buttonIndex == 0 {
                    let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
                    if let url = settingsUrl {
                        UIApplication.shared.openURL(url)
                    }
                } else {
                    
                }
            })
            return
        }
        let pdfManager = PDFManager.sharedInstance()
        pdfManager.authorizeDrive(self) { (success) -> Void in
            
            if success {
                self.buttonSubmit?.isHidden = true
                self.buttonBack?.isHidden = true
                
                pdfManager.uploadToGoogleDrive(self.pdfView == nil ? self.view : self.pdfView!, patient: self.patient, completionBlock: { (finished) -> Void in
                    if finished {
                        self.patient.selectedForms.removeFirst()
                        self.gotoNextForm()
                    } else {
                        self.buttonSubmit?.isHidden = false
                        self.buttonBack?.isHidden = false
                    }
                })
            } else {
                self.buttonSubmit?.isHidden = false
                self.buttonBack?.isHidden = false
            }
        }
    }
    
    
    @IBAction func buttonInfoAction() {
//        PopupTextView.popUpView().showWithPlaceHolder("COMMENTS") { (textView, isEdited) in
//            if textView.isEmpty {
//                self.buttonInfo.setTitle("TAP TO ADD/CHANGE COMMENT", forState: UIControlState.Normal)
//                self.labelComment1.text = ""
//                self.labelComment2.text = ""
//            } else {
//                self.buttonInfo.setTitle("", forState: UIControlState.Normal)
//                (textView as! MCTextView).textValue!.setTextForArrayOfLabels([self.labelComment1, self.labelComment2])
//            }
//
//        }
        PopupTextView.popUpView().showWithPlaceHolder("COMMENTS") { (popUpView, textView) in
            popUpView.close()
            if textView.isEmpty {
                self.buttonInfo.setTitle("TAP TO ADD/CHANGE COMMENT", for: UIControlState.normal)
                self.labelComment1.text = ""
                self.labelComment2.text = ""
            } else {
                self.buttonInfo.setTitle("", for: UIControlState.normal)
                textView.textValue!.setTextForArrayOfLabels([self.labelComment1, self.labelComment2])
            }
        }
    }
}
