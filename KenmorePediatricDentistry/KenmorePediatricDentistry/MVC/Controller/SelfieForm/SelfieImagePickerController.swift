//
//  SelfieImagePickerController.swift
//  Bellevue Dental
//
//  Created by Berlin Raj on 13/12/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

import UIKit
import AVFoundation

protocol SelfieImageCaptureDelegate {
    func selfieImagePicker(picker: SelfieImagePickerController, completedWithSelfieImage image: UIImage?)
    func selfieImagePickerDidCancel(picker: SelfieImagePickerController)
}

class SelfieImagePickerController: UIViewController {
    
    var captureSession = AVCaptureSession()
    var stillImageOutput: AVCaptureStillImageOutput!
    
    @IBOutlet var previewView: UIView!
    var delegate: SelfieImageCaptureDelegate!
    
    var isFrontCameraActive: Bool = false
    var capturePressed: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        edgesForExtendedLayout = []
        captureSession.beginConfiguration()
        captureSession.sessionPreset = AVCaptureSessionPreset640x480
        
        let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer?.frame = CGRect(x: 139.5, y: 171.5, width: 488, height: 651)
        previewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
        self.previewView.layer.addSublayer(previewLayer!)
        
        for device in AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo) {
            if (device as! AVCaptureDevice).position == AVCaptureDevicePosition.front {
                do {
                    let input = try AVCaptureDeviceInput(device: device as! AVCaptureDevice)
                    captureSession.addInput(input)
                    
                    stillImageOutput = AVCaptureStillImageOutput()
                    stillImageOutput.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
                    captureSession.addOutput(stillImageOutput)
                } catch {
                    
                }
            }
        }
        
        if let deviceInput = captureSession.inputs[0] as? AVCaptureDeviceInput {
            isFrontCameraActive = deviceInput.device.position == AVCaptureDevicePosition.front ? true : false
        } else {
            let device = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
            isFrontCameraActive = device?.position == AVCaptureDevicePosition.front ? true : false

            do {
                let input = try AVCaptureDeviceInput(device: device)
                captureSession.addInput(input)
                
                stillImageOutput = AVCaptureStillImageOutput()
                stillImageOutput.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
                captureSession.addOutput(stillImageOutput)
            } catch {
                
            }
        }
        
        captureSession.commitConfiguration()
        captureSession.startRunning()
    }
    @IBAction func toogleDevice() {
        if capturePressed == true {
            capturePressed = false
            return
        }
        captureSession.stopRunning()
        captureSession.beginConfiguration()
        
        for input in captureSession.inputs {
            captureSession.removeInput(input as! AVCaptureInput)
        }
        
        for device in AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo) {
            if ((device as! AVCaptureDevice).position == AVCaptureDevicePosition.front && isFrontCameraActive == false) || ((device as! AVCaptureDevice).position == AVCaptureDevicePosition.back && isFrontCameraActive == true) {
                do {
                    let input = try AVCaptureDeviceInput(device: device as! AVCaptureDevice)
                    captureSession.addInput(input)
                } catch {
                    
                }
            }
        }
        self.isFrontCameraActive = !isFrontCameraActive
        captureSession.commitConfiguration()
        captureSession.startRunning()
    }
    
    @IBAction func captureImage() {
        capturePressed = true
        var videoConnection: AVCaptureConnection!
        
        for connection in stillImageOutput.connections as! [AVCaptureConnection]{
            for port in connection.inputPorts {
                if (port as AnyObject).mediaType == AVMediaTypeVideo {
                    videoConnection = connection
                    break;
                }
            }
            if videoConnection != nil
            {
                break;
            }
        }
        print("about to request a capture from: \(stillImageOutput)")
        stillImageOutput.captureStillImageAsynchronously(from: videoConnection) { (buffer, error) in
            let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(buffer)
            let image = UIImage(data: imageData!)
            
            self.delegate.selfieImagePicker(picker: self, completedWithSelfieImage: image)
            self.capturePressed = false
        }
    }
    
    @IBAction func backAction() {
        if capturePressed == true {
            capturePressed = false
            return
        }
        captureSession.stopRunning()
        delegate.selfieImagePickerDidCancel(picker: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
