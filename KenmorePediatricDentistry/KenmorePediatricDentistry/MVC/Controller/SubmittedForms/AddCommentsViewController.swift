//
//  AddCommentsViewController.swift
//  WestKendall
//
//  Created by Berlin Raj on 10/27/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

protocol AddCommentsViewControllerDelegate {
    func submittedDetails(patient: MCPatient)
}

class AddCommentsViewController: MCViewController {
    
    var delegate: AddCommentsViewControllerDelegate?
    
    var pendingForm: PendingForm!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func buttonBackAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction override func onSubmitButtonPressed() {
        self.view.endEditing(false)
        
        let form = Forms()
        form.formTitle = self.pendingForm.formName
        let patient = MCPatient(forms: [form])
        
        patient.firstName = pendingForm.firstName
        patient.lastName = pendingForm.lastName
        
        //NewValues
        
        self.dismiss(animated: true, completion: {
            self.delegate?.submittedDetails(patient: patient)
        })
    }

}
