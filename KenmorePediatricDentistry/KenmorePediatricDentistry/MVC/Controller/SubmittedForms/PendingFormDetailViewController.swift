//
//  PendingFormDetailVC.swift
//  WestKendall
//
//  Created by Berlin Raj on 10/27/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

protocol PendingFormDetailViewControllerDelegate {
    func formSubmitted(form: PendingForm)
}

class PendingFormDetailViewController: MCViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var constraintViewHeight: NSLayoutConstraint!
    
    var delegate: PendingFormDetailViewControllerDelegate?
    
    var pendingForm: PendingForm!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.image = pendingForm.formImage
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func onSubmitButtonPressed() {
        let addCommentsVC = self.storyboard?.instantiateViewController(withIdentifier: "kAddCommentsViewController") as! AddCommentsViewController
        addCommentsVC.delegate = self
        addCommentsVC.pendingForm = pendingForm
        self.present(addCommentsVC, animated: true, completion: nil)
    }
    
    override func sendFormToGoogleDrive(completion: @escaping (_ success: Bool) -> Void) {
        self.completion = completion
        super.onSubmitButtonPressed()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        constraintViewHeight.constant = 2048
    }
}

extension PendingFormDetailViewController: AddCommentsViewControllerDelegate {
    
    func submittedDetails(patient: MCPatient) {
        //Display Values
        
        
        self.patient = patient
        self.sendFormToGoogleDrive { (success) in
            if success {
                BRProgressHUD.show()
                self.pendingForm.submitCompletedForm(completion: { (success, error) in
                    BRProgressHUD.hide()
                    if success {
                        self.delegate?.formSubmitted(form: self.pendingForm)
                        _ = self.navigationController?.popViewController(animated: true)
                    } else {
                        self.showAlert(error!.localizedDescription.uppercased())
                    }
                })
            } else {
                BRProgressHUD.hide()
                self.showAlert("SOMETHIG WENT WRONG\nPLEASE TRY AGAIN")
            }
        }
    }
}
