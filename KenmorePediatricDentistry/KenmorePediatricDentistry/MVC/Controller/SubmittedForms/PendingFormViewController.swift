//
//  PendingFormViewController.swift
//  WestKendall
//
//  Created by Berlin Raj on 10/27/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PendingFormViewController: MCViewController {
    
    var pendingForms: [PendingForm]!
    
    @IBOutlet weak var tableViewList: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonActionLogout() {
        self.showAlert("ARE YOU SURE YOU WANT TO LOGOUT?", buttonTitles: ["YES", "NO"]) { (buttonIndex) in
            if buttonIndex == 0 {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.pendingForms.count == 0 {
            self.showAlert("NO MORE RECORDS FOUND")
        }
    }
}

extension PendingFormViewController: PendingFormDetailViewControllerDelegate {
    func formSubmitted(form: PendingForm) {
        if let index = pendingForms.index(of: form) {
            self.pendingForms.remove(at: index)
            self.tableViewList.reloadData()
        }
    }
}

extension PendingFormViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let form = pendingForms[indexPath.row]
        func gotoDetailPage() {
            let pendingFormVC = self.storyboard?.instantiateViewController(withIdentifier: "kPendingFormDetailViewController") as! PendingFormDetailViewController
            pendingFormVC.pendingForm = form
            pendingFormVC.delegate = self
            self.navigationController?.pushViewController(pendingFormVC, animated: true)
        }
        if let _ = form.formImage {
            gotoDetailPage()
        } else {
            BRProgressHUD.show()
            NSURLConnection.sendAsynchronousRequest( NSURLRequest(url: (form.fileUrl! as NSURL) as URL) as URLRequest, queue: OperationQueue.main, completionHandler: { response, data, err in
                BRProgressHUD.hide()
                if let e = err {
                    self.showAlert(e.localizedDescription.uppercased())
                } else {
                    form.formImage = UIImage(data: data!)
                    gotoDetailPage()
                }
            })
        }
    }
}

extension PendingFormViewController : UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pendingForms.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath)
        let form = pendingForms[indexPath.row]
        cell.textLabel?.text = "\(form.firstName) \(form.lastName)"
        cell.detailTextLabel?.text = "Submitted Date: " + form.uploadDate.replacingOccurrences(of: "_", with: "-")
        return cell
    }
    
}
