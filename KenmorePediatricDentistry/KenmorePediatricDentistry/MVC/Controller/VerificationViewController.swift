//
//  VerificationViewController.swift
//  DiamondDentalAuto
//
//  Created by Bala Murugan on 10/6/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class VerificationViewController: MCViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var textfieldPatientId : UITextField!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
       // let _ = isFromPreviousForm
    }
    
    @IBAction func onBackButtonPressed (withSender sender : UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton) {
        if textfieldPatientId.isEmpty {
            self.showAlert("PLEASE ENTER THE PATIENT ID")
        } else {
            BRProgressHUD.show()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMMM dd, yyyy"
            let date = dateFormatter.date(from: patient.dateOfBirth)
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let manager = AFHTTPSessionManager(baseURL: URL(string: hostUrl))
            manager.responseSerializer.acceptableContentTypes = ["text/html"]
            
            manager.post("consent_fetch_patient_info_by_id.php", parameters: ["patient_id" : textfieldPatientId.text!, "first_name" : self.patient.firstName, "last_name": patient.lastName, "dob": dateFormatter.string(from: date!)], progress: { (progress) in
                
                }, success: { (task, object) in
                    BRProgressHUD.hide()
                    if self.navigationController?.topViewController == self {
                        let response = object as! [String : AnyObject]
                        if response["status"] as! String == "success" && response["message"] as! String == "Patient Found" {
                            let patientDetails = response["patientData"] as! [String: AnyObject]
                            self.patient.patientDetails = PatientDetails(details: patientDetails)
                            self.gotoNextForm()
                        }else{
                            self.showAlert("NO USER FOUND")
                        }
                    }
                    
                }, failure: { (session, error) in
                    BRProgressHUD.hide()
                    self.patient.patientDetails = nil
                    if self.navigationController?.topViewController == self {
                        self.showAlert(error.localizedDescription)
                    }
            })
        }
    }
}

extension VerificationViewController : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
