//
//  Forms.swift
//  WestgateSmiles
//
//  Created by samadsyed on 2/18/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class Forms: NSObject {
    
    var formTitle : String!
    var subForms : [Forms]!
    var isSelected : Bool!
    var index : Int!
    var isToothNumberRequired : Bool!
    var toothNumbers : String!
    
    init(formDetails : NSDictionary) {
        super.init()
        self.isSelected = false
    }
    
    override init() {
        super.init()
    }
    
    class func getAllForms (_ completion :(_ isConnectionfailed: Bool, _ forms : [Forms]?) -> Void) {
        let isConnected = Reachability.isConnectedToNetwork()
//        let forms = [kNewPatientSignInForm,kChildPatientSignInForm,kMedicalHistoryUpdate,kChildMedicalHistoryUpdate,kInsuranceCard, kDrivingLicense, kSelfieForm, kConsentForms]
//        
//        let formObj = getFormObjects(forms, isSubForm: false)
//        completion(isConnected ? false : true, formObj)
        
        #if AUTO
            var arrayResult : [Forms] = [Forms]()
            let forms = [kNewPatient, kExistingPatient, kConsentFormsAuto]
            for form in forms {
                let formObj = getFormObjects(forms: form)
                arrayResult.append(formObj)
            }
            completion(isConnected ? false : true, arrayResult)
            
        #else
            let forms = [kNewPatientSignInForm,kNewChildPatientSignInForm,kMedicalHistoryUpdate,kChildMedicalHistoryUpdate,kInsuranceCard, kDrivingLicense, kSelfieForm, kConsentForms]
            let formObj = getFormObjects(forms, isSubForm: false)
            completion(isConnected ? false : true, formObj)
        #endif
    }
    

    #if AUTO
    private class func getFormObjects (forms : [String : [String]]) -> Forms {
        let formObject = Forms()
        formObject.formTitle = forms.keys.first!
        formObject.isSelected = false
        
        var formList : [Forms] = [Forms]()
        for (_, form) in forms.values.first!.enumerated() {
            let formObj = Forms()
            formObj.isSelected = false
            formObj.formTitle = form
            formObj.isToothNumberRequired = toothNumberRequired.contains(form)
            formList.append(formObj)
        }
        formObject.subForms = formList
        return formObject
    }
    #else
    fileprivate class func getFormObjects (_ forms : [String], isSubForm : Bool) -> [Forms] {
    var formList : [Forms]! = [Forms]()
    for (idx, form) in forms.enumerated() {
    let formObj = Forms()
    formObj.isSelected = false
    formObj.index = isSubForm ? idx + 5 : idx
    formObj.formTitle = form
    formObj.isToothNumberRequired = toothNumberRequired.contains(form)
    if formObj.formTitle == kConsentForms {
    formObj.subForms = getFormObjects([kNitrousOxide,kSilverDiamine,kBoneGrafting,kOralSurgery,kRootCanal,kDentalExtract], isSubForm:  true)
    }
    
    if formObj.formTitle == kFeedBack {
    formObj.index = forms.count + formList[consentIndex].subForms.count + 1
    }
    
    formList.append(formObj)
    }
    return formList
    }
    #endif
    

    
//    fileprivate class func getFormObjects (_ forms : [String], isSubForm : Bool) -> [Forms] {
//        var formList : [Forms]! = [Forms]()
//        for (idx, form) in forms.enumerated() {
//            let formObj = Forms()
//            formObj.isSelected = false
//            formObj.index = isSubForm ? idx + 5 : idx
//            formObj.formTitle = form
//             formObj.isToothNumberRequired = toothNumberRequired.contains(form)
//            if formObj.formTitle == kConsentForms {
//                formObj.subForms = getFormObjects([kNitrousOxide,kSilverDiamine,kBoneGrafting,kOralSurgery,kRootCanal,kDentalExtract], isSubForm:  true)
//            }
//            
//            if formObj.formTitle == kFeedBack {
//               formObj.index = forms.count + formList[consentIndex].subForms.count + 1
//            }
//            
//            formList.append(formObj)
//        }
//        return formList
//    }
    
}
