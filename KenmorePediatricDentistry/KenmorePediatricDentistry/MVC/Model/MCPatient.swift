//
//  MCPatient.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit


class MCPatient: NSObject {
    
    required override init() {
        super.init()
    }
    
    init(forms : [Forms]) {
        super.init()
        self.selectedForms = forms
    }
    #if AUTO
    
    var patientDetails : PatientDetails?
    
    #endif
    
    var isChild : Bool = false
    var selectedForms : [Forms]!
    var firstName : String!
    var lastName : String!
    var initial : String!
    var preferredName: String!
    
    var dateOfBirth : String!
    var dateToday : String!
    var dentistName: String!
    
    var parentFirstName: String!
    var parentLastName: String!
    var parentDateOfBirth: String!
    
    var adultAddress: String!
    var adultCity: String!
    var adultZip: String!
    var adultState: String!
    var adultGender: Int! = 0
    var adultMarital: Int! = 0
    
    var adultHomePhone: String!
    var adultBusinessPhone: String!
    var adultCellPhone: String!
    var adultEmail: String!
    var adultSS: String!
    
    
    var adultEmployer: String!
    var adultEmployerOccupation: String!
    var adultEmpAddress: String!
    var adultEmpCity: String!
    var adultEmpZip: String!
    var adultEmpState: String!
    
    var adultSpouseName: String!
    var adultSpouseBdate: String!
    var adultSpouseEmployer: String!
    var adultSpouseOccupation: String!
    var adultSpouseAddress: String!
    var adultSpouseCity: String!
    var adultSpouseState: String!
    var adultSpouseZip: String!
    var adultSpouseSS: String!
    
    var adultEmergName: String!
    var adultEmergContactPhone: String!
    var adultRespName: String!
    var adultRespRelation: String!
    
    var adultPrimaryInsurance: Int! = 0
    var adultSecondaryInsurance: Int! = 0
    var adultWhoReferred: String!
    var adultPrimary : Int = 0
    var adultSecondary : Int = 0
    
    var adultPrimaryInsuredName: String!
    var adultPrimaryRelationTag: Int! = 0
    var adultPrimaryRelationValue: String!
    var adultPrimaryRelationOther: String!
    var adultPrimaryCompany: String!
    var adultPrimaryGroup: String!
    var adultPrimaryId: String!
    var adultPrimaryUnion: String!

    var adultSecondaryInsuredName: String!
    var adultSecondaryRelationTag: Int! = 0
    var adultSecondaryRelationValue: String!
    var adultSecondaryRelationOther: String!
    var adultSecondaryCompany: String!
    var adultSecondaryGroup: String!
    var adultSecondaryId: String!
    var adultSecondaryUnion: String!
    
    //Medical History
    var medicalHistoryQuestions1 : [MCQuestion]!
    var medicalHistoryQuestions2 : [MCQuestion]!
    var medicalHistoryQuestions3 : [MCQuestion]!
    var medicalHistoryQuestions4 : [MCQuestion]!
    var isWomen : Bool!
    var controlledSubstances : String!
    var controlledSubstancesClicked : Bool!
    var othersTextForm3 : String!
    var comments : String!
    var otherIllness : String!
    var buttonIllness: Int!
    var medicalSignature: UIImage!

    //Dental history
    
    var prevDentist: String!
    var lastDentalExam: String!
    var lastDentalTreatment: String!
    var lastDentalXray: String!
    var dropDownTeethClean: Int! = 0
    var dentalFloss: Int! = 2
    var dentalFlossValue: String!
    
    var is18YearsOld : Bool {
        return patientAge >= 18
    }
    
    var fullName: String {
        return initial.characters.count > 0 ? firstName + " " + initial + " " + lastName : firstName + " " + lastName
    }
    var patientAge: Int! {
        get {
            if dateOfBirth == nil || dateOfBirth.characters.count == 0 {
                return 0
            }
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = kCommonDateFormat
            
            let numberOfDays = (Date().timeIntervalSince(dateFormatter.date(from: self.dateOfBirth)!))/(3600 * 24)
            return Int(numberOfDays/365.2425)
        }
    }
    
    var Signature1: UIImage!
    var nitrousParentName : String = "N/A"
    var silverParentName : String = "N/A"
    
    //Oral Surgery
    
    var oralProcedure: String!
    var oralProcedure1: String!
    var oralParentName: String!
    //rootCanal
    var rootProcedure1: String!
     var rootParentName: String!
    
    var dentalExtractArray: NSMutableArray!
    var dentalExtractParentName: String!
    var dentalExtractOther: String!
    
    
    //child signin
    
    var childAddress: String!
    var childCity: String!
    var childState: String!
    var childZip: String!
    var childMainContact: String!
    var childSchool: String!
    var childNumberOfChildren: String!
    var childSiblings: String!
    var childGender: Int! = 0
    
    //responsibility1
    var childRespRelation1 : Int! = 0
    var childRespFatherName: String!
    var childRespBdate1: String!
    var childRespSSN1: String!
    var childRespAddress1: String!
    var childRespEmail1: String!
    var childRespCity1: String!
    var childRespState1: String!
    var childRespZipCode1: String!
    var childRespOccupation1: String!
    var childRespEmployer1: String!
    var childRespWorkPhone1: String!
    var childRespHomePhone1: String!
    var childRespCellPhone1: String!
    var childRespMainContact1: String!
    var childRespMaritalStatus1: Int! = 0
    
    //responsibility2
    var childRespRelation2 : Int! = 0
    var childRespMotherName: String!
    var childRespBdate2: String!
    var childRespSSN2: String!
    var childRespAddress2: String!
    var childRespEmail2: String!
    var childRespCity2: String!
    var childRespState2: String!
    var childRespZipCode2: String!
    var childRespOccupation2: String!
    var childRespEmployer2: String!
    var childRespWorkPhone2: String!
    var childRespHomePhone2: String!
    var childRespCellPhone2: String!
    var childRespMainContact2: String!
    var childRespMaritalStatus2: Int! = 0
    
    var childPrimaryInsurance: Int! = 0
    var childSecondaryInsurance: Int! = 0
    
    var withWhomChildReside: String!
    var howDidYouHearOffice: String!
    var primaryTag : Int = 0
    var secondaryTag : Int = 0
    
    //Primary Insurance
    
    var primaryInsuredName: String!
    var primaryRelationTag: Int! = 0
    var primaryRelationValue: String!
    var primaryRelationOther: String!
    var primaryBdate: String!
    var primarySSN: String!
    var primaryDateEmployed: String!
    var primaryEmployer: String!
    var primaryWorkPhone: String!
    var primaryInsurCompany: String!
    var primaryGroup: String!
    var primaryAddress: String!
    var primaryCity: String!
    var primaryState: String!
    var primaryZip: String!
    
    //Secondary Insurance
    
    var secondaryInsuredName: String!
    var secondaryRelationTag: Int! = 0
    var secondaryRelationValue: String!
    var secondaryRelationOther: String!
    var secondaryBdate: String!
    var secondarySSN: String!
    var secondaryDateEmployed: String!
    var secondaryEmployer: String!
    var secondaryWorkPhone: String!
    var secondaryInsurCompany: String!
    var secondaryGroup: String!
    var secondaryAddress: String!
    var secondaryCity: String!
    var secondaryState: String!
    var secondaryZip: String!
    
      var healthQues : HealthHistory = HealthHistory()
    var notesValue: String!
    
    //MEDCAL HISTORY
    var radioGraphPrevOffice: Int! = 0
    var childPreviouslyUnderPhysicianCare: Int! = 0
    var childPhysician: String!
    var childPhysicianPhone: String!
    var childLastPhysicalExam: String!
    var childFindings: String!
    
    var textViewElaborate : String!
    var doesYourChild : String!
    var medicalGuardianName: String!
    
    //DENTAL HISTORYVC1
    var brushTag: Int! = 0
    var brushOption: String!
    var flossTag: Int! = 0
    var flossOption: String!
    var byWhom: String!
    var bestCareChild: String!
    
    var immediateDentalConcern: String!
    var firstPartialDenture: String!
    var presentDenture: String!
    
    // privacy
    
    var relation : String!
    var relationTAG : Int! = 0
    var dependentFamily : String = ""
    var patientNamePrivacy : String!
    
    var relation1 : String!
    var relationTAG1 : Int! = 0
    var dependentFamily1 : String = ""
    var patientNamePrivacy1 : String!
    
    //PrivacyPractices Acknowledgement
    var signRefused: Bool!
    var signRefusalTag: Int!
    var signRefusalOther: String!
    var signRefusalEffort: String!
    var privacyAcknSignature: UIImage!
    var refusalName: String!
    var refusalCapacity: String!
    
    var signPolicy: UIImage!
    var signPatient: UIImage!
}

class Insurance: NSObject {
    var relation: Int!
    var companyName: String!
    var name: String!
    var employer: String!
    var group: String!
    var phoneNumber: String!
    var insuredID: String!
    var dateOfBirth: String!
    var otherRelation: String!
}

class HealthHistory : NSObject{
    
    var HealthHistoryQues : [[MCQuestion]]!
    
    var DentalHistoryQues : [[MCQuestion]]!
    
    required override init() {
        super.init()
        let quest0: [String] = ["Is this the patient’s first dental visit?",
                                "Has the patient been seen regularly?",
                                "Has the patient had any dental treatment in the past?",
                                "Has the patient ever had a difficult experience?",
                                "Has the patient had any trauma to the face/mouth/teeth?",
                                "Was your child bottle fed/breast fed and for how long?",
                                "Does/did your child suck their thumb, finder or pacifier?",
                                "Are you using fluoridated toothpaste?",
                                "Is your drinking water fluoridated?",
                                "Is your child taking fluoride tablets or drops?",
                                "Has your child ever had an orthodontic evaluation or treatment (braces)?"]
        
        let quest1: [String] = ["Bottle at bedtime",
                                "Pacifier",
                                "Thumb sucking/finger sucking",
                                "Lip sucking",
                                "Teeth grinding",
                                "Tongue thrust",
                                "Other"]
        
        let quest2: [String] = ["Speech problems/delay",
                                "Cavities",
                                "Broken teeth",
                                "Stained or discolored teeth",
                                "Dental infection/abscess",
                                "Pain from teeth",
                                "Popping or soreness of the jaws"]
        
        
        let quest3: [String] = ["In good health?",
                                "Sensitive or allergic to any medications, foods or latex?",
                                "Taking any medications?",
                                "Has your child every had any surgeries?",
                                "Has your child ever been hospitalized?"]
        
        let quest4: [String] = ["ADD/ADHD",
                                "Adenoid/Tonsil Problems",
                                "Anemia",
                                "Arthritis",
                                "Asthma",
                                "Autism Spectrum Disorder",
                                "Bleeding Problem",
                                "Blood Disorder",
                                "Blood Transfusion",
                                "Cancer",
                                "Cerebral Palsy",
                                "Depression"]
        
        let quest5: [String] = ["Developmental Delay",
                                "Diabetes",
                                "Eczema/Skin Problems",
                                "Endocrine Disorders",
                                "Excessive Gagging",
                                "Fainting or Dizziness",
                                "GERD/Acid Reflux",
                                "Hearing Disorder",
                                "Heart Murmur",
                                "Heart Disorder",
                                "Hepatitis",
                                "Hydrocephaly/Shunt"]
        
        let quest6: [String] = ["Hyper/Hypoglycemia",
                                "Impaired Vision",
                                "Intellectual Disability",
                                "Kidney Disease",
                                "Liver Disease",
                                "Learning Problems/Delays",
                                "Mononucleosis",
                                "Motor or Muscle Disorder",
                                "MRSA",
                                "Neglect/Abuse",
                                "Nutritional Deficiency",
                                "Rheumatic Fever"]
        
        let quest7: [String] = ["Seizure/Epilepsy",
                                "Sickle Cell Disease",
                                "Sleep Apnea/Snoring",
                                "Speech Disorders",
                                "Thyroid Problem",
                                "Tuberculosis (TB)",
                                "Other"]
        
        
        let dentalQuest1: [String] = ["Unhappy with appearance of your teeth",
                                "Unfavourable dental experience",
                                "Dental Fears",
                                "Preference for no anesthetic",
                                "Probs. w/effect or reactions to anesthetic",
                                "Orthodontic treatment",
                                "Periodontal (gum) treatment",
                                "Bleeding gums",
                                "Avoid brushing any part of your mouth",
                                "Part of your mouth is sensitive to pressure",
                                "Sore teeth"]
        
        let dentalQuest2: [String] = ["A burning sensation in your mouth",
                                "Difficulty swallowing",
                                "An unpleasant taste or odor in your mouth",
                                "Jaw problems (Temporomandibular joint)...",
                                "Diffuculty opening your mouth widely",
                                "Stiff neck muscles",
                                "Wake up with pain in jaws or teeth",
                                "Tension headaches",
                                "Clench or grind your teeth",
                                "Jaw clicking or popping",
                                "Lost any teeth"]
        
        let dentalQuest3: [String] = ["Has your present denture been relined?",
                                      "Is your present denture a problem?",
                                      "Are you satisfied with the appearance?",
                                      "Are you satisfied with the comfort?",
                                      "Are you satisfied with your chewing ability"]
    
        
        self.HealthHistoryQues = [MCQuestion.arrayOfQuestions(array: quest0),MCQuestion.arrayOfQuestions(array: quest1),MCQuestion.arrayOfQuestions(array: quest2),MCQuestion.arrayOfQuestions(array: quest3),MCQuestion.arrayOfQuestions(array: quest4),MCQuestion.arrayOfQuestions(array: quest5),MCQuestion.arrayOfQuestions(array: quest6),MCQuestion.arrayOfQuestions(array: quest7)]
        
        self.HealthHistoryQues[0][1].isAnswerRequired = true
        self.HealthHistoryQues[0][2].isAnswerRequired = true
        self.HealthHistoryQues[0][3].isAnswerRequired = true
        self.HealthHistoryQues[0][4].isAnswerRequired = true
        self.HealthHistoryQues[0][5].isAnswerRequired = true
        self.HealthHistoryQues[0][6].isAnswerRequired = true
        self.HealthHistoryQues[0][10].isAnswerRequired = true
        
        self.HealthHistoryQues[1][6].isAnswerRequired = true
        
        self.HealthHistoryQues[3][1].isAnswerRequired = true
        self.HealthHistoryQues[3][2].isAnswerRequired = true
        self.HealthHistoryQues[3][3].isAnswerRequired = true
        self.HealthHistoryQues[3][4].isAnswerRequired = true
        self.HealthHistoryQues[7][6].isAnswerRequired = true

        
        self.DentalHistoryQues = [MCQuestion.arrayOfQuestions(array: dentalQuest1),MCQuestion.arrayOfQuestions(array: dentalQuest2),MCQuestion.arrayOfQuestions(array: dentalQuest3)]
        
        
        self.DentalHistoryQues[0][0].isAnswerRequired = true
        self.DentalHistoryQues[0][1].isAnswerRequired = true
        self.DentalHistoryQues[0][2].isAnswerRequired = true
        self.DentalHistoryQues[0][3].isAnswerRequired = true
        self.DentalHistoryQues[0][4].isAnswerRequired = true
        self.DentalHistoryQues[0][5].isAnswerRequired = true
        self.DentalHistoryQues[0][6].isAnswerRequired = true
        self.DentalHistoryQues[0][7].isAnswerRequired = true
        self.DentalHistoryQues[0][8].isAnswerRequired = true
        self.DentalHistoryQues[0][9].isAnswerRequired = true
        self.DentalHistoryQues[0][10].isAnswerRequired = true
        
        self.DentalHistoryQues[1][0].isAnswerRequired = true
        self.DentalHistoryQues[1][1].isAnswerRequired = true
        self.DentalHistoryQues[1][2].isAnswerRequired = true
        self.DentalHistoryQues[1][3].isAnswerRequired = true
        self.DentalHistoryQues[1][4].isAnswerRequired = true
        self.DentalHistoryQues[1][5].isAnswerRequired = true
        self.DentalHistoryQues[1][6].isAnswerRequired = true
        self.DentalHistoryQues[1][7].isAnswerRequired = true
        self.DentalHistoryQues[1][8].isAnswerRequired = true
        self.DentalHistoryQues[1][9].isAnswerRequired = true
        self.DentalHistoryQues[1][10].isAnswerRequired = true
    }
    
    
}

enum MaritalStatus: String {
    case SINGLE = "S"
    case MARRIED = "M"
    case WIDOWED = "W"
    case DIVORCED = "D"
    case SEPARATE = "X"
    case UNKNOWN = "U"
    case DEFAULT = "DE"
    
    init(value: Int) {
        switch value {
        case 1: self = .SINGLE
        case 2: self = .MARRIED
        case 3: self = .WIDOWED
        case 4: self = .DIVORCED
        case 5: self = .SEPARATE
        case 6: self = .UNKNOWN
        default: self = .DEFAULT
        }
    }
    var index: Int {
        switch self {
        case .SINGLE: return 1
        case .MARRIED: return 2
        case .WIDOWED: return 3
        case .DIVORCED: return 4
        case .SEPARATE: return 5
        case .UNKNOWN: return 6
        default: return 7
        }
    }
    var description: String {
        switch self {
        case .SINGLE: return "SINGLE"
        case .MARRIED: return "MARRIED"
        case .WIDOWED: return "WIDOWED"
        case .DIVORCED: return "DIVORCED"
        case .SEPARATE: return "SEPARATED"
        case .UNKNOWN: return "UNKNOWN"
        default: return "DEFAULT"
        }
    }
}

enum Gender: String {
    case MALE = "M"
    case FEMALE = "F"
    case DEFAULT = "D"
    
    init(value: Int) {
        switch value {
        case 1: self = .MALE
        case 2: self = .FEMALE
        default: self = .DEFAULT
        }
    }
    var index: Int {
        switch self {
        case .MALE: return 1
        case .FEMALE: return 2
        default: return 3
        }
    }
    
    var description: String {
        switch self {
        case .MALE: return "MALE"
        case .FEMALE: return "FEMALE"
        default: return "DEFAULT"
        }
    }
}

#if AUTO
    class PatientDetails : NSObject {
        
        var dateOfBirth : String!
        var firstName : String!
        var lastName : String!
        var preferredName : String!
        var address : String!
        var city : String!
        var state : String!
        var zipCode : String!
        var country : String!
        var gender : Gender!
        var socialSecurityNumber : String!
        var email : String!
        var homePhone : String!
        var workPhone : String!
        var cellPhone : String!
        
        
        var patientNumber : String!
        var maritalStatus: MaritalStatus?
        var middleInitial : String!
        
        override init() {
            super.init()
        }
        
        init(details : [String: AnyObject]) {
            super.init()
            self.city = getValue(details["City"])
            self.dateOfBirth = getValue(details["Birthdate"])
            self.email = getValue(details["Email"])
            self.socialSecurityNumber = getValue(details["SSN"])
            self.address = getValue(details["Address"])
            self.zipCode = getValue(details["Zip"])
            
            if let gen = Int(details["sex"] as! String) {
                self.gender = Gender(value: gen)
            } else {
                self.gender = Gender(rawValue: details["sex"] as! String)
            }
            
            self.firstName = getValue(details["FName"])
            self.patientNumber = getValue(details["PatNum"])
            self.state = getValue(details["State"])
            self.homePhone = getValue(details["HmPhone"]).formattedPhoneNumber
            self.workPhone = getValue(details["work_phone"]).formattedPhoneNumber
            self.cellPhone = getValue(details["cellular_phone"]).formattedPhoneNumber
            self.country = getValue(details["Country"])
            self.lastName = getValue(details["LName"])
            self.preferredName = getValue(details["Preferred"])
            
            if let mar = Int(details["marital_status"] as! String) {
                self.maritalStatus = MaritalStatus(value: mar)
            } else {
                self.maritalStatus = MaritalStatus(rawValue: getValue(details["marital_status"]))
            }
            
            self.middleInitial = getValue(details["MiddleI"])
        }
        
        func getValue(_ value: AnyObject?) -> String {
            
            if value is String {
                return (value as! String).uppercased()
            }
            return ""
        }
        
        
    }
#endif

