//
//  PDFManager.swift
//  FutureDentistry
//
//  Created by Office on 2/21/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

var sharedPDFManager: PDFManager!

class PDFManager: NSObject, GIDSignInUIDelegate, GIDSignInDelegate {
    
    var service : GTLServiceDrive!
    var credentials : GTMOAuth2Authentication!
    var authViewController: UIViewController!
    var completion: ((_ success : Bool) -> Void)!
    
    class func sharedInstance() -> PDFManager {
        if sharedPDFManager == nil {
            sharedPDFManager = PDFManager()
        }
        return sharedPDFManager
    }
    
    fileprivate func driveService() -> GTLServiceDrive {
        if (service == nil)
        {
            service = GTLServiceDrive()
            service.shouldFetchNextPages = true
            service.isRetryEnabled = true
        }
        return service
    }
    
    func uploadToGoogleDrive(_ view : UIView, patient: MCPatient, completionBlock:@escaping (_ finished : Bool) -> Void) {
        if kOutputFileMustBeImage == true {
            if view.isKind(of: UIScrollView.self) {
                self.createImageForScrollView(view as! UIScrollView, patient: patient, completionBlock: { (finished) in
                    completionBlock(finished)
                })
            } else {
                self.createImageForView(view, patient: patient, completionBlock: { (finished) in
                    completionBlock(finished)
                })
            }
        } else {
            if view.isKind(of: UIScrollView.self) {
                self.createPDFForScrollView(view as! UIScrollView, patient: patient, completionBlock: { (finished) in
                    completionBlock(finished)
                })
            } else {
                self.createPDFForView(view, patient: patient, completionBlock: { (finished) in
                    completionBlock(finished)
                })
            }
        }
    }
    
    func createImageForView(_ view : UIView, patient : MCPatient, completionBlock:(_ finished : Bool) -> Void) {
        var image: UIImage?
        UIGraphicsBeginImageContext(view.bounds.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        if let _ = image {
            self.saveImage(UIImageJPEGRepresentation(image!, 1.0)!, patient: patient, completionBlock: completionBlock)
        }
    }
    
    func createImageForScrollView(_ scrollView : UIScrollView, patient : MCPatient, completionBlock:(_ finished : Bool) -> Void) {
        var image: UIImage?
        scrollView.isScrollEnabled = false
        scrollView.clipsToBounds = false
        let size: CGSize = CGSize(width: scrollView.contentSize.width, height: scrollView.contentSize.height)
        
        let savedContentOffset = scrollView.contentOffset
        
        UIGraphicsBeginImageContext(size)
        scrollView.layer.render(in: UIGraphicsGetCurrentContext()!)
        
        image = UIGraphicsGetImageFromCurrentImageContext()
        scrollView.contentOffset = savedContentOffset
        
        UIGraphicsEndImageContext()
        if let _ = image {
            self.saveImage(UIImageJPEGRepresentation(image!, 1.0)!, patient: patient, completionBlock: completionBlock)
        }
        scrollView.isScrollEnabled = true
        scrollView.clipsToBounds = true
    }
    
    
    func saveImage(_ imageData : Data, patient : MCPatient, completionBlock:(_ finished : Bool) -> Void) {
        do {
            // save as a local file
            let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            #if AUTO
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MMM dd, yyyy"
                let birthDate = dateFormatter.date(from: patient.dateOfBirth)
                dateFormatter.dateFormat = "yyyy'_'MM'_'dd"
                let dateString = dateFormatter.string(from: NSDate() as Date).uppercased()
                let birthDateString = dateFormatter.string(from: birthDate!)
                let patientId = patient.patientDetails != nil ? patient.patientDetails!.patientNumber! : "0"
                let name = "\(patient.firstName!)_\(patient.lastName!)-\(dateString)-\(patient.selectedForms.first!.formTitle.fileName)-\(birthDateString)-\(patientId).pdf"
            #else
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MM'_'dd'_'yyyy"
                let dateString = dateFormatter.string(from: Date()).uppercased()
                let name = patient.fullName.fileName + "_" + dateString + "_" + patient.selectedForms.first!.formTitle.fileName + ".pdf"
            #endif
            //            let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            //
            //            let dateFormatter = DateFormatter()
            //            dateFormatter.dateFormat = "MM'_'dd'_'yyyy"
            //            let dateString = dateFormatter.string(from: NSDate() as Date).uppercased()
            //            let name = "\(patient.fullName.fileName)_\(dateString)_\(patient.selectedForms.first!.formTitle.fileName).jpg"
            let path = "\(documentsPath)/\(name)"
            try imageData.write(to: URL(fileURLWithPath: path), options: .atomic)
            self.uploadFileToDrive(path, fileName: name, patientName: patient.fullName, formName: patient.selectedForms.first!.formTitle)
            completionBlock(true)
        } catch _ as NSError {
            completionBlock(false)
        }
    }
    
    func createPDFForView(_ view : UIView, patient : MCPatient, completionBlock:@escaping (_ finished : Bool) -> Void) {
        OperationQueue.main.addOperation {
            let pdfData = NSMutableData()
            let pageSize = screenSize
            UIGraphicsBeginPDFContextToData(pdfData, CGRect.zero, nil)
            let pdfContext : CGContext = UIGraphicsGetCurrentContext()!
            UIGraphicsBeginPDFPageWithInfo(CGRect(x: 0, y: 0, width: pageSize.width, height: pageSize.height), nil)
            view.layer.render(in: pdfContext)
            UIGraphicsEndPDFContext()
            do {
                let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
                #if AUTO
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "MMM dd, yyyy"
                    let birthDate = dateFormatter.date(from: patient.dateOfBirth)
                    dateFormatter.dateFormat = "yyyy'_'MM'_'dd"
                    let dateString = dateFormatter.string(from: NSDate() as Date).uppercased()
                    let birthDateString = dateFormatter.string(from: birthDate!)
                    let patientId = patient.patientDetails != nil ? patient.patientDetails!.patientNumber! : "0"
                    let name = "\(patient.firstName!)_\(patient.lastName!)-\(dateString)-\(patient.selectedForms.first!.formTitle.fileName)-\(birthDateString)-\(patientId).pdf"
                #else
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "MM'_'dd'_'yyyy"
                    let dateString = dateFormatter.string(from: Date()).uppercased()
                    let name = patient.fullName.fileName + "_" + dateString + "_" + patient.selectedForms.first!.formTitle.fileName + ".pdf"
                #endif
                // save as a local file
                let path = "\(documentsPath)/\(name)"
                try pdfData.write(toFile: path, options: .atomic)
                self.uploadFileToDrive(path, fileName: name, patientName: patient.fullName, formName: patient.selectedForms.first!.formTitle)
                completionBlock(true)
            } catch _ as NSError {
                completionBlock(false)
            }
        }
    }
    
    func createPDFForScrollView(_ scrollView : UIScrollView, patient : MCPatient, completionBlock:@escaping (_ finished : Bool) -> Void) {
        OperationQueue.main.addOperation {
            let pdfData = NSMutableData()
            let scrollHeight = scrollView.contentSize.height
            let rawNumberOfPages = scrollHeight / screenSize.height
            let numberOfPages = Int(ceil(rawNumberOfPages))
            var pageNumber = Int()
            let pageSize = screenSize
            scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
            UIGraphicsBeginPDFContextToData(pdfData, CGRect.zero, nil)
            let pdfContext : CGContext = UIGraphicsGetCurrentContext()!
            repeat {
                UIGraphicsBeginPDFPageWithInfo(CGRect(x: 0, y: 0, width: pageSize.width, height: pageSize.height), nil)
                if pageNumber < 1 {
                    scrollView.layer.render(in: pdfContext)
                } else if pageNumber >= 1 {
                    let offsetForScroll = CGFloat(pageNumber) * screenSize.height
                    scrollView.setContentOffset(CGPoint(x: 0, y: offsetForScroll), animated: false)
                    UIGraphicsGetCurrentContext()!.translateBy(x: 0, y: -offsetForScroll)
                    scrollView.layer.render(in: pdfContext)
                }
                pageNumber = pageNumber + 1
            }
                while pageNumber < numberOfPages
            UIGraphicsEndPDFContext()
            do {
                // save as a local file
                let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
                #if AUTO
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "MMM dd, yyyy"
                    let birthDate = dateFormatter.date(from: patient.dateOfBirth)
                    dateFormatter.dateFormat = "yyyy'_'MM'_'dd"
                    let dateString = dateFormatter.string(from: NSDate() as Date).uppercased()
                    let birthDateString = dateFormatter.string(from: birthDate!)
                    let patientId = patient.patientDetails != nil ? patient.patientDetails!.patientNumber! : "0"
                    let name = "\(patient.firstName!)_\(patient.lastName!)-\(dateString)-\(patient.selectedForms.first!.formTitle.fileName)-\(birthDateString)-\(patientId).pdf"
                #else
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "MM'_'dd'_'yyyy"
                    let dateString = dateFormatter.string(from: Date()).uppercased()
                    let name = patient.fullName.fileName + "_" + dateString + "_" + patient.selectedForms.first!.formTitle.fileName + ".pdf"
                #endif
                let path = "\(documentsPath)/\(name)"
                try pdfData.write(toFile: path, options: .atomic)
                self.uploadFileToDrive(path, fileName: name, patientName: patient.fullName, formName: patient.selectedForms.first!.formTitle)
                completionBlock(true)
            } catch _ as NSError {
                completionBlock(false)
            }
        }
    }
    
    
    
    ///MARK:- CHECK DRIVE FREE SPACE
    func CheckGoogleDriveFreeSpace()  {
        let query : GTLQueryDrive = GTLQueryDrive.queryForAboutGet()
        query.fields = "storageQuota,user"
        _  = driveService().executeQuery(query) { (ticket, about, error) in
            if error == nil{
                let abt : GTLDriveAbout = about as! GTLDriveAbout
                let limitInGB : Int = Int(abt.storageQuota.limit.doubleValue/1024.0/1024.0/1024.0)
                let usageinGB : Int = Int(abt.storageQuota.usage.doubleValue/1024.0/1024.0/1024.0)
                let userEmail : String = abt.user.emailAddress
                
                //ALMOST FULL
                if usageinGB > limitInGB - 1{
                    self.showGoogleDriveErrorAlert()
                    
                }else if (usageinGB/limitInGB) * 100 > 80{
                    /// 80% OF DRIVE IS FULL
                    self.sendWarningEmail(userEmail)
                    
                }
            }else{
                print("Error \(String(describing: error?.localizedDescription))")
            }
            
        }
    }
    
    //MARK:- GOOGLE DRIVE ERROR ALERT
    func showGoogleDriveErrorAlert () {
        ((UIApplication.shared.delegate as! AppDelegate).window?.rootViewController as! UINavigationController).viewControllers.last!.showAlert("WARNING", message: "YOUR GOOGLE DRIVE IS ALMOST FULL")
    }
    
    //MARK:- SEND WARNING EMAIL
    func sendWarningEmail(_ toEmail : String){
        let message : SMTPMessage = SMTPMessage()
        message.from = kGoogleID
        message.to = toEmail
        message.host = "smtp.gmail.com"
        message.account = kGoogleID
        message.pwd = kGooglePassword
        
        message.subject = "Your Google drive almost Full"
        message.content = "<p>Hi,</p><p>Your google drive account is 80% full. So please clear some contents immediately</p><p>Thank you.</p>"
        message.send({ (messg, now, total) in
            
        }, success: { (messg) in
            print("Email sent")
        }, failure: { (messg, error) in
        })
        
    }
    
    
    
    func uploadFileToDrive(_ path : String, fileName : String, patientName: String, formName: String) {
        
        self.CheckGoogleDriveFreeSpace()
        
        let formData = try! Data(contentsOf: URL(fileURLWithPath: path))
        
        DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
            #if AUTO
                let manager = AFHTTPSessionManager(baseURL: URL(string: hostUrl))
                manager.responseSerializer.acceptableContentTypes = ["text/html"]
                manager.post("consent_form_upload_to_server.php", parameters: nil, constructingBodyWith: { (data) in
                    data.appendPart(withFileData: formData, name: "consent_file", fileName: fileName, mimeType: fileName.hasSuffix("") ? "application/pdf" : "image/jpeg")
                }, progress: { (progress) in
                    
                }, success: { (task, result) in
                    
                }, failure: { (task, error) in
                    
                })
            #else
                // http://mconsent.net/admin/api/activity_api.php?client_name=pp&formname=MedicalHistoryform&task_data=image&email=srs@gmail.com
                
                let manager = AFHTTPSessionManager(baseURL: URL(string: "http://mconsent.net/admin/api/")!)
                
                manager.responseSerializer.acceptableContentTypes = ["text/html"]
                manager.post("activity_api.php?", parameters: ["client_name": kClinicName, "patient_name": patientName, "formname": formName, "appkey": kAppKey], constructingBodyWith: { (data) in
                    data.appendPart(withFileData: formData, name: "task_data", fileName: fileName, mimeType: fileName.hasSuffix(".pdf") ? "application/pdf" : "image/jpeg")
                }, progress: { (progress) in
                    
                }, success: { (task, result) in
                    
                }, failure: { (task, error) in
                    
                })
            #endif
        }
        
        func uploadFile(_ identitifer : String) {
            let driveFile = GTLDriveFile()
            driveFile.mimeType = fileName.hasSuffix(".pdf") ? "application/pdf" : "image/jpeg"
            driveFile.originalFilename = "\(fileName)"
            driveFile.name = "\(fileName)"
            driveFile.parents = [identitifer]
            
            let uploadParameters = GTLUploadParameters(data: formData, mimeType: fileName.hasSuffix(".pdf") ? "application/pdf" : "image/jpeg")
            let query = GTLQueryDrive.queryForFilesCreate(withObject: driveFile, uploadParameters: uploadParameters)
            query?.addParents = identitifer
            
            self.driveService().executeQuery(query!, completionHandler: { (ticket, uploadedFile, error) -> Void in
                if (error == nil) {
                    let fileManager = FileManager.default
                    if fileManager.fileExists(atPath: path) {
                        do {
                            try fileManager.removeItem(atPath: path)
                        } catch  {
                            
                        }
                    }
                } else {
                    
                }
            })
        }
        
        func checkAndCreateFolder(_ folderName: String, parent: GTLDriveFile?, completion: @escaping ((_ success: Bool, _ folder: GTLDriveFile) -> Void)) {
            let folderDateQuery = GTLQueryDrive.queryForFilesList()
            folderDateQuery?.q = parent == nil ? "mimeType = 'application/vnd.google-apps.folder' and name = '\(folderName)' and trashed = false" : "mimeType = 'application/vnd.google-apps.folder' and name = '\(folderName)' and trashed = false and '\(parent!.identifier!)' in parents"
            
            self.driveService().executeQuery(folderDateQuery!, completionHandler: { (ticket, obj, error) -> Void in
                if error == nil {
                    let childFolder = (obj as! GTLDriveFileList).files
                    if childFolder != nil && (childFolder?.count)! > 0 {
                        let dateFolder = childFolder?[0] as! GTLDriveFile
                        completion(true, dateFolder)
                    } else {
                        createNewFolder(folderName, parent: parent == nil ? nil : [parent!.identifier!], createCompletion: { (success, returnFolder) in
                            if success {
                                completion(true, returnFolder!)
                            }
                        })
                    }
                } else {
                }
            })
        }
        
        func createNewFolder(_ folderName : String, parent : [String]?, createCompletion: @escaping ((_ success: Bool, _ returnFolder: GTLDriveFile?) -> Void)) {
            let folderObj = GTLDriveFile()
            folderObj.name = folderName
            if parent != nil {
                folderObj.parents = parent
            }
            folderObj.mimeType = "application/vnd.google-apps.folder"
            
            let queryFolder = GTLQueryDrive.queryForFilesCreate(withObject: folderObj, uploadParameters: nil)
            
            self.driveService().executeQuery(queryFolder!, completionHandler: { (ticket, result, error) -> Void in
                if (error == nil) {
                    let folder = result as! GTLDriveFile
                    createCompletion(true, folder)
                } else {
                    createCompletion(false, nil)
                }
            })
        }
        
        let name = "\(UIDevice.current.name)_" + kFolderName
        checkAndCreateFolder(name, parent: nil) { (success, folder) in
            if success {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MM'-'dd'-'yyyy"
                let folderName = dateFormatter.string(from: NSDate() as Date).uppercased()
                
                checkAndCreateFolder(folderName.fileName, parent: folder, completion: { (success, folder) in
                    uploadFile(folder.identifier!)
                })
            }
        }
    }
    func authorizeDrive(_ viewController : UIViewController, completion:@escaping (_ success : Bool) -> Void) {
        credentials = GTMOAuth2ViewControllerTouch.authForGoogleFromKeychain(forName: kKeychainItemName, clientID: kClientId, clientSecret: kClientSecret)
        if credentials.canAuthorize {
            self.driveService().authorizer = credentials
            completion(true)
        } else {
            self.completion = completion
            self.authViewController = viewController
            var configureError: NSError?
            GGLContext.sharedInstance().configureWithError(&configureError)
            assert(configureError == nil, "Error configuring Google services: \(String(describing: configureError))")
            
            GIDSignIn.sharedInstance().uiDelegate = self
            GIDSignIn.sharedInstance().delegate = self
            GIDSignIn.sharedInstance().scopes = ["https://www.googleapis.com/auth/drive"]
            GIDSignIn.sharedInstance().signIn()
        }
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        authViewController.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        authViewController.dismiss(animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if error == nil {
            // ...
            self.driveService().authorizer = user.authentication.fetcherAuthorizer()
            self.completion(true)
        } else {
            self.completion(false)
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user:GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }
}
