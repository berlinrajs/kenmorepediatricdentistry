//
//  FormsTableViewCell.swift
//  ProDental
//
//  Created by samadsyed on 2/18/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class FormsTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewCheckMark: UIImageView!
    
    @IBOutlet weak var labelFormName: UILabel!
        var indexPath: IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
