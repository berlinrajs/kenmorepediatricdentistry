//
//  MedicalHistoryCollectionViewCell.swift
//  ABC Clinic
//
//  Created by Leojin Bose on 2/29/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class MedicalHistoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var buttonQuestion: UIButton!
    
    func configureCellOption (obj : MCQuestion) {
        buttonQuestion.setTitle(" " + obj.question, for: .normal)
        if let selected = obj.selectedOption {
            buttonQuestion.isSelected = selected
        } else {
            buttonQuestion.isSelected = false
        }
    }
}
