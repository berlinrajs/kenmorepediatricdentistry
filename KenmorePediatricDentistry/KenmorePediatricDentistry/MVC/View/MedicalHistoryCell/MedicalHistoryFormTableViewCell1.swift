//
//  MedicalHistoryFormTableViewCell1.swift
//  ABC Clinic
//
//  Created by Leojin Bose on 2/29/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class MedicalHistoryFormTableViewCell1: UITableViewCell {

    @IBOutlet weak var labelQuestion: UILabel!
    @IBOutlet weak var buttonYes: RadioButton!
    @IBOutlet weak var labelAnswer: MCLabel!
    @IBOutlet weak var labelIfYes: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell (obj : MCQuestion) {

        labelQuestion.text = obj.question
        if let answer = obj.answer {
            labelAnswer.text = " \(answer)"
        } else {
            labelAnswer.text = ""
        }
        
        labelAnswer.isHidden = !obj.isAnswerRequired
        labelIfYes.isHidden = !obj.isAnswerRequired
        
        if let selected = obj.selectedOption {
            buttonYes.isSelected = selected
        } else {
            buttonYes.deselectAllButtons()
        }
    }

}
