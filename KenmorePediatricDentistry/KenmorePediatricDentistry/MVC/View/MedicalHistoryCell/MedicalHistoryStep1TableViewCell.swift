//
//  MedicalHistoryStep1TableViewCell.swift
//   Angell Family Dentistry
//
//  Created by Leojin Bose on 2/26/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

protocol MedicalHistoryCellDelegate {
    func radioButtonAction(sender : RadioButton)
}

class MedicalHistoryStep1TableViewCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var buttonYes: RadioButton!
    @IBOutlet weak var buttonCheck: UIButton!
    
    var delegate : MedicalHistoryCellDelegate?
    var question : MCQuestion!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell (obj : MCQuestion) {
        backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.clear
        question = obj
        labelTitle.text = obj.question
        buttonYes.isSelected = obj.selectedOption!
    }
    
    func configureCheckCell (obj : MCQuestion) {
        backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.clear
        question = obj
        labelTitle.text = obj.question
        buttonCheck.isSelected = obj.selectedOption == nil ? false : obj.selectedOption!
    }
    
    @IBAction func radioButtonAction(sender: RadioButton) {
        if question != nil {
            if question.isAnswerRequired == true && sender == buttonYes {
                question.selectedOption = buttonYes.isSelected
                self.delegate?.radioButtonAction(sender: sender)
            } else {
                question.selectedOption = buttonYes.isSelected
            }
        }
    }
    
    @IBAction func buttonCheckAction() {
        buttonCheck.isSelected = !buttonCheck.isSelected
        if question != nil {
            if question.isAnswerRequired == true && buttonCheck.isSelected {
                question.selectedOption = buttonYes.isSelected
//                self.delegate?.radioButtonAction(buttonCheck)
            } else {
                question.selectedOption = buttonYes.isSelected
            }
        }
    }
}
